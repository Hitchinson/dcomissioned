{
    "id": "2db88b5f-5eb5-47c5-994a-e461577ace98",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_EnemyType2",
    "eventList": [
        {
            "id": "5e291a33-5ac6-4c34-b243-bbd2167a7026",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2db88b5f-5eb5-47c5-994a-e461577ace98"
        },
        {
            "id": "f294e993-8995-44f6-ad04-39f4b1c87b7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2db88b5f-5eb5-47c5-994a-e461577ace98"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "deca4d61-5c18-4555-bce2-2c2fec343390",
    "visible": true
}