//Drone following Player
move_towards_point(obj_Player.x + random_range(-7,-10), obj_Player.y - random_range(117, 120), 5)

//DroneColour

if (obj_Status.playerHealth >= 75)
{
	sprite_index = spr_DroneFull;
}

if (obj_Status.playerHealth < 75)
{
	sprite_index = spr_DroneMed;
}

if (obj_Status.playerHealth < 50)
{
	sprite_index = spr_DroneLow;
}

if (obj_Status.playerHealth <25)
{
	sprite_index = spr_DroneCrit;
}


