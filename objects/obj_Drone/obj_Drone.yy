{
    "id": "1dc754f2-1f74-44b6-9eef-5f53a8719757",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Drone",
    "eventList": [
        {
            "id": "bd4a8593-bd38-4de4-bf91-59d5985f55ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1dc754f2-1f74-44b6-9eef-5f53a8719757"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "1d64066a-7650-41aa-89db-02cc4a021fb6",
    "visible": true
}