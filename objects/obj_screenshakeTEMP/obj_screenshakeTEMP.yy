{
    "id": "4023d22d-d981-452a-b2f2-1c2470fcf743",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_screenshakeTEMP",
    "eventList": [
        {
            "id": "30b0678d-d7c5-4031-815f-a734c83391e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4023d22d-d981-452a-b2f2-1c2470fcf743"
        },
        {
            "id": "35303f64-fda6-4b79-825f-318008e7b629",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4023d22d-d981-452a-b2f2-1c2470fcf743"
        },
        {
            "id": "362144c0-a454-4c5a-8a7f-e5aa79f45d99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4023d22d-d981-452a-b2f2-1c2470fcf743"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}