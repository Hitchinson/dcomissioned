{
    "id": "3c05b155-b0d4-4dba-827d-5b1a196904c5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Launch",
    "eventList": [
        {
            "id": "2dcce4c6-585a-492f-b61b-4ad101cb7b51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3c05b155-b0d4-4dba-827d-5b1a196904c5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "41c1f7e8-57e3-48b0-8d4a-94d7cd144b3e",
    "visible": true
}