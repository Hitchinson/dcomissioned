//Draw Textbox
draw_sprite(spr_Textbox, 0, x, y);

//Draw Text
draw_set_font(fnt_ComicSans);
draw_set_halign(fa_left);
draw_set_valign(fa_middle);
draw_set_color(c_white);
draw_text_ext(x, y, text, stringHeight, boxWidth);