{
    "id": "ec34842e-853f-44b3-8bd6-f5e198c42603",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Platform1",
    "eventList": [
        
    ],
    "maskSpriteId": "8900dbae-5820-4167-80c0-01e8a4212567",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "283145a6-3256-45ce-8cf5-1c13cc8de060",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "5f309552-ed48-4f31-ac7d-286cbb3744ab",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 0
        },
        {
            "id": "63e139bb-49a7-4a7f-83a6-6ab2e4e5371f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 44
        },
        {
            "id": "e59e56f9-0104-406e-a534-0b7955b2711d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 44
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8900dbae-5820-4167-80c0-01e8a4212567",
    "visible": true
}