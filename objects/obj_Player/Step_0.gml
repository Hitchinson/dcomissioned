//Get Player Input
key_left = keyboard_check(ord("A")) or keyboard_check(vk_left);
key_right = keyboard_check(ord("D")) or keyboard_check(vk_right);
key_up = keyboard_check(ord("W")) or keyboard_check(vk_up);
key_down = keyboard_check(ord("S")) or keyboard_check(vk_down);
key_jump = keyboard_check(ord("W")) or keyboard_check(vk_up);
key_flight = keyboard_check(vk_space);


//Calculate Movement
isAlive = (obj_Status.playerHealth>0);
onSolid = place_meeting(x,y+1,obj_Wall);

if (isAlive){
	var move = key_right - key_left;
	hsp = move * walksp;
	vsp = vsp + grv;
	
	//Special state
	switch(state){
		case "start_jump": case "end_jump": case "fly_start":
			hsp = 0;
		break;
	}
}
else{
	hsp=0;
	state = "death";
}

trigger_jump = (onSolid and (key_jump));
trigger_flight = (key_flight and obj_Status.playerFuel > 0);

//Horizontal Collision
if (place_meeting(x+hsp,y,obj_Wall))
{
	while (!place_meeting(x+sign(hsp),y,obj_Wall))
	{
		x = x + sign(hsp);
	}
	hsp = 0;
}

x = x + hsp;

//Vertical Collision
if (place_meeting(x,y+vsp,obj_Wall))
{
	while (!place_meeting(x,y+sign(vsp),obj_Wall))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}

y = y + vsp;

//Process State Change
switch(state){
	case "idle":
		if(trigger_jump){
			state = "start_jump";
		}
		else if(trigger_flight){
			state = "fly_start";
		}
		else if(hsp!=0){
			state = "start_walk";
		}
		break;
	case "start_walk":
		if(trigger_jump){
			state = "start_jump";
		}
		else if(trigger_flight){
			state = "fly_start";
		}
		else if(sprite_index==sprPlayerWalkStart && (image_index+image_speed>=image_number-1)){
			state = "walk";
		}
		else if(hsp==0){
			state = "stop_walk";
		}
		break;
	case "walk":
		if(trigger_jump){
			state = "start_jump";
		}
		else if(trigger_flight){
			state = "fly_start";
		}
		else if(hsp==0){
			state = "stop_walk";
		}
		break;
	case "stop_walk":
		if(trigger_jump){
			state = "start_jump";
		}
		else if(trigger_flight){
			state = "fly_start";
		}
		else if(hsp!=0){
			state = "start_walk";
		}
		else if(sprite_index==sprPlayerWalkStop && (image_index+image_speed>=image_number-1)){
			state = "idle";
		}
		break;
	case "start_jump":
		image_speed = 4;
		if(sprite_index==sprPlayerJumpUp && (image_index+image_speed>=image_number-1)){
			state = "mid_air";
			vsp = -15;
			audio_play_sound(aud_Jump,10,false);
			image_index=image_number-1;
			image_speed=0;
		}
		break;
	case "mid_air":
		if(trigger_flight){
			state="fly_mid";
		}	
		else if(vsp>=0){
			sprite_index = sprPlayerJumpLand;
			image_index = 0;
			if(onSolid){
				state = "end_jump";
				image_speed = defimgspd;
			}
		}	
		break;
	case "end_jump":
		image_speed = 8;
		if(sprite_index==sprPlayerJumpLand && (image_index+image_speed>=image_number-1)){
			state = "idle";
			sprite_index=sprPlayerIdle;
			image_speed = defimgspd;
		}
		break;
	case "fly_start":
		image_speed = 5;
		if(sprite_index==sprPlayerFlyStart && (image_index+image_speed>=image_number) && trigger_flight){
			state = "fly_mid";
		}
		else if(!trigger_flight){
			state = "fly_mid";
		}
		break;
	case "fly_mid":
		image_speed = 5;
		if(trigger_flight){
			vsp = 0;
			vsp-=grv;
			if(key_up){
				vsp-=5;
			}
			if(key_down){
				vsp+=5;
			}
			obj_Status.playerFuel = obj_Status.playerFuel - 0.05;
		}
		if(!trigger_flight && place_meeting(x,y+1+vsp,obj_Wall)){
			state="fly_end";
		}
		break;
	case "fly_end":
		image_speed = 5;
		if(sprite_index==sprPlayerFlyLand && image_index+image_speed>=image_number){
			state="idle";
		}
		break;
	case "death":
		if(sprite_index==sprPlayerDeath && image_index+image_speed>=image_number){
			room_goto_next();
		}
		break;
	default: break;
}

/// Player sprite assigned here
//Change heading based on hsp
if(hsp!=0){
	image_xscale = sign(hsp);
}

//Change sprite based on cow state
switch(state){
	case "idle":
			if(sprite_index!=sprPlayerIdle){
				sprite_index=sprPlayerIdle;
				image_index=0;
			}
		break;
	case "start_walk":
			if(sprite_index!=sprPlayerWalkStart){
				sprite_index=sprPlayerWalkStart;
				image_index=0;
			}
		break;
	case "walk":
			if(sprite_index!=sprPlayerWalking){
				sprite_index=sprPlayerWalking;
				image_index=0;
			}
		break;
	case "stop_walk":
			if(sprite_index!=sprPlayerWalkStop){
				sprite_index=sprPlayerWalkStop;
				image_index=0;
			}
		break;
	case "start_jump":
		image_speed=4;
		if(sprite_index!=sprPlayerJumpUp){
			sprite_index=sprPlayerJumpUp;
			image_index=0;
		}
		break;
	case "fly_start":
		if(sprite_index!=sprPlayerFlyStart){
			sprite_index=sprPlayerFlyStart;
			image_index=0;
		}
		break;
	case "fly_mid":
		if(sprite_index!=sprPlayerFlyLoop){
			sprite_index=sprPlayerFlyLoop;
			image_index=0;
		}
		break;
	case "fly_end":
		if(sprite_index!=sprPlayerFlyLand){
			sprite_index=sprPlayerFlyLand;
			image_index=0;
		}
		break;
	case "death":	//Change sprite
		if(sprite_index!=sprPlayerDeath){
			sprite_index=sprPlayerDeath;
			image_index=0;
			image_speed=2;
			audio_play_sound(aud_Death,10,false);
		}
		break;
	default: break;
}

	
//Parallax Backgrounds
var x_cam = camera_get_view_x(view_camera[0]);
layer_x("SkyBG", x_cam * 0.25);
layer_x("SecondaryBG", x_cam * 0.15);

//Audio Listener
audio_listener_position(x,y,0);
