{
    "id": "4a0bfebb-652a-4ad3-a00a-c22b62fcbb12",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Player",
    "eventList": [
        {
            "id": "3c8d28e9-bc92-4c88-8e53-59c04eb55ef9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4a0bfebb-652a-4ad3-a00a-c22b62fcbb12"
        },
        {
            "id": "8fec869f-9973-4718-b09b-aaae44d4fdd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4a0bfebb-652a-4ad3-a00a-c22b62fcbb12"
        },
        {
            "id": "00540dbc-533d-427c-8f3d-d77ca1ca3e92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "4a0bfebb-652a-4ad3-a00a-c22b62fcbb12"
        },
        {
            "id": "9a83a874-183f-4d45-8eaf-43efd21e71e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3075f1aa-2e88-4ddb-9686-3d7fa12f3b36",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4a0bfebb-652a-4ad3-a00a-c22b62fcbb12"
        },
        {
            "id": "d2777612-57d6-4e86-a261-572b83780d45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4a0bfebb-652a-4ad3-a00a-c22b62fcbb12"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "e3726050-bd87-4f50-a413-846101e0edde",
    "visible": true
}