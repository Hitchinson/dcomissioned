hsp = 0;
vsp = 0;
grv = 0.5;
walksp = 5;
damage = 0;
death_sound = true;
global.playerScrap = 0;
global.playerTesla = 0;

audio_play_sound(aud_Junkyard,10,true);

//Initialization of state machine
//Available player state are: 'idle','start_walk','walk','stop_walk','fly','death'
//'start_jump','mid_air','end_jump'
//'fly_start, 'fly_mid', 'fly_land'
isAlive = true;
state = "idle";
defimgspd = 5;
image_speed=defimgspd;		//Variable animation speed