{
    "id": "e197dc77-aae2-4941-befe-c6dddc7a2c50",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Platform4",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "7017a882-7420-4124-97b8-ee4d21bee2b9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "30fb41b5-74eb-4837-8ed5-0604a430d00e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 0
        },
        {
            "id": "afa9bb2c-e557-4266-a55f-98be145847ce",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 44
        },
        {
            "id": "dc6b2d6f-c9b6-401d-8141-d7460a886851",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 44
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "2797cdef-4bbd-4df3-a91f-07e255a8a018",
    "visible": true
}