{
    "id": "1b9d7758-93ed-4495-8464-b4b6be21f416",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oScrap",
    "eventList": [
        {
            "id": "1dfb9ff2-ec65-4a6f-985a-811248565b76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1b9d7758-93ed-4495-8464-b4b6be21f416"
        },
        {
            "id": "0b3b7acb-770e-4192-a1ec-7e733dd86ef4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1b9d7758-93ed-4495-8464-b4b6be21f416"
        },
        {
            "id": "7119c2ca-0682-47a0-90de-8bc174cfbf07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4a0bfebb-652a-4ad3-a00a-c22b62fcbb12",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1b9d7758-93ed-4495-8464-b4b6be21f416"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "84ad9230-6e9d-48dc-bd02-d1e3a7e3a57f",
    "visible": true
}