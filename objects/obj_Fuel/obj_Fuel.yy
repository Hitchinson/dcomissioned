{
    "id": "ef9fbad7-9c54-4c7e-97ad-dc33dca2a9b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Fuel",
    "eventList": [
        {
            "id": "bfb5f266-d975-49cc-a2cc-520a6ceb58c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4a0bfebb-652a-4ad3-a00a-c22b62fcbb12",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ef9fbad7-9c54-4c7e-97ad-dc33dca2a9b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "a625b9ca-ed05-4e17-b266-c623199a8a84",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "93eefd93-e0e7-45d8-a0e9-36da19060c16",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 0
        },
        {
            "id": "1b5b6674-5f2d-41bc-9742-ba3385134c86",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 44
        },
        {
            "id": "e1674a9f-aea6-4de5-959a-fa5ccfbc199d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 44
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "fa0c36d2-2fb6-42d6-8edd-5b330dea93aa",
    "visible": true
}