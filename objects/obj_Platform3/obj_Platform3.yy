{
    "id": "6458d438-7c6f-40b4-b07b-0b8e0108e0ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Platform3",
    "eventList": [
        
    ],
    "maskSpriteId": "874e86eb-2fd7-4e45-8104-819cfc1fd71f",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "3955dd33-f402-4c68-af4d-2ef9165e0b5c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "63e68f04-c965-4fe5-8ce2-b336cc48435a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 0
        },
        {
            "id": "bd43e3e5-dbeb-4faa-9825-b682ee2c3f59",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 44
        },
        {
            "id": "60757e88-cc50-40c1-a3d9-01883fce3d08",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 44
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "874e86eb-2fd7-4e45-8104-819cfc1fd71f",
    "visible": true
}