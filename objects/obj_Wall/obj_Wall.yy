{
    "id": "0f8375ae-4aa3-42b6-96d4-e00083262bb3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Wall",
    "eventList": [
        
    ],
    "maskSpriteId": "41c9469e-8d8a-4e2f-b409-5b0b66bf3d9f",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "41c9469e-8d8a-4e2f-b409-5b0b66bf3d9f",
    "visible": false
}