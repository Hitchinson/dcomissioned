{
    "id": "826b2f21-3977-4bc0-9b54-492ed049aadb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Energy",
    "eventList": [
        {
            "id": "16d6e806-d3ab-4102-9cd5-8ccc05c23155",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4a0bfebb-652a-4ad3-a00a-c22b62fcbb12",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "826b2f21-3977-4bc0-9b54-492ed049aadb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "92ee70e2-be69-4dd1-8849-50c9bbd5c6fe",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "f0c2b5ac-9ad8-4376-92a8-9e1764c652f2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 0
        },
        {
            "id": "787e26ae-8c8a-4b02-b2e5-ad9390ed7321",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 44
        },
        {
            "id": "f3e353cd-d134-4095-b58e-634143083aac",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 44
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "e6995e6a-759d-4f10-8d52-f3ce18b3fb9a",
    "visible": true
}