//Draw the GUI
draw_sprite(spr_FuelGUI, 0, 0, 768);

//Draw Energy Bar
//draw_sprite_ext(spr_energyFiller, 0, 21, 75, max(0, playerEnergy/playerMaxEnergy), 1, 0, c_white,1);

//Draw Health Bar
//draw_sprite (spr_healthBorder, 1, 8, 9);
//draw_sprite_ext(spr_healthFiller, 1, 21, 200, max(0,playerHealth/playerMaxHealth), 1, 0, c_white, 1);

//Draw Fuel Bar
draw_sprite_ext(spr_FuelBar, 1, 20, 700, max(0,playerFuel/playerMaxFuel), 1, 90, c_white, 1);