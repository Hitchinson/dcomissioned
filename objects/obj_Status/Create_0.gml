randomize();

//Initialise Player Stats

playerLevel = 1;

playerMaxHealth = 75 + (playerLevel * 25);
playerHealth = playerMaxHealth;

playerMaxEnergy = 75 + (playerLevel * 25);
playerEnergy = playerMaxEnergy;

playerMaxFuel = 75 + (playerLevel * 25);
playerFuel = playerMaxFuel;

playerAttack = 5;
playerDefense = 5;

playerCurrentEXP = 0;
playerMaxEXP = 100;

playerScrap = 0;

minerals = 0;

