{
    "id": "fcd0c5fb-5fd1-4285-bf0f-7e4876c3ada1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWorkstationScreen",
    "eventList": [
        {
            "id": "8745c0d8-45f1-47eb-b37e-c3a35f5859ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fcd0c5fb-5fd1-4285-bf0f-7e4876c3ada1"
        },
        {
            "id": "28d05afb-10ba-4d3e-b3ef-d48857552f35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fcd0c5fb-5fd1-4285-bf0f-7e4876c3ada1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
    "visible": true
}