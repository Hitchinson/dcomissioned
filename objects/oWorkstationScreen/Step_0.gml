/// @description Insert description here
// You can write your code in this editor

var shiftkey = keyboard_check_pressed(vk_shift);

//Change state here
switch(state){
	case "startup":
		if(sprite_index==sprWorkstationStartup && image_index==image_number-1){
			//advance to running_state
			state = "running";
			//spawn the cog and tesla
			scrapinst = instance_create_depth(840,400,-100,oWorkstationScrap);
			teslainst = instance_create_depth(840,640,-100,oWorkstationTesla);
			installinst = instance_create_depth(1027,837,-100,oWorkstationInstall);
		}
		break;
	case "running":
		if(shiftkey){
			state = "shutdown";
			instance_destroy(scrapinst);
			instance_destroy(teslainst);
			instance_destroy(installinst);
		}
		break;
	case "shutdown":
		if(sprite_index==sprWorkstationStartup && image_index==0){
			room_goto(global.room_back);
		}
		break;
	default: break;
}

//Change sprite here
switch(state){
	case "startup":
		if(sprite_index!=sprWorkstationStartup){
			sprite_index=sprWorkstationStartup;
			image_index=0;
		}
		break;
	case "running":
		if(sprite_index!=sprWorkstationRunning){
			sprite_index=sprWorkstationRunning;
			image_index=0;
		}
		break;
	case "shutdown":
		if(sprite_index!=sprWorkstationStartup){
			sprite_index=sprWorkstationStartup;
			image_index=image_number-1;
			image_speed = -1;
		}
		break;
	default: break;
}