{
    "id": "3953439c-0d39-4642-a02e-6deea05f7a66",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWorkstationTesla",
    "eventList": [
        {
            "id": "4d434307-b8df-42e2-9668-28b76171acef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3953439c-0d39-4642-a02e-6deea05f7a66"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "51fa1e39-a989-407d-b50c-e484ee9b8ba1",
    "visible": true
}