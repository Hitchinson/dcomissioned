{
    "id": "3075f1aa-2e88-4ddb-9686-3d7fa12f3b36",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_EnemyType1",
    "eventList": [
        {
            "id": "e66fd60a-6699-4c7c-8258-7e662b232146",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3075f1aa-2e88-4ddb-9686-3d7fa12f3b36"
        },
        {
            "id": "286a3e64-fb8d-4980-a0cc-a75ec7692ef5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3075f1aa-2e88-4ddb-9686-3d7fa12f3b36"
        },
        {
            "id": "9e27ceba-c532-4b0e-93ca-fbde57506303",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3075f1aa-2e88-4ddb-9686-3d7fa12f3b36"
        },
        {
            "id": "ebf34b74-1f43-463d-bf4b-85c2733247eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "3075f1aa-2e88-4ddb-9686-3d7fa12f3b36"
        },
        {
            "id": "bc8ec79c-a3f2-47e8-9858-db7387b190f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3075f1aa-2e88-4ddb-9686-3d7fa12f3b36"
        }
    ],
    "maskSpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
    "parentObjectId": "7ee96003-9e63-494d-92f9-5e71adcf2583",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
    "visible": true
}