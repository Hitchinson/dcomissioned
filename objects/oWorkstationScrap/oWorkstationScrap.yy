{
    "id": "f4c739f7-19cc-4828-908b-ad7f8d3dc845",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWorkstationScrap",
    "eventList": [
        {
            "id": "e2398413-d72a-42c8-ae47-0cdbfa816658",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f4c739f7-19cc-4828-908b-ad7f8d3dc845"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "ebbe1d72-dd98-457a-b384-b48c81f2307e",
    "visible": true
}