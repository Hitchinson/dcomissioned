{
    "id": "b74565b1-ccab-4c55-ad53-572e85e8de4f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Platform2",
    "eventList": [
        
    ],
    "maskSpriteId": "c84e4dbe-5a40-41ce-8bba-4ef5ab7ad9f8",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "64466e4d-1e90-461c-ac7c-bd4f40e924f2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "287229c0-0120-46c4-8cdb-d8cef5b5b368",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 0
        },
        {
            "id": "4173edae-f9e4-43df-9cac-6bb5634e8af0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 44
        },
        {
            "id": "0a7ad0c9-94c1-427a-9229-89584b989f9b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 44
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "c84e4dbe-5a40-41ce-8bba-4ef5ab7ad9f8",
    "visible": true
}