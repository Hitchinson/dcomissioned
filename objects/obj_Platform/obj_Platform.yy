{
    "id": "6def5e2d-f8fc-4099-bc8a-3ae9b5a9cd8a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Platform",
    "eventList": [
        
    ],
    "maskSpriteId": "9617fe78-6def-4935-8c53-7ac621d7269d",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "eb08aab7-a9cd-4288-ad39-2a3b0dc1cf2b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "5333f393-7303-4d32-be50-716c20e139dc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 0
        },
        {
            "id": "40313e2d-fa10-4920-bce5-6e5697b325b6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 44
        },
        {
            "id": "6029743e-1d81-48bf-8a81-fabddd97d75a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 44
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "9617fe78-6def-4935-8c53-7ac621d7269d",
    "visible": true
}