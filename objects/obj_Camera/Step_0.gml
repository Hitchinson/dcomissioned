x += (xTo - x)/25;
y += (yTo - y)/25;

x = clamp(x,0+(1920/2),room_width-(1920/2));
y = clamp(y,0+(1080/2),room_height-(1080/2));

if (follow != noone)
{
	xTo = follow.x;
	yTo = follow.y;
}

var vm = matrix_build_lookat(x,y,-1000,x,y,0,0,1,0);
camera_set_view_mat(camera,vm);

//Death
if (obj_Status.playerHealth <= 0)
{
instance_destroy();
}

