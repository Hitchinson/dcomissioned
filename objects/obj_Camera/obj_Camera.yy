{
    "id": "1356a7b2-b019-44cf-8eb2-dbf6446d4c71",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Camera",
    "eventList": [
        {
            "id": "b1e96ecd-e08c-47d2-b2b8-5d240c894a66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1356a7b2-b019-44cf-8eb2-dbf6446d4c71"
        },
        {
            "id": "b9d55899-1718-4174-9539-34ab6c3f0342",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1356a7b2-b019-44cf-8eb2-dbf6446d4c71"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}