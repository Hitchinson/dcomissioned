{
    "id": "e2fa6672-8c94-465e-8665-8b2431c463ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Platform5",
    "eventList": [
        
    ],
    "maskSpriteId": "39e32874-d8bd-4ecc-948e-08db1ce0f058",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "1ebfdefc-5800-4e82-809a-9a6daf65ab35",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "62f2161e-1ee7-44f4-af25-f1346be73b1e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 0
        },
        {
            "id": "7dffa2f0-53d6-4438-88bb-1406c8af5773",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 44
        },
        {
            "id": "1b2b605d-21b4-44e3-af4b-291f3d60554f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 44
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "39e32874-d8bd-4ecc-948e-08db1ce0f058",
    "visible": true
}