var shiftkey = keyboard_check_pressed(vk_shift);

if (place_meeting(x, y, obj_Player)){
	//Display the green cogwheel workstation when in range
	sprite_index=spr_Station;
	if (myTextBox == noone){
		myTextBox = instance_create_layer(x, y, "GUITexts", obj_Textbox);
		myTextBox.text = myText;
	}
	if(shiftkey){
		room_persistent = true;
		global.room_back= room;
		room_goto(rm_workstation);
	}
}
else{
	//Display the yellow cogwheel workstation when not in range
	sprite_index=spr_Station2;
	if (myTextBox != noone){
		instance_destroy(myTextBox);
		myTextBox = noone;
	}
}
		