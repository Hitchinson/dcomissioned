{
    "id": "d6b2d6b5-6e81-4bca-9e6e-e64f8461abfd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Station",
    "eventList": [
        {
            "id": "8c637d56-0f45-42a6-a18d-5ce9ec37cb71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d6b2d6b5-6e81-4bca-9e6e-e64f8461abfd"
        },
        {
            "id": "14c109d8-12bd-4a41-857e-c512db1291d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d6b2d6b5-6e81-4bca-9e6e-e64f8461abfd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "04a6b914-c88e-4617-84da-047034083c9e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "ce52df1f-89b0-4b18-89d3-9d04ec6b61f7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 0
        },
        {
            "id": "7db3060e-67d5-4099-946e-37639c1f9978",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 318,
            "y": 44
        },
        {
            "id": "cec7c6f5-a4c3-4be7-bff1-1fc93249a326",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 44
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4cadc284-9c74-443d-8048-a6b3b81bc6e6",
    "visible": true
}