{
    "id": "c66a162f-b6f0-4551-a224-7001996fc06f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Gun",
    "eventList": [
        {
            "id": "3cc2a772-53dd-45c6-b0cf-9c196e6b6b8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "c66a162f-b6f0-4551-a224-7001996fc06f"
        },
        {
            "id": "fbb2ec89-23ec-4fd2-930d-69c441248c44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c66a162f-b6f0-4551-a224-7001996fc06f"
        },
        {
            "id": "752d29eb-105f-43ff-9ebb-ba5b36fb5933",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c66a162f-b6f0-4551-a224-7001996fc06f"
        },
        {
            "id": "8b71bbcb-6737-4ee6-ab63-90eab8b5ef3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c66a162f-b6f0-4551-a224-7001996fc06f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "d3d0ff00-64ed-4f5e-854f-7188cb08cdad",
    "visible": true
}