//Get input button
cmdShoot = (mouse_check_button(mb_left) || keyboard_check(ord("Z")));

//Destroy the gun when player die
if (obj_Status.playerHealth <= 0)
{
	instance_destroy();
}

//Reposition the gun relative to the cow head
cowState = obj_Player.state;
cowX = obj_Player.x;
cowY = obj_Player.y;
cowXscale = obj_Player.image_xscale;
cowXscalesign = sign(obj_Player.image_xscale);

//Check when to slide the gun in or out
slideIn = (cowState=="fly_start" or cowState=="fly_mid");
slideOut = (cowState=="fly_end");

//Calculate slide position
if(slideIn){
	if(retractPos<gunRetract){
		retractPos+=retractSpeed;
	}
}
if(slideOut){
	if(retractPos>0){
		retractPos-=retractSpeed;
	}
}

image_xscale = cowXscale;
x = cowX+(cowXscalesign*(xPosToCow-retractPos));
y = cowY+yPosToCow;

//Reduce gun cooldown
firingdelay = firingdelay -1;
recoil = max(0, recoil - 1);

//Advance State
switch(state){
	case "idle":
		if(cmdShoot){
			state="start";
		}
		break;
	case "start":
		if(sprite_index==spr_GunStart && image_index+image_speed>=image_number-1){
			state="shoot";
		}
		else if(!cmdShoot || retractPos!=0){
			state="end";
		}
		break;
	case "shoot":
		if(!cmdShoot){
			state="end";
		}
		break;
	case "end":
		if(sprite_index==spr_GunStop && image_index+image_speed>=image_number-1){
			state="idle";
		}
		else if(cmdShoot){
			state="start";
		}
		break;
	default: break;
}

//Change Sprite
switch(state){
	case "idle":
		if(sprite_index!=spr_Gun){
			sprite_index=spr_Gun;
			image_index=0;
		}
		break;
	case "start":
		if(sprite_index!=spr_GunStart){
			sprite_index=spr_GunStart;
			image_index=0;
		}
		image_speed=1;
		break;
	case "shoot":
		if(sprite_index!=spr_GunShoot){
			sprite_index=spr_GunShoot;
			image_index=0;
		}
		image_speed=1;
		break;
	case "end":
		if(sprite_index!=spr_GunStop){
			sprite_index=spr_GunStop;
			image_index=0;
		}
		image_speed=1;
		break;
	default: break;
}

//Shoot command
if (cmdShoot and (firingdelay < 0) and (obj_Status.playerEnergy >= 7) and state=="shoot")
{
	recoil = 4;
	firingdelay = 5;
	obj_Status.playerEnergy = obj_Status.playerEnergy - 7;
	with (instance_create_layer(x,y+15,"Bullets",obj_Bullet))
	{
		speed = 25;
		direction = 270 + (90*(obj_Gun.cowXscalesign)) + random_range(-3,3);	//Change how the bullet direction is being headed
		image_xscale = obj_Gun.cowXscalesign;
	}
}

x = x - lengthdir_x(recoil,image_angle); 
y = y - lengthdir_y(recoil,image_angle); 