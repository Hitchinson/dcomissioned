{
    "id": "3ccff361-a475-4015-8257-7f78d5a13fb0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_EnemyType1Dead",
    "eventList": [
        {
            "id": "fe0be5d5-a160-4bf1-aca3-351fa53b1401",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3ccff361-a475-4015-8257-7f78d5a13fb0"
        },
        {
            "id": "f92562fc-2655-4c4e-b36a-cde22743914f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "3ccff361-a475-4015-8257-7f78d5a13fb0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "0e414b03-1358-4f59-943f-87cf886522f2",
    "visible": true
}