{
    "id": "51fa1e39-a989-407d-b50c-e484ee9b8ba1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTeslaIcon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 129,
    "bbox_left": 36,
    "bbox_right": 126,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d07e0a05-bdd8-43bd-8c58-36e88fac86a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51fa1e39-a989-407d-b50c-e484ee9b8ba1",
            "compositeImage": {
                "id": "2b5321d7-8ef7-4f51-8eca-af02d29f993e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d07e0a05-bdd8-43bd-8c58-36e88fac86a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f7cc455-454f-48a0-9e62-c6ff6580730b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d07e0a05-bdd8-43bd-8c58-36e88fac86a6",
                    "LayerId": "0319d9e9-700f-478f-9961-80194be2c0dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 132,
    "layers": [
        {
            "id": "0319d9e9-700f-478f-9961-80194be2c0dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51fa1e39-a989-407d-b50c-e484ee9b8ba1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 161,
    "xorig": 80,
    "yorig": 66
}