{
    "id": "5070b10f-e358-4b30-b606-1e130ec9578d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerWalking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 71,
    "bbox_right": 265,
    "bbox_top": 75,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f7ab3208-45f7-4679-a2cb-0fe7cf8db2bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "91e8f0b9-1608-4628-8bff-854dda42666c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7ab3208-45f7-4679-a2cb-0fe7cf8db2bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cd7372c-475d-48e0-93a9-6ac3d7d00fb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7ab3208-45f7-4679-a2cb-0fe7cf8db2bd",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "1a4e3a53-a0cb-4ac1-b9e2-6148139f6a5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "4bef8a81-a367-4a4c-bdda-6a693c48cd67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a4e3a53-a0cb-4ac1-b9e2-6148139f6a5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "472b4606-a150-4aa3-b3ae-aa6c2aa5f1d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a4e3a53-a0cb-4ac1-b9e2-6148139f6a5b",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "22a401c3-be21-40ef-8d1d-ae591efa2376",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "71b51656-cbd7-4aba-94b8-ba9d217fea08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22a401c3-be21-40ef-8d1d-ae591efa2376",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad5f857b-9431-43a7-887a-376ca58d8b7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22a401c3-be21-40ef-8d1d-ae591efa2376",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "f7919d78-b26f-47fa-a680-742f20624288",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "4377fb20-c126-4de6-b5c3-cd93bb12337a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7919d78-b26f-47fa-a680-742f20624288",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f048990-2fb5-4373-a60b-4967bec5b1a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7919d78-b26f-47fa-a680-742f20624288",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "9c0a7c0c-e124-410f-baf0-ba1c9a9900f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "ebbb0b13-d304-4de0-8157-7e0bb55f2e49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c0a7c0c-e124-410f-baf0-ba1c9a9900f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f3bb22e-96f9-4075-a239-59f2fa218907",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c0a7c0c-e124-410f-baf0-ba1c9a9900f9",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "83c66c6f-2db7-412a-a0d6-706fea0e8a10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "cc19d8e9-5050-4c12-bc8b-1b1ddaf77b0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83c66c6f-2db7-412a-a0d6-706fea0e8a10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e99f9651-1baa-4638-bf2d-7ce7c1bce2f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83c66c6f-2db7-412a-a0d6-706fea0e8a10",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "d344650a-50f4-4958-84ac-e0af659f5f56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "f7292c0b-15b0-444d-973a-760ec0c0962b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d344650a-50f4-4958-84ac-e0af659f5f56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4efae709-cd1c-42a0-b7b1-0ffa489e2487",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d344650a-50f4-4958-84ac-e0af659f5f56",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "158c4837-ec27-4208-91a0-10a774bd653c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "97752dd9-dd9f-4242-8e00-754e78395631",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "158c4837-ec27-4208-91a0-10a774bd653c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e2a5e4-a7ef-44b6-b8a0-6eb5bd9ed2ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "158c4837-ec27-4208-91a0-10a774bd653c",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "6fff322b-79e9-4fd5-b274-86e8f9dd467b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "adbf70f4-0042-42f9-9297-7ae90ba704fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fff322b-79e9-4fd5-b274-86e8f9dd467b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d10fcf8-a5a5-4081-8bf7-dfff4c54b60b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fff322b-79e9-4fd5-b274-86e8f9dd467b",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "e963a034-086d-4f48-983d-d8ca46f18568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "181dc513-a462-4334-84c8-f2962330d20e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e963a034-086d-4f48-983d-d8ca46f18568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65c83278-68b8-4a4f-956a-16ea6d6b61f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e963a034-086d-4f48-983d-d8ca46f18568",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "6bd03f1e-9827-4fad-b1f5-ac90c4ce3495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "b9c5d0bd-f3fb-495f-b6d2-34366c3ab3d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bd03f1e-9827-4fad-b1f5-ac90c4ce3495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0675bf10-196f-4668-ba73-f3f4ea97e470",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bd03f1e-9827-4fad-b1f5-ac90c4ce3495",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "3b2b3f92-8871-474b-9f11-1141d917736d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "84fc59de-f134-4eed-8667-3557224bfe25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b2b3f92-8871-474b-9f11-1141d917736d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faaae3d5-1cd2-4706-a6fe-64e0af2a0e4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b2b3f92-8871-474b-9f11-1141d917736d",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "07d39ba9-198b-48c7-9a96-b6f44e9a883a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "3ff1a520-a53d-4f98-87d5-47b3fc38f309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07d39ba9-198b-48c7-9a96-b6f44e9a883a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93138add-35a0-422c-a508-f6f912f405ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07d39ba9-198b-48c7-9a96-b6f44e9a883a",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "193e55a2-1a3d-41b4-8b71-bbcce0d6ec41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "840f3fc8-73fa-4ff1-a747-9d44bbcd1196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "193e55a2-1a3d-41b4-8b71-bbcce0d6ec41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dd87f65-a91b-4baf-96c6-e59e41194eac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "193e55a2-1a3d-41b4-8b71-bbcce0d6ec41",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "ca9e5e0c-721d-47ea-93bd-260cd7371013",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "2dda87a9-45fa-417d-a434-dc86313f418f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca9e5e0c-721d-47ea-93bd-260cd7371013",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df1c5b41-453b-437f-871b-dfc5508e9c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca9e5e0c-721d-47ea-93bd-260cd7371013",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "b6db6190-ff29-4480-9d5d-8234db53169b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "81ea5005-b4a5-4e95-b91c-f44c49efabf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6db6190-ff29-4480-9d5d-8234db53169b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "500d28b5-77a6-4a23-a3fd-69712d3b6592",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6db6190-ff29-4480-9d5d-8234db53169b",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "cc08685d-f2ea-41a3-87aa-13a82c3222a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "39ffcb3f-8822-4d7f-b8dc-51a9ce3b4a84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc08685d-f2ea-41a3-87aa-13a82c3222a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c70e1b57-2900-49ae-b730-63d8e3a4670e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc08685d-f2ea-41a3-87aa-13a82c3222a1",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "d1143c83-bce8-4a9b-abf4-45d9b494211a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "426e9f67-0ec2-4513-9ea4-4b9ecd37ac98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1143c83-bce8-4a9b-abf4-45d9b494211a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "184b6904-b4ec-4304-af86-5cba6a264a89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1143c83-bce8-4a9b-abf4-45d9b494211a",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "4558c83b-ea54-450c-b197-5bac73191696",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "0b6b2ee9-057f-4efd-9882-b79de426951b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4558c83b-ea54-450c-b197-5bac73191696",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f5bcbee-18b3-4dbe-adf4-30fd77671b1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4558c83b-ea54-450c-b197-5bac73191696",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "cdcb82d5-f2d5-4213-a587-50937fceb9a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "a2d104fd-d5ef-4c19-82a3-40674e91bbc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdcb82d5-f2d5-4213-a587-50937fceb9a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f5a5fb3-f136-40ff-8e08-f14326185626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdcb82d5-f2d5-4213-a587-50937fceb9a5",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "7b420fa5-4604-41ee-8ccf-a175501c0d3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "bed6f372-d898-4e65-ab90-57c6f95100d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b420fa5-4604-41ee-8ccf-a175501c0d3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a7e53af-9c71-4bd0-b66b-055d679ff25d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b420fa5-4604-41ee-8ccf-a175501c0d3c",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "6c2a8a33-606d-47ac-870c-c048de47cb85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "38d0bcf3-1801-4291-ae74-dcd4bda514db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c2a8a33-606d-47ac-870c-c048de47cb85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1426d831-9eff-4103-a06d-ca6f37be165c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c2a8a33-606d-47ac-870c-c048de47cb85",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "7dd15fbb-638b-4c58-abce-7b95d702d18f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "6f96c382-f2e4-4729-8525-e4d94e48446d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dd15fbb-638b-4c58-abce-7b95d702d18f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "028d5136-03d7-4957-a824-5e3ff5111dc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dd15fbb-638b-4c58-abce-7b95d702d18f",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "db7372a1-3b71-4cc9-8392-14e118c0e1e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "703b0ae2-73d9-4c43-98fe-21ce8195be19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db7372a1-3b71-4cc9-8392-14e118c0e1e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f0ff4ba-849b-4d3a-b06c-26e76ddd95b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db7372a1-3b71-4cc9-8392-14e118c0e1e6",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "6703f584-7f10-4dfd-b39e-d80ee3654558",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "1123e4f3-e273-46af-9075-a8f7501ec5d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6703f584-7f10-4dfd-b39e-d80ee3654558",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1c48f2d-4548-4256-aa65-7af356a80607",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6703f584-7f10-4dfd-b39e-d80ee3654558",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "848b2c93-e104-4296-ac8d-e3e7e597ca29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "3f43b30e-7dbb-4971-968e-569e0f090c70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "848b2c93-e104-4296-ac8d-e3e7e597ca29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ac3b2c0-a766-473d-8188-0fc558c53dfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "848b2c93-e104-4296-ac8d-e3e7e597ca29",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "7139c697-a720-4dd6-89a8-f4db02bd4160",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "e43f2834-b594-4688-8427-de94970929e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7139c697-a720-4dd6-89a8-f4db02bd4160",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9c679e4-ebbd-4346-968d-9bf05a6ed777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7139c697-a720-4dd6-89a8-f4db02bd4160",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "2f37e8fd-81a0-42f5-8d4d-55863dc0ad13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "3ee68a24-121b-448d-855f-95a20bd7669a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f37e8fd-81a0-42f5-8d4d-55863dc0ad13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5910357-8fd3-4bee-9323-0cb444822b36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f37e8fd-81a0-42f5-8d4d-55863dc0ad13",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "6d14bb38-15d8-4ce8-921f-61a1d295f9e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "931e83d6-c17e-4568-996e-384f07442ba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d14bb38-15d8-4ce8-921f-61a1d295f9e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa4eadfd-f5ef-4477-b415-2dd132fb27c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d14bb38-15d8-4ce8-921f-61a1d295f9e7",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        },
        {
            "id": "252b6eed-a517-41b5-9be5-212787db0227",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "compositeImage": {
                "id": "bcaee30b-3603-42d8-b2c2-48b978dfeee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "252b6eed-a517-41b5-9be5-212787db0227",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac29ce13-3800-4a09-bc8e-90061eadfb0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "252b6eed-a517-41b5-9be5-212787db0227",
                    "LayerId": "5aa443fc-ac50-4c11-be11-fc0ff976681e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "5aa443fc-ac50-4c11-be11-fc0ff976681e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5070b10f-e358-4b30-b606-1e130ec9578d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 164,
    "yorig": 144
}