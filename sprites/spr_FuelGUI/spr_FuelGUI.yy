{
    "id": "ccef8482-b7dc-4c22-b3a7-d8d92ffa30d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FuelGUI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 409,
    "bbox_left": 0,
    "bbox_right": 161,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f7f7b3b3-e452-4f35-a68b-f845325fa269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccef8482-b7dc-4c22-b3a7-d8d92ffa30d7",
            "compositeImage": {
                "id": "b76a6ec3-5f0d-45a0-8246-875289017fe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f7b3b3-e452-4f35-a68b-f845325fa269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2e7f4a6-be5e-4a4c-9936-2dbd07346240",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f7b3b3-e452-4f35-a68b-f845325fa269",
                    "LayerId": "501f721e-afea-4a19-ae95-3eb8ce1ed6d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 410,
    "layers": [
        {
            "id": "501f721e-afea-4a19-ae95-3eb8ce1ed6d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccef8482-b7dc-4c22-b3a7-d8d92ffa30d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 162,
    "xorig": 0,
    "yorig": 409
}