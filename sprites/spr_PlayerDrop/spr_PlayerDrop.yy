{
    "id": "5355bacb-6fba-401b-b669-bba5f4e25216",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerDrop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 0,
    "bbox_right": 189,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2356e5e7-676b-4d3a-ade5-3058808472a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5355bacb-6fba-401b-b669-bba5f4e25216",
            "compositeImage": {
                "id": "e58e67f8-7864-4aec-846a-fb16cef3d559",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2356e5e7-676b-4d3a-ade5-3058808472a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e1584be-4fda-46e1-ac77-f983b2019486",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2356e5e7-676b-4d3a-ade5-3058808472a2",
                    "LayerId": "7ccf38bb-7e0b-4c64-b0ef-5a6edb1fc6c3"
                }
            ]
        },
        {
            "id": "722cc356-791e-4f65-92e5-4b3ff1163b15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5355bacb-6fba-401b-b669-bba5f4e25216",
            "compositeImage": {
                "id": "2561c73b-025f-451d-877c-87703a9c038a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "722cc356-791e-4f65-92e5-4b3ff1163b15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "104a6837-e46b-4e85-9a4d-db3d41796bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "722cc356-791e-4f65-92e5-4b3ff1163b15",
                    "LayerId": "7ccf38bb-7e0b-4c64-b0ef-5a6edb1fc6c3"
                }
            ]
        },
        {
            "id": "e679485d-2467-4f6c-b65e-d1bfb7b902b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5355bacb-6fba-401b-b669-bba5f4e25216",
            "compositeImage": {
                "id": "6a80bf18-2b8f-45c8-b424-fab6433b9c73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e679485d-2467-4f6c-b65e-d1bfb7b902b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2b6a6cb-eba9-48bc-9752-020e7b461bd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e679485d-2467-4f6c-b65e-d1bfb7b902b3",
                    "LayerId": "7ccf38bb-7e0b-4c64-b0ef-5a6edb1fc6c3"
                }
            ]
        },
        {
            "id": "08ac1333-bee1-49c0-963e-2d83b92e0f30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5355bacb-6fba-401b-b669-bba5f4e25216",
            "compositeImage": {
                "id": "453b19c8-cd14-45c9-a59b-741ac37dfc94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08ac1333-bee1-49c0-963e-2d83b92e0f30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8925a8f2-ff30-46ba-9bda-6cba1ef2f958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08ac1333-bee1-49c0-963e-2d83b92e0f30",
                    "LayerId": "7ccf38bb-7e0b-4c64-b0ef-5a6edb1fc6c3"
                }
            ]
        },
        {
            "id": "9f283206-e1e8-4aac-b171-b7b8bbc8edca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5355bacb-6fba-401b-b669-bba5f4e25216",
            "compositeImage": {
                "id": "b3590d6e-aaa2-4357-a720-2e62cb6f38cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f283206-e1e8-4aac-b171-b7b8bbc8edca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89cbd48d-9908-4e4e-87bc-d03cd68f74ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f283206-e1e8-4aac-b171-b7b8bbc8edca",
                    "LayerId": "7ccf38bb-7e0b-4c64-b0ef-5a6edb1fc6c3"
                }
            ]
        },
        {
            "id": "726b164b-2229-47c4-b139-e947db30965e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5355bacb-6fba-401b-b669-bba5f4e25216",
            "compositeImage": {
                "id": "18c96ed4-3da0-45a9-a43d-a0a9556fbc7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "726b164b-2229-47c4-b139-e947db30965e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d46ea67-7f06-4cd3-b5dc-ad76c63372e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "726b164b-2229-47c4-b139-e947db30965e",
                    "LayerId": "7ccf38bb-7e0b-4c64-b0ef-5a6edb1fc6c3"
                }
            ]
        },
        {
            "id": "a8e89ada-5238-40dc-b0fa-a911d2cff13e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5355bacb-6fba-401b-b669-bba5f4e25216",
            "compositeImage": {
                "id": "41b0a2aa-d765-4236-bdab-f145de59d4da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8e89ada-5238-40dc-b0fa-a911d2cff13e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a34e5670-09c6-4ddb-b68f-65089a605162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8e89ada-5238-40dc-b0fa-a911d2cff13e",
                    "LayerId": "7ccf38bb-7e0b-4c64-b0ef-5a6edb1fc6c3"
                }
            ]
        },
        {
            "id": "7c07797b-740b-40d7-a363-d17985c0c057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5355bacb-6fba-401b-b669-bba5f4e25216",
            "compositeImage": {
                "id": "983c704d-75c0-479d-83a6-6ca551556e31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c07797b-740b-40d7-a363-d17985c0c057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "737430e1-bef1-41ce-a4b5-31a7c9932923",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c07797b-740b-40d7-a363-d17985c0c057",
                    "LayerId": "7ccf38bb-7e0b-4c64-b0ef-5a6edb1fc6c3"
                }
            ]
        },
        {
            "id": "2f2949e9-414e-4f52-95a0-262bb36a46fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5355bacb-6fba-401b-b669-bba5f4e25216",
            "compositeImage": {
                "id": "2a7fffbe-c13e-4114-a29d-bd2e812205ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f2949e9-414e-4f52-95a0-262bb36a46fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f476ce2-2fca-47aa-9b47-d4ba640d38a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f2949e9-414e-4f52-95a0-262bb36a46fa",
                    "LayerId": "7ccf38bb-7e0b-4c64-b0ef-5a6edb1fc6c3"
                }
            ]
        },
        {
            "id": "24ed8c8b-78a2-4924-875c-6ed64b7d27ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5355bacb-6fba-401b-b669-bba5f4e25216",
            "compositeImage": {
                "id": "130a10c9-f8a7-425b-a378-3d155dcef94f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24ed8c8b-78a2-4924-875c-6ed64b7d27ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04422773-e87a-4b1e-961e-3d8bb17add77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24ed8c8b-78a2-4924-875c-6ed64b7d27ef",
                    "LayerId": "7ccf38bb-7e0b-4c64-b0ef-5a6edb1fc6c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 124,
    "layers": [
        {
            "id": "7ccf38bb-7e0b-4c64-b0ef-5a6edb1fc6c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5355bacb-6fba-401b-b669-bba5f4e25216",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 195,
    "xorig": 97,
    "yorig": 62
}