{
    "id": "ebbe1d72-dd98-457a-b384-b48c81f2307e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprScrapIcon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 4,
    "bbox_right": 157,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "44c2ed4d-1a7f-4c24-9e55-9d13d2adf4c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebbe1d72-dd98-457a-b384-b48c81f2307e",
            "compositeImage": {
                "id": "25ea9dfe-8ff9-4b8d-b588-e845928b41c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44c2ed4d-1a7f-4c24-9e55-9d13d2adf4c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b707e93d-fd56-4d57-bedb-2bd2474a5ea4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44c2ed4d-1a7f-4c24-9e55-9d13d2adf4c1",
                    "LayerId": "ba77765d-5e83-4866-9f1a-4dc275ee9e88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 132,
    "layers": [
        {
            "id": "ba77765d-5e83-4866-9f1a-4dc275ee9e88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebbe1d72-dd98-457a-b384-b48c81f2307e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 161,
    "xorig": 80,
    "yorig": 66
}