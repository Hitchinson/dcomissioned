{
    "id": "fadf2d89-c634-41f2-91f6-d764370451aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerDeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 94,
    "bbox_right": 162,
    "bbox_top": 84,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2206c5f9-4d6f-4160-8c76-52064b9b5ab0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "9fd8f9aa-b72f-424f-949d-dccd864d5c72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2206c5f9-4d6f-4160-8c76-52064b9b5ab0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b027607-52ab-4205-90fd-a6124b6d07d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2206c5f9-4d6f-4160-8c76-52064b9b5ab0",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "bb32fcaf-bd37-4983-96ec-eac113e13c3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "1b47e688-71f8-4627-b482-6fd7440e0ca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb32fcaf-bd37-4983-96ec-eac113e13c3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd3b7b93-5cf5-4084-b325-aa1d021e4739",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb32fcaf-bd37-4983-96ec-eac113e13c3b",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "c102a2e7-4f74-4472-aedc-89898be75527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "8b5717e9-08b4-46a2-8f93-ae89e9a02f08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c102a2e7-4f74-4472-aedc-89898be75527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6ef8e89-51e8-4540-96ce-28bad902ac2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c102a2e7-4f74-4472-aedc-89898be75527",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "d3fa8522-043b-4961-877e-0db42f3e8d97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "43ee5194-0dbd-462e-8133-fc13afdc0433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3fa8522-043b-4961-877e-0db42f3e8d97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f5e4ef7-94b6-481a-a308-a65874c7bd4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3fa8522-043b-4961-877e-0db42f3e8d97",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "5b1bd72f-6ab8-4a61-b959-d5988a991361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "bfe99138-3772-4b83-a1ea-82ebd3e4c40b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b1bd72f-6ab8-4a61-b959-d5988a991361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10a99371-1ad7-4459-8f30-02b095276b6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b1bd72f-6ab8-4a61-b959-d5988a991361",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "de13cc62-2c4d-4d2f-93b3-98d2c27bb908",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "0c5eb9fa-9096-4200-a211-71f4512e0a1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de13cc62-2c4d-4d2f-93b3-98d2c27bb908",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29012485-92b0-4e42-8793-cc26adbfaf75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de13cc62-2c4d-4d2f-93b3-98d2c27bb908",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "be8899df-c915-4120-b1ab-53d3c2127068",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "77b24efd-6f31-4706-8860-5cf8b917a5af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be8899df-c915-4120-b1ab-53d3c2127068",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae7ce0d-550c-4978-bace-9c064e189812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be8899df-c915-4120-b1ab-53d3c2127068",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "56456421-f6e9-48c3-9b7d-64a1d23bb465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "4f1a471c-a5ac-4813-ad88-68255d706e18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56456421-f6e9-48c3-9b7d-64a1d23bb465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3d83ade-7b97-402f-a6bc-e76e009663cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56456421-f6e9-48c3-9b7d-64a1d23bb465",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "e38c0a85-b403-4879-9a9b-5eaffc660896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "e5a3c51b-4a44-4cd9-81e1-7da5a60d0f4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e38c0a85-b403-4879-9a9b-5eaffc660896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ccbf521-724e-4c8b-a172-1a3b95ba6561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e38c0a85-b403-4879-9a9b-5eaffc660896",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "b2dbec9c-2c68-4c6b-8460-d4f335d50537",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "a06f6297-8c9c-419b-bd00-fe0d5d282afd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2dbec9c-2c68-4c6b-8460-d4f335d50537",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3babd6bc-00f9-4145-a24a-1d558dba3fef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2dbec9c-2c68-4c6b-8460-d4f335d50537",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "20637d91-27d5-4cdb-9008-46aa799e8553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "e0b430d8-f4f0-4c08-9950-4992dcbd0822",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20637d91-27d5-4cdb-9008-46aa799e8553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e183774e-77db-4505-8208-c620e925b83c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20637d91-27d5-4cdb-9008-46aa799e8553",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "04e83db6-ef5e-42c6-97aa-3b100050140a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "de193a18-8caf-414c-9358-bbf824bafda3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04e83db6-ef5e-42c6-97aa-3b100050140a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d834f2d-c1a6-4d94-a404-e192f65a41e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04e83db6-ef5e-42c6-97aa-3b100050140a",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "ce3567b0-8637-49fa-959a-5b41ffcb4136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "aed7cc82-d841-44b5-9e2a-e0594d649506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce3567b0-8637-49fa-959a-5b41ffcb4136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "786a8fb7-2a2d-45a9-812d-13bef3627682",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce3567b0-8637-49fa-959a-5b41ffcb4136",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "d95ca1a1-30bf-4082-bfe8-dec3ffe63e5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "e7b30e5d-48fe-4f41-a663-1911f3da9aef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d95ca1a1-30bf-4082-bfe8-dec3ffe63e5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b883de5c-b0c8-4e14-9af0-ee5d116e9f5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d95ca1a1-30bf-4082-bfe8-dec3ffe63e5a",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "dc56aec2-2c5d-4259-b96f-a951cc577f7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "0bb23267-0f39-4b6b-8b73-1bbc9b14aaea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc56aec2-2c5d-4259-b96f-a951cc577f7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c766824-54fc-4a40-ae5b-2571628346e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc56aec2-2c5d-4259-b96f-a951cc577f7e",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "d6a07a7a-c08c-48a8-a51d-3f713f5ee891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "259539f5-6fe4-45fd-9412-1f76fa7abdfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6a07a7a-c08c-48a8-a51d-3f713f5ee891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13530ba8-375c-439c-92cc-0bcec155e820",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6a07a7a-c08c-48a8-a51d-3f713f5ee891",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "2e375bbf-a96d-4697-8220-24e5a3cdf2d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "f8c0f329-fc68-4ffc-bbf6-ffb931385dd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e375bbf-a96d-4697-8220-24e5a3cdf2d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e91cc85-206e-40a0-bbf1-71460c87805a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e375bbf-a96d-4697-8220-24e5a3cdf2d3",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "0496b7ff-d3ed-457b-8852-394b5bcca4f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "13139b31-dc22-4305-ad00-4d2a5d757176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0496b7ff-d3ed-457b-8852-394b5bcca4f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7adcc541-384c-464c-bdee-2737261fc673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0496b7ff-d3ed-457b-8852-394b5bcca4f5",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "277c4870-2bc1-483e-bcfb-95a69f6e70df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "68a21f4a-b26b-404a-b9e6-866a5f9c12ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "277c4870-2bc1-483e-bcfb-95a69f6e70df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9480650-9e2b-42e9-b0bc-a5da8bdf9e9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "277c4870-2bc1-483e-bcfb-95a69f6e70df",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "fbe57e25-ac9a-4221-ac47-f4e22ff96edb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "2f11988c-d2ea-48b0-a174-e6dea0eb729a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbe57e25-ac9a-4221-ac47-f4e22ff96edb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0aa9012f-91b1-4b14-9af0-30273182813a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbe57e25-ac9a-4221-ac47-f4e22ff96edb",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "b4899e2a-747e-49c6-aeeb-4c6d4746cea1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "ea317be8-6133-4909-983b-8c0a1b890fc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4899e2a-747e-49c6-aeeb-4c6d4746cea1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0161e367-4559-466e-889b-6960d194e896",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4899e2a-747e-49c6-aeeb-4c6d4746cea1",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "fd15cbe7-e4ae-4880-b3f1-ee88d9d86431",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "e3557e64-dcea-4d01-906e-17acfe300f75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd15cbe7-e4ae-4880-b3f1-ee88d9d86431",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95a52e58-1bea-4043-aa6d-dd1c60ba3209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd15cbe7-e4ae-4880-b3f1-ee88d9d86431",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "8c8d7011-307a-4ee0-9c33-aacd9b2aecef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "0f6b7e03-9e4b-41a7-9f07-ac2e3c3a00df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c8d7011-307a-4ee0-9c33-aacd9b2aecef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71358270-22d2-48dd-a296-0ad3bf38e3aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c8d7011-307a-4ee0-9c33-aacd9b2aecef",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "026849b4-9340-4e44-9595-a8825b547ab0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "61eefa41-38d4-4ccc-bf94-5555121f82ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "026849b4-9340-4e44-9595-a8825b547ab0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4643997-0514-4967-addd-2dad9f191cd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "026849b4-9340-4e44-9595-a8825b547ab0",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "f5ce3eae-9af6-497a-991e-c0c7bf1bfede",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "a4459c2f-e209-406e-abc3-6c109d4b180f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5ce3eae-9af6-497a-991e-c0c7bf1bfede",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9033f4ce-c150-4015-9088-913193c5abcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5ce3eae-9af6-497a-991e-c0c7bf1bfede",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "a21577cf-7ebf-477d-b5ce-8e32b692fe40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "e2d866f7-211f-4caa-a2fc-09b9f4729238",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a21577cf-7ebf-477d-b5ce-8e32b692fe40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22ffe1fa-a4bf-496a-abb7-6fd4dea7adc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a21577cf-7ebf-477d-b5ce-8e32b692fe40",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "bd1bb23d-758e-482b-8622-56f226dddb92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "544bba71-2ceb-407d-ad53-1a2681ae820a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd1bb23d-758e-482b-8622-56f226dddb92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4e121a3-5186-401c-917b-96a8d1c86d37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd1bb23d-758e-482b-8622-56f226dddb92",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "d4cf0890-abde-43ff-a445-8e2a8b9e0646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "0db007d2-0d4c-4072-bb0f-8b9e2a09ebde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4cf0890-abde-43ff-a445-8e2a8b9e0646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7ce2b4d-70c6-48e5-b275-38c67ddc631d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4cf0890-abde-43ff-a445-8e2a8b9e0646",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "1dd517dc-c730-45b8-9f99-dc2d33b16705",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "83c31aa3-1482-400c-9c7e-cb86a25974f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dd517dc-c730-45b8-9f99-dc2d33b16705",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5453584-9ef2-4e6d-9240-41d49c81ae4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dd517dc-c730-45b8-9f99-dc2d33b16705",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "222cd248-653f-4bfe-bcf9-e818a3387ed5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "b16f614b-3b84-4203-89c2-3ba0d32fa83a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "222cd248-653f-4bfe-bcf9-e818a3387ed5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e83f72a6-78f6-4e76-a59f-0236b9c5ea32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "222cd248-653f-4bfe-bcf9-e818a3387ed5",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "971ec33b-ec23-4d39-92d7-dfd3c6ba91c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "67176e9b-4c76-4b8b-929e-5bdca06d0362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "971ec33b-ec23-4d39-92d7-dfd3c6ba91c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c46f4d8-2668-4adf-9461-ef0ef6b934a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "971ec33b-ec23-4d39-92d7-dfd3c6ba91c4",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "83625aa5-25d4-4abe-abdf-d22a647e014a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "8a4c25f9-a9a3-4330-a49b-2cd71ceae3a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83625aa5-25d4-4abe-abdf-d22a647e014a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9a22090-baea-4d1f-a556-adfb391a9eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83625aa5-25d4-4abe-abdf-d22a647e014a",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "5a096a4d-12a1-4ef4-8096-34cadce0be7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "6873ae5f-1db8-4e89-bf92-912d89fb8342",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a096a4d-12a1-4ef4-8096-34cadce0be7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5669dc7d-7e85-4519-8826-a57c8b7c3b87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a096a4d-12a1-4ef4-8096-34cadce0be7f",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "4150fe78-f416-40c7-92b7-777427915371",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "90d8a60d-fad4-4544-8c0c-ffb8d4813f42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4150fe78-f416-40c7-92b7-777427915371",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf4c7e8c-7ed6-4f8b-914f-cd5792958872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4150fe78-f416-40c7-92b7-777427915371",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "e758d25f-7d3a-4ace-8d3b-b9e4afa2638a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "0f186818-fd64-4cff-acf5-fa83347bf697",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e758d25f-7d3a-4ace-8d3b-b9e4afa2638a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bb9bcd7-21f3-4661-bf29-55edcbb456e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e758d25f-7d3a-4ace-8d3b-b9e4afa2638a",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "2e8b7b26-b7da-4158-ba61-3028bf5ba472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "9d635885-9a56-463d-9ea8-f23ed8823033",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e8b7b26-b7da-4158-ba61-3028bf5ba472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89116d92-9118-40da-b677-d6436c999a08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e8b7b26-b7da-4158-ba61-3028bf5ba472",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "82deb1a5-4110-48fb-aefc-a71d994672bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "8b83dc9f-c785-464b-8f4e-33e3b2e4e32f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82deb1a5-4110-48fb-aefc-a71d994672bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7deab419-0ec7-4c06-b002-4b56ba93fd1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82deb1a5-4110-48fb-aefc-a71d994672bd",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "37fe1c7b-6c60-4139-a3c7-9f0e524fa35b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "0d2f3643-d46a-4655-94c2-3b309b3592d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37fe1c7b-6c60-4139-a3c7-9f0e524fa35b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed7fbfde-222e-4c0d-b1f5-6a09947dd2cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37fe1c7b-6c60-4139-a3c7-9f0e524fa35b",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "13385c74-1441-4bc3-adea-a724ab0cfb79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "b9ef4d2b-7de9-478b-a5c4-1211f2f990e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13385c74-1441-4bc3-adea-a724ab0cfb79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97fe05a9-99d4-40eb-ae72-c9a2f69d185f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13385c74-1441-4bc3-adea-a724ab0cfb79",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "03784f47-ce03-4232-92f5-685ea739dc72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "cd6ab354-eb62-4b12-a79a-99b0c1ee9a3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03784f47-ce03-4232-92f5-685ea739dc72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d57d801-7a71-44e9-b487-d947349845d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03784f47-ce03-4232-92f5-685ea739dc72",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "4a6a9828-a677-4b14-8898-28e21e87fcbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "01fa17d5-ed04-4fe4-afff-fbb91a237f8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a6a9828-a677-4b14-8898-28e21e87fcbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8e8a232-8a41-4de7-b9bc-5afcadd296ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a6a9828-a677-4b14-8898-28e21e87fcbe",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "ab048d91-94a0-4064-802b-cf03a26a22f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "1b1bd613-54dc-4679-b65a-4ca3db68b7a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab048d91-94a0-4064-802b-cf03a26a22f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a865da8-772e-4d95-bd79-ea9ac326897e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab048d91-94a0-4064-802b-cf03a26a22f0",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "bdd78b3c-1cf0-443d-ba57-5d75d2fb5209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "87aeea82-eef8-4ec6-9232-0e2eba8bd368",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdd78b3c-1cf0-443d-ba57-5d75d2fb5209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e16deb2-3753-45ea-bd2c-f9e546e8c951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdd78b3c-1cf0-443d-ba57-5d75d2fb5209",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        },
        {
            "id": "ad871e6e-4be2-4331-a036-809adba72e9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "compositeImage": {
                "id": "23621ea0-93de-4386-9519-6b256ba38aeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad871e6e-4be2-4331-a036-809adba72e9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "344f2b8c-896a-4dfe-8f13-8912cda4a1ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad871e6e-4be2-4331-a036-809adba72e9a",
                    "LayerId": "71f3788c-518f-469f-bfb3-923910001db2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "71f3788c-518f-469f-bfb3-923910001db2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fadf2d89-c634-41f2-91f6-d764370451aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 247,
    "xorig": 123,
    "yorig": 75
}