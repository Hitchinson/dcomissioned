{
    "id": "1da28b02-7052-423f-bb96-3a7bc3b39fc5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_energyFiller",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 315,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "934f61e4-60f2-4ab0-a229-22e271b609d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1da28b02-7052-423f-bb96-3a7bc3b39fc5",
            "compositeImage": {
                "id": "0c1303a2-4fdf-4a5f-97f1-9db3a42ae6db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "934f61e4-60f2-4ab0-a229-22e271b609d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9b9605e-46c8-4f1b-8233-823063c77365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "934f61e4-60f2-4ab0-a229-22e271b609d4",
                    "LayerId": "389895ae-ff69-447b-abc3-342fceb70ba7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "389895ae-ff69-447b-abc3-342fceb70ba7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1da28b02-7052-423f-bb96-3a7bc3b39fc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 316,
    "xorig": 0,
    "yorig": 0
}