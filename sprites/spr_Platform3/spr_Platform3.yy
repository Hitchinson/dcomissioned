{
    "id": "874e86eb-2fd7-4e45-8104-819cfc1fd71f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Platform3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 169,
    "bbox_left": 0,
    "bbox_right": 561,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0a2fe1d3-8129-423b-a623-ebf86c6fc4b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "874e86eb-2fd7-4e45-8104-819cfc1fd71f",
            "compositeImage": {
                "id": "7607d623-22f3-4262-b0cd-d5de57eb4a2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a2fe1d3-8129-423b-a623-ebf86c6fc4b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfbd2f23-45da-4e92-affd-5235f01573ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a2fe1d3-8129-423b-a623-ebf86c6fc4b4",
                    "LayerId": "b9846551-03af-4358-9c4b-64fb96fd659b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 170,
    "layers": [
        {
            "id": "b9846551-03af-4358-9c4b-64fb96fd659b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "874e86eb-2fd7-4e45-8104-819cfc1fd71f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 562,
    "xorig": 281,
    "yorig": 85
}