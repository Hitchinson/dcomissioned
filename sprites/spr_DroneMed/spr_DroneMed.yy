{
    "id": "a2c92fbd-ce27-4586-9611-d3bcb9a68046",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_DroneMed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 2,
    "bbox_right": 64,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f6cc4e9b-a2e6-4acd-8a97-53ab5933d93f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2c92fbd-ce27-4586-9611-d3bcb9a68046",
            "compositeImage": {
                "id": "9a5e59d1-cc1e-484a-bc56-194764aa5fa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6cc4e9b-a2e6-4acd-8a97-53ab5933d93f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37f711b2-e2bd-41bf-9212-6c9efc1c16c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6cc4e9b-a2e6-4acd-8a97-53ab5933d93f",
                    "LayerId": "9400b087-6c61-4520-8abe-dc3fb89f3f0d"
                }
            ]
        },
        {
            "id": "cc69d2c9-4637-40cc-b18a-7ee42239296b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2c92fbd-ce27-4586-9611-d3bcb9a68046",
            "compositeImage": {
                "id": "ee64b939-18af-400c-a9ae-ab26cd35db56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc69d2c9-4637-40cc-b18a-7ee42239296b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "595ffb2b-9bcb-4063-9b69-b7b323a9b019",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc69d2c9-4637-40cc-b18a-7ee42239296b",
                    "LayerId": "9400b087-6c61-4520-8abe-dc3fb89f3f0d"
                }
            ]
        },
        {
            "id": "b1d22487-23ec-4bf0-ad5c-fa5019c2ffcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2c92fbd-ce27-4586-9611-d3bcb9a68046",
            "compositeImage": {
                "id": "a41d4448-c836-4e34-8a19-9c9e04eb6f35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1d22487-23ec-4bf0-ad5c-fa5019c2ffcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c438dad1-89dd-4b5d-b618-b5a5546f2a8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1d22487-23ec-4bf0-ad5c-fa5019c2ffcc",
                    "LayerId": "9400b087-6c61-4520-8abe-dc3fb89f3f0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "9400b087-6c61-4520-8abe-dc3fb89f3f0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2c92fbd-ce27-4586-9611-d3bcb9a68046",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 34,
    "yorig": 23
}