{
    "id": "1ab6678e-d4b1-4e51-9bcb-48492e62a4e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fuelFiller",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 315,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7c013123-c12c-43c5-bf5f-8b3b512077c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ab6678e-d4b1-4e51-9bcb-48492e62a4e8",
            "compositeImage": {
                "id": "8dbf5fb0-d8c2-4cc1-afa5-ac5cffe95de0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c013123-c12c-43c5-bf5f-8b3b512077c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90fa4e46-8407-47c3-bd41-6c1f277d9b6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c013123-c12c-43c5-bf5f-8b3b512077c8",
                    "LayerId": "7fc8a0dc-435f-4717-8d8c-eab34becea5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "7fc8a0dc-435f-4717-8d8c-eab34becea5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ab6678e-d4b1-4e51-9bcb-48492e62a4e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 316,
    "xorig": 0,
    "yorig": 0
}