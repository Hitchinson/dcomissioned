{
    "id": "d53b2362-44ba-4e6a-ac1c-4ef2d8daa610",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GUIBars",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 169,
    "bbox_left": 21,
    "bbox_right": 336,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0831504d-828e-4cd8-84fa-2383fbf015c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d53b2362-44ba-4e6a-ac1c-4ef2d8daa610",
            "compositeImage": {
                "id": "b669d720-d54e-42e7-8a20-4926561a430d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0831504d-828e-4cd8-84fa-2383fbf015c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "547d7aec-7760-465c-a284-14aef92bfe4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0831504d-828e-4cd8-84fa-2383fbf015c2",
                    "LayerId": "09c5ac7f-44c9-4d4a-b9f8-ffc88e7d9114"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "09c5ac7f-44c9-4d4a-b9f8-ffc88e7d9114",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d53b2362-44ba-4e6a-ac1c-4ef2d8daa610",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}