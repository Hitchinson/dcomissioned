{
    "id": "9617fe78-6def-4935-8c53-7ac621d7269d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Platform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 318,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1f49e365-6b20-4b3e-8128-c675fb09246c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9617fe78-6def-4935-8c53-7ac621d7269d",
            "compositeImage": {
                "id": "e66fc739-03b3-40fc-80f4-d6bc6890d77a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f49e365-6b20-4b3e-8128-c675fb09246c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "569ee7e4-ae76-4189-885f-c203307f2c26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f49e365-6b20-4b3e-8128-c675fb09246c",
                    "LayerId": "acd3cce8-e7e8-4f3e-940d-5458ac75f15e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "acd3cce8-e7e8-4f3e-940d-5458ac75f15e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9617fe78-6def-4935-8c53-7ac621d7269d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 319,
    "xorig": 159,
    "yorig": 22
}