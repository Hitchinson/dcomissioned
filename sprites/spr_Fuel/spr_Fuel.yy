{
    "id": "fa0c36d2-2fb6-42d6-8edd-5b330dea93aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Fuel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 148,
    "bbox_left": 0,
    "bbox_right": 119,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9ff9283e-6722-47e5-ae17-6c0838f2f56a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa0c36d2-2fb6-42d6-8edd-5b330dea93aa",
            "compositeImage": {
                "id": "b32acb6c-5a9e-4355-b02e-e482d39de847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ff9283e-6722-47e5-ae17-6c0838f2f56a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48a56d20-4b6d-45d7-ab4b-8386ba58d640",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ff9283e-6722-47e5-ae17-6c0838f2f56a",
                    "LayerId": "2bb4b37b-719a-4627-be5a-3026fb9a5a85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 149,
    "layers": [
        {
            "id": "2bb4b37b-719a-4627-be5a-3026fb9a5a85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa0c36d2-2fb6-42d6-8edd-5b330dea93aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 60,
    "yorig": 74
}