{
    "id": "32a51603-5dd2-4e06-b7fe-576ab79f6588",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWorkstationRunning",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "33bd1cf4-980b-43d4-918d-422f6687b56f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32a51603-5dd2-4e06-b7fe-576ab79f6588",
            "compositeImage": {
                "id": "822c1550-0659-4ced-a95c-505963f258d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33bd1cf4-980b-43d4-918d-422f6687b56f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4728e3e-dcdc-49e7-9304-9b06bac0b33e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33bd1cf4-980b-43d4-918d-422f6687b56f",
                    "LayerId": "e5dd81c8-31dd-4220-8c2e-e66e224eba8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "e5dd81c8-31dd-4220-8c2e-e66e224eba8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32a51603-5dd2-4e06-b7fe-576ab79f6588",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}