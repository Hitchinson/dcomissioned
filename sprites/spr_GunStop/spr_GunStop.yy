{
    "id": "d8c5278b-1959-43fb-a32e-4b51a2fd204a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GunStop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 3,
    "bbox_right": 76,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b5eeed4d-5d3a-4859-ad12-b4a50539a9d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8c5278b-1959-43fb-a32e-4b51a2fd204a",
            "compositeImage": {
                "id": "e3ce5101-49f9-4f4e-bf1e-161048786afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5eeed4d-5d3a-4859-ad12-b4a50539a9d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29f5989c-8612-4055-b4e6-30bfb261026c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5eeed4d-5d3a-4859-ad12-b4a50539a9d7",
                    "LayerId": "293e01d3-0920-49c3-b990-4c61676288b7"
                }
            ]
        },
        {
            "id": "915a7a2a-666f-471d-b8be-384b1839bfee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8c5278b-1959-43fb-a32e-4b51a2fd204a",
            "compositeImage": {
                "id": "ecc64dc8-4b3f-4ddf-9f7c-35f186acd844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "915a7a2a-666f-471d-b8be-384b1839bfee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49218e86-b624-4475-a3b6-6a8e21b5399c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "915a7a2a-666f-471d-b8be-384b1839bfee",
                    "LayerId": "293e01d3-0920-49c3-b990-4c61676288b7"
                }
            ]
        },
        {
            "id": "6aeef549-a19a-4caa-a457-6505daf57dee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8c5278b-1959-43fb-a32e-4b51a2fd204a",
            "compositeImage": {
                "id": "22465088-8fcc-4b59-8c43-f18057285e86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aeef549-a19a-4caa-a457-6505daf57dee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dec76a7-2211-4b20-8cf3-fdddf8aab74a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aeef549-a19a-4caa-a457-6505daf57dee",
                    "LayerId": "293e01d3-0920-49c3-b990-4c61676288b7"
                }
            ]
        },
        {
            "id": "48a8a54e-1fcd-4aaa-ba45-f8498cbd01fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8c5278b-1959-43fb-a32e-4b51a2fd204a",
            "compositeImage": {
                "id": "a6445aa6-19ff-43dc-867c-678547686bc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48a8a54e-1fcd-4aaa-ba45-f8498cbd01fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "108b6582-a763-4786-b9c0-b8a2bb23e1c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48a8a54e-1fcd-4aaa-ba45-f8498cbd01fc",
                    "LayerId": "293e01d3-0920-49c3-b990-4c61676288b7"
                }
            ]
        },
        {
            "id": "937d4b29-6d3f-4487-8dda-26c281cefc11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8c5278b-1959-43fb-a32e-4b51a2fd204a",
            "compositeImage": {
                "id": "a4a31307-2ff7-463f-8df1-08bcc228236e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "937d4b29-6d3f-4487-8dda-26c281cefc11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6490ac8-fae2-46b7-b699-dead256ffdf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "937d4b29-6d3f-4487-8dda-26c281cefc11",
                    "LayerId": "293e01d3-0920-49c3-b990-4c61676288b7"
                }
            ]
        },
        {
            "id": "20725a20-79f5-4757-9f3b-9131bf52a82c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8c5278b-1959-43fb-a32e-4b51a2fd204a",
            "compositeImage": {
                "id": "a0f7c644-f5d8-4d04-a78f-0a9abce3cadf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20725a20-79f5-4757-9f3b-9131bf52a82c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fabdef1f-45cd-49f0-aba6-736bda473ed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20725a20-79f5-4757-9f3b-9131bf52a82c",
                    "LayerId": "293e01d3-0920-49c3-b990-4c61676288b7"
                }
            ]
        },
        {
            "id": "ebe79592-6c0e-4713-953d-7be99b11be69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8c5278b-1959-43fb-a32e-4b51a2fd204a",
            "compositeImage": {
                "id": "e9a3b405-f380-488a-bd98-bbc7cbab42aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebe79592-6c0e-4713-953d-7be99b11be69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dd1db03-0c44-407e-ba8f-1b28ad2a8a48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebe79592-6c0e-4713-953d-7be99b11be69",
                    "LayerId": "293e01d3-0920-49c3-b990-4c61676288b7"
                }
            ]
        },
        {
            "id": "e76b761d-bd44-49e0-8133-2d8efe575eb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8c5278b-1959-43fb-a32e-4b51a2fd204a",
            "compositeImage": {
                "id": "704988de-5151-4fef-b85c-8013652d9d30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e76b761d-bd44-49e0-8133-2d8efe575eb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f92dfc30-aab1-489c-a36a-5a156f982a76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e76b761d-bd44-49e0-8133-2d8efe575eb4",
                    "LayerId": "293e01d3-0920-49c3-b990-4c61676288b7"
                }
            ]
        },
        {
            "id": "ab7cb93f-25b0-4ede-8fca-efd57aaf4ee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8c5278b-1959-43fb-a32e-4b51a2fd204a",
            "compositeImage": {
                "id": "ed1dadbb-e4d1-4098-94aa-952039486fc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab7cb93f-25b0-4ede-8fca-efd57aaf4ee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "311e41ff-3118-491f-a579-f3f3543908bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab7cb93f-25b0-4ede-8fca-efd57aaf4ee8",
                    "LayerId": "293e01d3-0920-49c3-b990-4c61676288b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 74,
    "layers": [
        {
            "id": "293e01d3-0920-49c3-b990-4c61676288b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8c5278b-1959-43fb-a32e-4b51a2fd204a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 37
}