{
    "id": "8fb4db54-628d-4a5e-96f2-2b67aaf87626",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_healthFiller",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 315,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "aaac7533-3e8f-4843-984a-0e74996e8c4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb4db54-628d-4a5e-96f2-2b67aaf87626",
            "compositeImage": {
                "id": "e978fc62-e070-4ab6-9e89-367e500a0dd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaac7533-3e8f-4843-984a-0e74996e8c4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae01bdd4-bbe6-476e-a5b3-eae26bda1573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaac7533-3e8f-4843-984a-0e74996e8c4b",
                    "LayerId": "adb204ed-34a1-4664-bfe4-cbcae468849a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "adb204ed-34a1-4664-bfe4-cbcae468849a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fb4db54-628d-4a5e-96f2-2b67aaf87626",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 316,
    "xorig": 0,
    "yorig": 0
}