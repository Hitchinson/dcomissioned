{
    "id": "4c2461b9-ebca-47ef-afa2-cc4482cdf2f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_End_TEMP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8fd4c0b9-41ab-4ebf-8c35-ee83d7143d3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c2461b9-ebca-47ef-afa2-cc4482cdf2f0",
            "compositeImage": {
                "id": "8b956aaf-60e9-4dc9-a09c-f6fd2e9553eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd4c0b9-41ab-4ebf-8c35-ee83d7143d3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f07e9b77-2de6-4526-8dd5-27b92d838c86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd4c0b9-41ab-4ebf-8c35-ee83d7143d3d",
                    "LayerId": "09242ad1-8a29-475c-bd9a-52f4f7cb17f4"
                }
            ]
        }
    ],
    "gridX": 100,
    "gridY": 100,
    "height": 1080,
    "layers": [
        {
            "id": "09242ad1-8a29-475c-bd9a-52f4f7cb17f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c2461b9-ebca-47ef-afa2-cc4482cdf2f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 24,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 960,
    "yorig": 540
}