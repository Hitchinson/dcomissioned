{
    "id": "76a891ae-eac3-442e-a4d8-110b460d0b68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 6,
    "bbox_right": 186,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e0d8bf2e-7685-408e-9f10-68e151d8d6f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "7d8aa686-55a1-4319-b206-1477cb4042e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0d8bf2e-7685-408e-9f10-68e151d8d6f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5609f44f-2e80-4e08-accf-285c91547eb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0d8bf2e-7685-408e-9f10-68e151d8d6f7",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "c4ba50ef-5cb9-4374-a88c-677ab1f49ae9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "cbf9e28a-6571-4994-8cf3-0e22a7207cab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4ba50ef-5cb9-4374-a88c-677ab1f49ae9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b4cd2a8-6a6a-4cd3-898e-5fe090aa8103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4ba50ef-5cb9-4374-a88c-677ab1f49ae9",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "ab263d1a-a5c9-47d5-b7b6-8ca44bc5ce60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "cec33cbc-76ad-4f5e-9fd9-bd6085b20ef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab263d1a-a5c9-47d5-b7b6-8ca44bc5ce60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a60e58f2-bbed-47e3-9990-8630daf20d1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab263d1a-a5c9-47d5-b7b6-8ca44bc5ce60",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "cace4a67-12af-44d1-9dc9-afe9e4ed50fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "e489b5db-025c-46ea-8342-a80c2a35dd48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cace4a67-12af-44d1-9dc9-afe9e4ed50fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba442c60-71c4-42c8-ad9b-5cf3924a74e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cace4a67-12af-44d1-9dc9-afe9e4ed50fa",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "0c6751bf-ef22-4cf0-ba83-097ec722915a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "f83c4245-611c-42e3-9939-9cd6acaaa67a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c6751bf-ef22-4cf0-ba83-097ec722915a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afe1a5ce-390f-4432-b19e-a29844cdcda2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c6751bf-ef22-4cf0-ba83-097ec722915a",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "90771359-fb1e-43e8-a535-b5a9129dd487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "5640cfe7-0c12-4a3f-b52a-f1161398eae5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90771359-fb1e-43e8-a535-b5a9129dd487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c0f3994-fe62-4f66-83bc-97022d0b6efd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90771359-fb1e-43e8-a535-b5a9129dd487",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "0b6f8702-dfdd-46f3-8846-6463f536e243",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "d784c07a-dd5e-4bc5-9f7a-8e8e4f8d1b49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b6f8702-dfdd-46f3-8846-6463f536e243",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e0c1888-d4c3-4c23-81be-2fb5ce874516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b6f8702-dfdd-46f3-8846-6463f536e243",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "5ff63b11-633c-4cda-87e1-7c570f55fac2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "5d039cca-5f67-4a46-9011-8c317834e9a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ff63b11-633c-4cda-87e1-7c570f55fac2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f388410-5336-4d3d-8345-17f40b42ccac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ff63b11-633c-4cda-87e1-7c570f55fac2",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "0253a977-6e86-471c-9e0a-4b2766d321f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "d4c15ef7-0ce4-427e-ab8d-0cd972233336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0253a977-6e86-471c-9e0a-4b2766d321f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e13cefde-9093-4f92-ba1e-86fd872684d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0253a977-6e86-471c-9e0a-4b2766d321f7",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "886518fd-7482-44be-aea4-7426e9fbd9bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "825c44d9-57a9-4ff9-b4a0-9780adb0a242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "886518fd-7482-44be-aea4-7426e9fbd9bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2c9f698-6716-4e25-b6a6-cbce03572b62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "886518fd-7482-44be-aea4-7426e9fbd9bb",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "8d1fa075-3dd4-4a17-bffb-7273f3afc0bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "533c211f-4d14-43b8-8a3d-2ec6c0fec9ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d1fa075-3dd4-4a17-bffb-7273f3afc0bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bdd0860-3f8b-4ae7-978d-35b00a9bd1ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d1fa075-3dd4-4a17-bffb-7273f3afc0bf",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "31a4f9e9-c71d-4b89-9ebc-40509ee912ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "396e7217-ca35-408b-a8cd-125f1affe74f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31a4f9e9-c71d-4b89-9ebc-40509ee912ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9b02b7a-a3d3-4b39-bcec-de825aa39ff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31a4f9e9-c71d-4b89-9ebc-40509ee912ae",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "3072da12-b400-4094-a3f1-ebfca54fdeb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "4ae0115a-2baa-421e-b6b3-1221714bd606",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3072da12-b400-4094-a3f1-ebfca54fdeb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e956f02-e886-46ee-8880-3869428c6fa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3072da12-b400-4094-a3f1-ebfca54fdeb9",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "8b6016f6-6897-4de6-a168-e9d4dc525a83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "e0f2b941-c440-44e8-ab65-4ba447eef2c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b6016f6-6897-4de6-a168-e9d4dc525a83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ede7e8ca-1bb1-45ad-8518-108a7477c744",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b6016f6-6897-4de6-a168-e9d4dc525a83",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "0ec86b14-3255-4d9d-9128-65c62622cd66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "79e52826-45a9-4f60-91a9-b1c4a40ac476",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ec86b14-3255-4d9d-9128-65c62622cd66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84999651-d8aa-431a-8a6a-45c63eb7d185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ec86b14-3255-4d9d-9128-65c62622cd66",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "69ae93e1-be0c-4219-a786-97667d22e82d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "e5da36b0-aa69-4f2b-a3b5-d2a479785a5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69ae93e1-be0c-4219-a786-97667d22e82d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8446067f-e414-4cdb-8cc4-8795bd487885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69ae93e1-be0c-4219-a786-97667d22e82d",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "04990e3b-cce3-41ec-8f85-10bb48cd2715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "5c4fc466-3b97-4c25-9b68-093d6801d105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04990e3b-cce3-41ec-8f85-10bb48cd2715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf742c15-a4e6-4fdf-bbb3-d5fcf190002a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04990e3b-cce3-41ec-8f85-10bb48cd2715",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "d2cdf2b9-4d2c-4075-ab2a-11f34ed760c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "90483a6c-32fc-4269-a18e-5a742ff2d597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2cdf2b9-4d2c-4075-ab2a-11f34ed760c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "030f4da2-5a5d-4e7d-ad19-99893077e48f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2cdf2b9-4d2c-4075-ab2a-11f34ed760c7",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "827990e8-f886-4f21-9ab0-cc654ffc900f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "82946405-baa5-4b68-b120-cef98376247d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "827990e8-f886-4f21-9ab0-cc654ffc900f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35364dff-1c7c-4e71-8328-239ecfaece62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "827990e8-f886-4f21-9ab0-cc654ffc900f",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "609adb78-2d00-4d73-978e-9b76c38084db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "718055ef-0b99-4dd9-acc7-46ce44042360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "609adb78-2d00-4d73-978e-9b76c38084db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a165e52-db53-42b3-b974-83b2725e563e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "609adb78-2d00-4d73-978e-9b76c38084db",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "59376e52-b94a-445a-bed7-da366e0a9627",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "29ea2afd-15f0-4d86-a29f-010050c38cec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59376e52-b94a-445a-bed7-da366e0a9627",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04f4fc75-2f0a-4a41-b63f-bd3376da0ad1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59376e52-b94a-445a-bed7-da366e0a9627",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "38eba3d0-631b-4c68-a376-216ebd61af4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "f32f83ac-14fc-48ae-ba22-a297889960b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38eba3d0-631b-4c68-a376-216ebd61af4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1641c28-8a22-43fe-8c54-e63d8b5d76ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38eba3d0-631b-4c68-a376-216ebd61af4a",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "43dc16c2-12e0-4526-91e1-9193a70964e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "946e660b-5760-4ab7-b711-ddb2d03afb4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43dc16c2-12e0-4526-91e1-9193a70964e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "512220f7-f2b5-45f1-9f4a-062f3d73cba6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43dc16c2-12e0-4526-91e1-9193a70964e4",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "bea039e0-12c9-4cb4-bbae-fa8e7c92cf45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "4b200f81-c26b-4f36-a7f6-e4c89d97325b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bea039e0-12c9-4cb4-bbae-fa8e7c92cf45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e16262e9-f1e7-4156-b6e0-b582ea66c380",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bea039e0-12c9-4cb4-bbae-fa8e7c92cf45",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "d64d9db1-e988-46cc-ac51-558ce332757a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "dbdf55b4-e298-43e0-9305-39504868df71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d64d9db1-e988-46cc-ac51-558ce332757a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "900bd335-b348-43b4-ac43-1e3908ed10ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d64d9db1-e988-46cc-ac51-558ce332757a",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "5a893781-3b46-45a9-aafc-db191eb44074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "437245fe-723d-47a7-823f-01aefb90c847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a893781-3b46-45a9-aafc-db191eb44074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7423e28a-4331-487f-b8ba-3c1fe556815b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a893781-3b46-45a9-aafc-db191eb44074",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "e43b942c-5612-4fd5-8644-526bdb4a5c8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "7e4b4c83-930c-4ca1-af58-2a2096574b5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e43b942c-5612-4fd5-8644-526bdb4a5c8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "799a1608-c45b-4cb4-acf1-0d3a6da2377e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e43b942c-5612-4fd5-8644-526bdb4a5c8e",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "3adac1f8-d3aa-4ec3-9ac7-8cc9f72f4a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "d97ad542-c7c4-4749-a688-0fcf9b79f919",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3adac1f8-d3aa-4ec3-9ac7-8cc9f72f4a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "916e1d0c-dd83-489f-a013-e35531789ebe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3adac1f8-d3aa-4ec3-9ac7-8cc9f72f4a86",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "df75a15e-939d-4661-ade2-db74cb0aaccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "4d7a6f0c-2ddb-4ca6-89cd-e7e9a3aaa22e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df75a15e-939d-4661-ade2-db74cb0aaccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ba5cbc4-c66f-4d12-8a85-1930bc69170f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df75a15e-939d-4661-ade2-db74cb0aaccf",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        },
        {
            "id": "9b483310-795b-4e11-9e0b-dbfab2fe47d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "compositeImage": {
                "id": "a4f5ad6f-64ef-4582-8f57-fe28ab14258a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b483310-795b-4e11-9e0b-dbfab2fe47d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8673c4ba-4242-4fe8-9b56-d74706f4ca66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b483310-795b-4e11-9e0b-dbfab2fe47d9",
                    "LayerId": "459aeb77-1854-4c15-ac16-1323aa674455"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 121,
    "layers": [
        {
            "id": "459aeb77-1854-4c15-ac16-1323aa674455",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76a891ae-eac3-442e-a4d8-110b460d0b68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 188,
    "xorig": 94,
    "yorig": 60
}