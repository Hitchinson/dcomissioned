{
    "id": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWorkstationStartup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ab045f52-8f34-4d3f-8fb0-7b8cd7c70042",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "e4b91414-d767-49a8-a2c2-709ae82a1ff8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab045f52-8f34-4d3f-8fb0-7b8cd7c70042",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc919cf5-22b3-41d8-8d7a-f584e0620918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab045f52-8f34-4d3f-8fb0-7b8cd7c70042",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "ca315723-042c-484c-8c69-c5928a289193",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "c67f6040-af98-45ef-b8cc-33c11cebd0eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca315723-042c-484c-8c69-c5928a289193",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "684e4217-aff8-452e-9814-45b39c17ee5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca315723-042c-484c-8c69-c5928a289193",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "4274b030-a220-4efe-aa82-1129303075d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "386a3e85-9100-41d0-aa03-8b5b5a0ed718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4274b030-a220-4efe-aa82-1129303075d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26f50f14-a3ca-46e5-9edb-217918a5b537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4274b030-a220-4efe-aa82-1129303075d4",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "0e1e5129-9448-42e2-936f-18efff61fc58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "6ae05c51-698a-47fb-b4e1-7e2aec6880fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e1e5129-9448-42e2-936f-18efff61fc58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d00a681e-b595-4b72-89fa-1d9a5e123b0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e1e5129-9448-42e2-936f-18efff61fc58",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "82355800-2417-4bd5-9a19-961dabd31013",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "1ae958b4-c51c-4f8e-bf0a-4262bc1601ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82355800-2417-4bd5-9a19-961dabd31013",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce8b85c5-3ec3-4843-a690-7d2ab0ca803e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82355800-2417-4bd5-9a19-961dabd31013",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "9459cacd-5e88-4e04-b934-3a8ad2ed7621",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "58a03ed1-c53f-489b-ab06-c7842bb076b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9459cacd-5e88-4e04-b934-3a8ad2ed7621",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7697ecca-41d4-400d-a135-e7fc32b7f392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9459cacd-5e88-4e04-b934-3a8ad2ed7621",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "1d3f39ec-3633-4cdf-8c27-6e323b5092d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "8b1f2e33-1941-45ca-ae0c-ca385248003e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d3f39ec-3633-4cdf-8c27-6e323b5092d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a69e341b-6a5e-47ac-b607-23f74fd7fdf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d3f39ec-3633-4cdf-8c27-6e323b5092d1",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "d3522885-e147-4dd9-b285-18fb641912b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "0be2aec8-5cf4-410b-8897-fb3a89e725b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3522885-e147-4dd9-b285-18fb641912b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abff8f64-5a3c-4389-a422-28b4794ae429",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3522885-e147-4dd9-b285-18fb641912b3",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "d76debfe-c719-44b6-b326-80caf5f43b43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "8b963366-83a6-45ee-88b2-109735a4bbea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d76debfe-c719-44b6-b326-80caf5f43b43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed56bf07-8ed6-404c-9a11-bb0d482c13e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d76debfe-c719-44b6-b326-80caf5f43b43",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "519ac199-e13a-4a26-a122-9ccca90bde8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "db893baf-5a4f-4526-8759-7997ce0e0d8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "519ac199-e13a-4a26-a122-9ccca90bde8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de42068a-9e24-4280-82cf-fedd649c31ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "519ac199-e13a-4a26-a122-9ccca90bde8c",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "749448cb-43d6-41db-8357-0dc457ebe8c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "e4ef9965-164f-4026-a113-99cf99decf19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "749448cb-43d6-41db-8357-0dc457ebe8c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baf8794d-4b1a-42fc-88ef-f617a5fdefb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "749448cb-43d6-41db-8357-0dc457ebe8c1",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "3c9cdfba-c4e8-4a74-9964-b7a2f39670d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "d84f4998-b93c-4fc6-a9e0-1f302f26efa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c9cdfba-c4e8-4a74-9964-b7a2f39670d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07021201-48d1-4294-a584-2e2ef5d11e89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c9cdfba-c4e8-4a74-9964-b7a2f39670d5",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "3bc1223b-c8c5-4171-9644-01d1b28c7943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "7b56357f-f465-4509-ace1-a39bb11aace3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bc1223b-c8c5-4171-9644-01d1b28c7943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5254fd77-56fa-4378-b7d2-716343239fe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bc1223b-c8c5-4171-9644-01d1b28c7943",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "3ea9e119-f3f3-4e69-b54b-f62a7987bd76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "f37e8cf6-4336-4b64-aed5-925b94350ee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ea9e119-f3f3-4e69-b54b-f62a7987bd76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a98f42fc-6903-408b-b880-c2ef4058a7f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ea9e119-f3f3-4e69-b54b-f62a7987bd76",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "c9027de9-f5e9-4f5b-99d6-0f3c7855e6f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "0912ab69-ef5f-4a57-a689-534317b504ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9027de9-f5e9-4f5b-99d6-0f3c7855e6f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "618eea5d-1668-41bf-a1ba-63d796478b87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9027de9-f5e9-4f5b-99d6-0f3c7855e6f3",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "a0e897d0-ee6c-4721-a9ba-8a9c9acb49d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "7eeed370-9ba8-481d-8229-b61fed736c37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0e897d0-ee6c-4721-a9ba-8a9c9acb49d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5067c34-e700-49f1-9eac-4c12a861084e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0e897d0-ee6c-4721-a9ba-8a9c9acb49d5",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "8cc2023b-6b11-4ec9-8441-3f518a89db12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "6f7fbb21-ccd2-45cf-b79f-fe9a548b3b73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cc2023b-6b11-4ec9-8441-3f518a89db12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f18840e6-730b-48e5-9d7f-ade92477a86d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cc2023b-6b11-4ec9-8441-3f518a89db12",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "1e364d62-720b-47a4-81d9-4fd4a5918f85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "a95e11e6-5571-49a2-ae2f-ea8f2c769297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e364d62-720b-47a4-81d9-4fd4a5918f85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5886eef0-9ef9-4c08-a0f7-a5666e5e840a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e364d62-720b-47a4-81d9-4fd4a5918f85",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "b3841cf8-a6d5-4e28-9d44-82616713a7fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "8c8e6735-1368-4424-9221-913714de7436",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3841cf8-a6d5-4e28-9d44-82616713a7fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f107a6f2-3296-4204-8a8f-d55edbe9f73f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3841cf8-a6d5-4e28-9d44-82616713a7fa",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "b1f02429-e6db-4539-b1d1-0206d11114f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "35d60dd9-7f10-44fe-b022-c303053faaf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1f02429-e6db-4539-b1d1-0206d11114f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96198b66-d0ad-4545-a9fa-9e94c6e02c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1f02429-e6db-4539-b1d1-0206d11114f2",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "7aa0e403-6506-47a1-bc69-94f4adbdf172",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "a245cf63-0fad-4f85-9f5d-6242adfbbdef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aa0e403-6506-47a1-bc69-94f4adbdf172",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0b0982b-d1a3-45e0-8748-cd5c360a0d5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aa0e403-6506-47a1-bc69-94f4adbdf172",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "df64cb2c-25af-46c1-972b-fddc7094dccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "20abcf11-24e6-4814-ba06-e000d7135d56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df64cb2c-25af-46c1-972b-fddc7094dccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2be6079f-c59a-4f7a-9d86-a5142cbbc1c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df64cb2c-25af-46c1-972b-fddc7094dccd",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "dbbd3817-007c-404e-bdb7-c056ff917767",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "8e1ec969-f4c7-426a-8858-fdbc49215f1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbbd3817-007c-404e-bdb7-c056ff917767",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97925919-fafa-42db-8308-3a33245c6a2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbbd3817-007c-404e-bdb7-c056ff917767",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "f7873f50-a572-45dc-b6ad-724bf69bfada",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "69a71da3-cd73-47c4-868d-6d15a629f9d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7873f50-a572-45dc-b6ad-724bf69bfada",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f237e82-e6fe-40ee-b157-4554da140c0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7873f50-a572-45dc-b6ad-724bf69bfada",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "25551b50-f8d1-42f6-9a1e-17b2fdd64272",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "9bda4ad5-cb26-42ee-bd97-0c0801d7652e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25551b50-f8d1-42f6-9a1e-17b2fdd64272",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6405b74-27a2-4dee-a87a-3f67fbe108de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25551b50-f8d1-42f6-9a1e-17b2fdd64272",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "26519572-d76e-4a67-9e5f-ea88e493e55e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "239f9983-8d01-47e4-abc8-6cae9a7676ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26519572-d76e-4a67-9e5f-ea88e493e55e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3000dd4b-1491-4c3b-a372-c564194289b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26519572-d76e-4a67-9e5f-ea88e493e55e",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "e9ec58d4-1c42-459f-a983-2cd15bca4d5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "a8c7d8f4-af63-4101-bbd8-4f0a1cf4daf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9ec58d4-1c42-459f-a983-2cd15bca4d5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85eb980a-faab-416b-8858-66a70c24301a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ec58d4-1c42-459f-a983-2cd15bca4d5b",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "f1c0b733-da0a-491b-ac9f-427447cc3a4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "b1f67561-4d1a-4050-9e12-b267e31405bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1c0b733-da0a-491b-ac9f-427447cc3a4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec22cc81-c236-4979-a170-402328bd7a9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1c0b733-da0a-491b-ac9f-427447cc3a4a",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "d9b04703-d579-4877-aae9-6c98b3343323",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "9ba4e081-f9d4-450a-9ef1-551ad92cb017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9b04703-d579-4877-aae9-6c98b3343323",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36fe9584-01a0-4fc7-aeeb-40008c6fa456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9b04703-d579-4877-aae9-6c98b3343323",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "041e76d2-f9a3-4934-8d05-84d3eb8b987a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "da2daf92-dc69-4655-8c98-3b6334a0f523",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "041e76d2-f9a3-4934-8d05-84d3eb8b987a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9dd9969-91da-4f10-a006-5877f84f882e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "041e76d2-f9a3-4934-8d05-84d3eb8b987a",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "8fc60fdb-176b-4ed3-8ec3-445bfa1a2f81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "794be478-59ca-4780-bd92-164313c7fe86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fc60fdb-176b-4ed3-8ec3-445bfa1a2f81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e656d144-617f-46d0-a6a4-7387f69af478",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fc60fdb-176b-4ed3-8ec3-445bfa1a2f81",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "c94154d4-69ea-4fd9-bf6a-1bde6127722e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "1c60aab0-758e-4e1a-8780-e8037f856b9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c94154d4-69ea-4fd9-bf6a-1bde6127722e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30e9f98e-aa04-4eb4-86e2-d1dca12dd2ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c94154d4-69ea-4fd9-bf6a-1bde6127722e",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "6a28823d-a4cd-4b6d-aed8-e9664f356cb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "647bc96e-0eb8-4fe6-bdfa-9233048900ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a28823d-a4cd-4b6d-aed8-e9664f356cb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8285a539-bb02-4364-9f24-f8cdb103660c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a28823d-a4cd-4b6d-aed8-e9664f356cb9",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "203e0817-da63-4636-93d1-4a56a32b5884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "240ed7d3-c0f2-4ecb-8b8a-3ec516bd5653",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "203e0817-da63-4636-93d1-4a56a32b5884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b003c554-1b40-4753-beb0-518084156c77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "203e0817-da63-4636-93d1-4a56a32b5884",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "e4e135a8-e1c9-4f8d-b431-4f98ddf54cbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "6b21900d-c358-4e85-80a5-d5b8b692cbaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4e135a8-e1c9-4f8d-b431-4f98ddf54cbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "893b56b9-10ff-447a-abad-1e1e0550cc21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4e135a8-e1c9-4f8d-b431-4f98ddf54cbb",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "846bb5d7-df05-4d17-8f42-b86b746ddf18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "380a5525-4b23-4e8b-9373-e975cff8672d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "846bb5d7-df05-4d17-8f42-b86b746ddf18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35c2f7ed-499c-466a-8f5f-b6b500025450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "846bb5d7-df05-4d17-8f42-b86b746ddf18",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "43a7cfaf-1d3d-428a-ad4f-a96320613e22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "95966714-195d-45f0-9bb9-4b4b5580e46b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a7cfaf-1d3d-428a-ad4f-a96320613e22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e479d21-2a20-40b1-a632-f7a25db32246",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a7cfaf-1d3d-428a-ad4f-a96320613e22",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "ed53b5fe-5ecf-4ad1-b9dc-65d008942d68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "90c7bd72-36e3-4aab-a383-0fc05b3f41ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed53b5fe-5ecf-4ad1-b9dc-65d008942d68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9148f89d-4a21-41fb-8d37-0f3db3667c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed53b5fe-5ecf-4ad1-b9dc-65d008942d68",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "2719178d-cdc2-4bd2-9d8a-3dec21fb248c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "8ae45e36-5fe6-4e7c-adb2-3bb4e89a5f79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2719178d-cdc2-4bd2-9d8a-3dec21fb248c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97b81d1c-a30e-4473-b901-96bbb8d9a6cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2719178d-cdc2-4bd2-9d8a-3dec21fb248c",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "65214798-c2ce-4894-acc7-ae80322a0978",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "4d42559c-8832-4dc3-8a98-a42f286c1f66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65214798-c2ce-4894-acc7-ae80322a0978",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "723126a3-51e7-4040-872a-d0415d4d5836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65214798-c2ce-4894-acc7-ae80322a0978",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "54eb3df3-9bc2-45c6-8cf5-7cbc120b3b22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "cf021e53-5271-4576-9485-cfa80a317a39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54eb3df3-9bc2-45c6-8cf5-7cbc120b3b22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a807b4d-08e4-4c5b-a2cf-d89ec9b0ffd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54eb3df3-9bc2-45c6-8cf5-7cbc120b3b22",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "7ab337cc-e8d8-4292-9cbd-bd7207ddc9b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "ff02e1a0-c7aa-45ba-81cb-7d92096d1b36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ab337cc-e8d8-4292-9cbd-bd7207ddc9b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a0d76e3-0fb4-4c53-aa9a-fe6b8ad1c587",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ab337cc-e8d8-4292-9cbd-bd7207ddc9b3",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "7923241e-6938-4f1f-a54b-fe6af1d7ecb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "7b99aeff-40f4-4b40-b573-28ce3180693d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7923241e-6938-4f1f-a54b-fe6af1d7ecb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33023d96-3d7f-4212-9aa7-7f2a3dead222",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7923241e-6938-4f1f-a54b-fe6af1d7ecb0",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "8b00eccd-75b7-4f5c-8509-db022a59cc08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "bdcc1ed1-bd93-4f23-a4b9-f82ff63b17df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b00eccd-75b7-4f5c-8509-db022a59cc08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45b5afd9-9812-4b0b-91fd-2e812b967dc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b00eccd-75b7-4f5c-8509-db022a59cc08",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "19a5976d-ed2a-42af-8b80-a3154e8fd124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "f0c4d0eb-ead7-43e5-9911-1278b0784ccf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19a5976d-ed2a-42af-8b80-a3154e8fd124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4919877a-435c-42eb-83f0-672d64f83638",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19a5976d-ed2a-42af-8b80-a3154e8fd124",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "9ede410a-e859-4bcc-9a14-b414b63d874a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "2f293de1-3bef-4dd6-88ad-d3d0ccd19e08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ede410a-e859-4bcc-9a14-b414b63d874a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa115b96-f0c2-4ffc-b9cf-2b6e73dd68e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ede410a-e859-4bcc-9a14-b414b63d874a",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "432b71a2-3b69-4b96-a1d6-998d23b79ae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "6a7b0447-45ce-454b-ac1a-889ccb25421e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "432b71a2-3b69-4b96-a1d6-998d23b79ae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f2874e7-bfba-41f7-a61e-63acaad8d52d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "432b71a2-3b69-4b96-a1d6-998d23b79ae4",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "aab0af6d-dd17-4a72-90c0-88a72923d2bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "e9d21526-d2ef-487e-8692-ded8fb44c4d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab0af6d-dd17-4a72-90c0-88a72923d2bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c63605dc-beb4-4b4d-8cbd-47fa0f9f2796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab0af6d-dd17-4a72-90c0-88a72923d2bb",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "cdad678c-c6c1-4e31-925b-ba5b57f47a0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "8fd1b2a1-55c6-4fe7-a6e7-312b59f9b749",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdad678c-c6c1-4e31-925b-ba5b57f47a0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bff7d286-4d69-4339-8597-81eb2b35efda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdad678c-c6c1-4e31-925b-ba5b57f47a0d",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "eb23939b-2178-4a4f-85c6-e7b5a104b774",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "9df60e3d-cbfd-4292-ac0c-c2920b670a0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb23939b-2178-4a4f-85c6-e7b5a104b774",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "387b7fd5-3bdb-4f85-b81f-77ddd19471f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb23939b-2178-4a4f-85c6-e7b5a104b774",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "36b9b77e-7197-4f91-9dd2-1c49391f7f90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "a314e238-dd17-4270-bb47-2c2b850146ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36b9b77e-7197-4f91-9dd2-1c49391f7f90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aab9c0c-5aa5-4813-813f-4798ce92a559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36b9b77e-7197-4f91-9dd2-1c49391f7f90",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "a53abda8-c339-4aae-817f-619e263a4148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "0a5c0a92-3e6e-4a94-aa0a-ed32a0c1d3c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a53abda8-c339-4aae-817f-619e263a4148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db362553-4241-4694-ae8d-f12edab42122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a53abda8-c339-4aae-817f-619e263a4148",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "ff3f02f8-0e7a-4a51-a9c7-6104ecf354b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "c42c0521-26a4-4bcb-b90d-d85508b9c03b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff3f02f8-0e7a-4a51-a9c7-6104ecf354b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07a9cd6a-9fe6-43c5-8464-3e384186ee8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff3f02f8-0e7a-4a51-a9c7-6104ecf354b5",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "b61c8696-4bf5-4bda-8f48-816698358388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "f62758f8-902a-4cfd-a8b7-740a18d6daba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b61c8696-4bf5-4bda-8f48-816698358388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce0f2344-316f-488a-8edd-dc4a26255078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b61c8696-4bf5-4bda-8f48-816698358388",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "8c1c53ef-cd4b-410b-99ba-1f6e7a7906ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "125ab5dc-ea04-4c14-8245-40d9e7eca1e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c1c53ef-cd4b-410b-99ba-1f6e7a7906ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f422805a-1d28-49ee-963b-b96edd690de0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c1c53ef-cd4b-410b-99ba-1f6e7a7906ec",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "76074968-e77c-473e-b3ed-9934306d7fb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "0a4abcce-556b-47d7-b5ac-56a52ecbb31e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76074968-e77c-473e-b3ed-9934306d7fb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1274cdf2-c629-42f6-ab2f-d5ee47ca00ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76074968-e77c-473e-b3ed-9934306d7fb3",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "26801d0f-59a0-4089-a698-4160426b0854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "e8b3bf70-8a5c-4bd4-b424-d008e0153227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26801d0f-59a0-4089-a698-4160426b0854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d872c8db-57c2-45b1-b280-ac9c978dd473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26801d0f-59a0-4089-a698-4160426b0854",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "6b6d4c1b-fc97-4c38-a471-ee8154bae54b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "5c21a50e-54c0-4001-b049-34b6deca7296",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b6d4c1b-fc97-4c38-a471-ee8154bae54b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc4994a0-1d37-46f8-91b8-8a7a8ff7059d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b6d4c1b-fc97-4c38-a471-ee8154bae54b",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "79a5fbec-02d2-484f-99a2-b110aa35d517",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "2cee0a24-32c1-4681-a831-4ba10566bb35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79a5fbec-02d2-484f-99a2-b110aa35d517",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38ceacb0-b864-431b-9057-444f1f4ca445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79a5fbec-02d2-484f-99a2-b110aa35d517",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "b6b0a958-f898-461d-8db2-66f9f23a51a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "df4d9fa2-8496-47f9-983e-7e24e748f317",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6b0a958-f898-461d-8db2-66f9f23a51a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e27f2206-86e5-4938-bd55-852d5b69d705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6b0a958-f898-461d-8db2-66f9f23a51a8",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "916406f8-500c-4210-8faa-5580462a3c18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "045d94fe-a9d0-4749-8c5a-610b6d46784b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "916406f8-500c-4210-8faa-5580462a3c18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d84597d-0ce7-400e-9edd-a85c485d41b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "916406f8-500c-4210-8faa-5580462a3c18",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        },
        {
            "id": "bed8acdc-07c8-4a00-a4d2-bac9cd09d09a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "compositeImage": {
                "id": "f1a9d380-49ee-491b-957d-c4a2b0d4d6bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bed8acdc-07c8-4a00-a4d2-bac9cd09d09a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dc9b272-3b41-4709-af93-09a823a3c4eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bed8acdc-07c8-4a00-a4d2-bac9cd09d09a",
                    "LayerId": "3e012b5f-6c36-447f-93e2-4c5b51408463"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "3e012b5f-6c36-447f-93e2-4c5b51408463",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d7d45da-c864-48c0-82b9-7afb10f6d186",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}