{
    "id": "c84e4dbe-5a40-41ce-8bba-4ef5ab7ad9f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Platform2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "frames": [
        {
            "id": "eb6a2c8d-ad35-43b4-ac76-1aaf6bf5d98e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c84e4dbe-5a40-41ce-8bba-4ef5ab7ad9f8",
            "compositeImage": {
                "id": "13b1d99e-3ada-4373-a45c-4783245a2087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb6a2c8d-ad35-43b4-ac76-1aaf6bf5d98e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a136bbf-37b2-4907-83df-2cdd18d59041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb6a2c8d-ad35-43b4-ac76-1aaf6bf5d98e",
                    "LayerId": "481ebb3f-07eb-45bf-abaa-c7e6599128d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 291,
    "layers": [
        {
            "id": "481ebb3f-07eb-45bf-abaa-c7e6599128d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c84e4dbe-5a40-41ce-8bba-4ef5ab7ad9f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 417,
    "xorig": 208,
    "yorig": 145
}