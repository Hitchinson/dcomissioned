{
    "id": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerDeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 71,
    "bbox_right": 265,
    "bbox_top": 75,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9246ac53-4343-4b90-8e07-ffbd0f70bf88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "62fee54c-23aa-45d9-9d1e-7cad7d67fa84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9246ac53-4343-4b90-8e07-ffbd0f70bf88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "753dbec2-6edc-4f80-9a05-2b1ee302a826",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9246ac53-4343-4b90-8e07-ffbd0f70bf88",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "50f8b0c3-8fae-40a9-8855-c49f8ecc4d1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "c4fe3130-073b-4fea-a445-7e1f6d24337a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50f8b0c3-8fae-40a9-8855-c49f8ecc4d1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3213482a-d8d0-40ce-8bda-c08dfe3d40e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50f8b0c3-8fae-40a9-8855-c49f8ecc4d1c",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "2fe06196-873c-4481-b6ed-3261cacc4a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "2ab38323-2896-4c29-946d-2188314f7304",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fe06196-873c-4481-b6ed-3261cacc4a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2582e43c-c204-4926-ab59-4e3df2779799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fe06196-873c-4481-b6ed-3261cacc4a12",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "ba03fd29-5e96-4937-b639-faa4bcbff4b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "f83d86bf-6818-4536-8ca6-82e9c2d23f5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba03fd29-5e96-4937-b639-faa4bcbff4b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90e8d6e2-8706-4909-9dae-7102b7f68c16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba03fd29-5e96-4937-b639-faa4bcbff4b6",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "ec17cba8-aa64-4120-b785-68a756d90f9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "29f51efa-97a9-4698-be5c-30e376d6c827",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec17cba8-aa64-4120-b785-68a756d90f9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6da794dd-96ea-409e-b4fc-c4d5b719f93e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec17cba8-aa64-4120-b785-68a756d90f9f",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "676fa5ab-ef4e-4b42-ac8e-265cdff77190",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "73112094-02c7-4a57-8df3-4e26b6f1dbc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "676fa5ab-ef4e-4b42-ac8e-265cdff77190",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "705c6f1d-ab20-46b7-80c6-5316966417ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "676fa5ab-ef4e-4b42-ac8e-265cdff77190",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "017bc16f-dee2-4b5e-b5ba-2dbfb85226ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "a74725a7-5677-480b-99e6-06ec9a22d428",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "017bc16f-dee2-4b5e-b5ba-2dbfb85226ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "febd6811-a3fb-46b1-9864-d3798a4ac48d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "017bc16f-dee2-4b5e-b5ba-2dbfb85226ab",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "1d6539f2-60c6-48e3-b600-8734baad24f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "7ad592be-5efd-42c0-b5ae-75f14abbd614",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d6539f2-60c6-48e3-b600-8734baad24f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "991ccc03-633b-4bd2-8b94-156188aaca72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d6539f2-60c6-48e3-b600-8734baad24f1",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "0e635f53-3649-460f-99da-cf549dc77cb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "5c65fdaf-55fe-4295-bfa7-9596dcbc4c5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e635f53-3649-460f-99da-cf549dc77cb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd4e8dab-7884-46c1-b5ce-48f8693e61d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e635f53-3649-460f-99da-cf549dc77cb7",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "a0c357d8-3d01-4ab2-9ec6-942bad34e3d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "3930ec77-a3d9-45bb-87ff-39304e11dbab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0c357d8-3d01-4ab2-9ec6-942bad34e3d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45f58718-c7cb-4172-9400-d2cd0b622bd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0c357d8-3d01-4ab2-9ec6-942bad34e3d0",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "a20175ec-a118-47fa-b437-4566b0b0b6bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "104a2170-75e7-4813-bc15-ad18a78fe002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a20175ec-a118-47fa-b437-4566b0b0b6bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bda5dee3-88b1-4693-903e-fba9421f5a79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a20175ec-a118-47fa-b437-4566b0b0b6bd",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "8c66de98-23e5-4924-a45b-8f948fa70675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "e8603711-a32f-47f8-b91e-7a8c4cf26e89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c66de98-23e5-4924-a45b-8f948fa70675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6218b19-e18f-4b13-848d-3876461a85d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c66de98-23e5-4924-a45b-8f948fa70675",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "f1b7f821-0aa3-467b-9689-803699fa69c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "254e41c0-41b1-415f-ad27-295a7fbb0d88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1b7f821-0aa3-467b-9689-803699fa69c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40f426c2-2623-4e9b-a020-7fbea167e68c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1b7f821-0aa3-467b-9689-803699fa69c9",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "d99ee5fd-4ee0-4655-9d18-17850bd15799",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "edd6089b-adc5-42ff-aa58-c403e28bd8f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d99ee5fd-4ee0-4655-9d18-17850bd15799",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e0d6423-6b79-411c-adaa-f7bf1bfbc7e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d99ee5fd-4ee0-4655-9d18-17850bd15799",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "af4d3139-f218-4c41-97d8-d0674612ae2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "5ceccaef-d151-4764-a239-d76940997610",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af4d3139-f218-4c41-97d8-d0674612ae2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "900326c9-35a2-4baa-8568-0768545b3074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af4d3139-f218-4c41-97d8-d0674612ae2d",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "d360b0db-e0b1-46db-8937-acfbf167ebc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "92b84084-16e1-4fb1-8ffa-665e3328d3d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d360b0db-e0b1-46db-8937-acfbf167ebc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f529969-7f4e-4c7b-9332-1dc169b7b224",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d360b0db-e0b1-46db-8937-acfbf167ebc8",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "3d8df5c1-6796-4363-8f18-10d6bfc5b7a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "2e245926-9bd5-4004-a5a7-76539a727346",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d8df5c1-6796-4363-8f18-10d6bfc5b7a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "656c81d2-c081-4a89-a5e8-f1a9dd992d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d8df5c1-6796-4363-8f18-10d6bfc5b7a3",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "89484ec7-9927-4d79-823e-49eb8c853b96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "badbfe0f-3f71-4e53-959b-bfb0ee403f3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89484ec7-9927-4d79-823e-49eb8c853b96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24c41a42-10e8-4aa8-8144-e3f03063386e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89484ec7-9927-4d79-823e-49eb8c853b96",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "66fbe927-26a3-4c22-82b9-cad0935a6b2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "532a8d33-5b74-41c9-930a-4616a5e10287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66fbe927-26a3-4c22-82b9-cad0935a6b2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e560e4e-a37b-41c2-917d-eacb29263d42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66fbe927-26a3-4c22-82b9-cad0935a6b2d",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "9ec939e2-b1f5-4927-9036-799d426accd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "f7b4c3fe-1d48-439b-bbc4-d5714ff2f0c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec939e2-b1f5-4927-9036-799d426accd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "205f6ba4-2f42-479a-a310-9ab29da7844c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec939e2-b1f5-4927-9036-799d426accd3",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "8b179837-93a0-43aa-a023-2ca3b4fc29c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "cedd6470-7bf7-41bb-b831-edf56d977ebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b179837-93a0-43aa-a023-2ca3b4fc29c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93f67526-da8a-4fc5-a8b0-ca39d0943cbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b179837-93a0-43aa-a023-2ca3b4fc29c0",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "763412eb-e39a-49d6-be86-9de280502c6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "ad94f99d-ec69-434e-ab3d-4cd7585780e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "763412eb-e39a-49d6-be86-9de280502c6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baad3ca9-60ea-44d5-a092-3b229265e23c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "763412eb-e39a-49d6-be86-9de280502c6f",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "375e8e88-71bf-4891-9b95-8373857db552",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "7b7aced3-f3ea-4365-a883-5bd037779c37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "375e8e88-71bf-4891-9b95-8373857db552",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae6db796-c656-46f4-a02f-916c4a749025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "375e8e88-71bf-4891-9b95-8373857db552",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "eabe8b9c-c3c5-4255-8e14-21c44658daa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "2da5b77e-d14a-4923-a05e-b63a67e1d041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eabe8b9c-c3c5-4255-8e14-21c44658daa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f64b252-9f25-4ed3-8e75-37f3a8151d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eabe8b9c-c3c5-4255-8e14-21c44658daa0",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "47307540-c656-4aa9-a7be-d4fbb2724040",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "2d836db1-da62-47a2-b07e-f4bda876086e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47307540-c656-4aa9-a7be-d4fbb2724040",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b326e077-e8d7-4f3f-9b34-30c0a3db0ae5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47307540-c656-4aa9-a7be-d4fbb2724040",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "0d8a3856-a14a-49ab-85d7-848f59adcfb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "9f3a53d8-9533-4a3c-8f6e-05a75a0e3222",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d8a3856-a14a-49ab-85d7-848f59adcfb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "352b7df4-bd12-427b-aa28-bc4aec53dcb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d8a3856-a14a-49ab-85d7-848f59adcfb6",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "737c72b7-394d-401a-8672-65dd324d3d68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "1e9b1c05-9bb5-4afa-ad80-78caeb5b4d10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "737c72b7-394d-401a-8672-65dd324d3d68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83915a1f-b2ab-4f31-a609-0f96e4d4b123",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "737c72b7-394d-401a-8672-65dd324d3d68",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "47e06930-33ce-4b46-a89d-c26d44bc8d37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "87530185-fb54-485d-9007-044667ec38a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47e06930-33ce-4b46-a89d-c26d44bc8d37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d50105c-03ac-441d-94d3-e3901331b8f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47e06930-33ce-4b46-a89d-c26d44bc8d37",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "245edd45-d04c-48f6-a386-95768352ecfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "eb7e9ff9-4d00-465e-995a-655ec5f0d819",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "245edd45-d04c-48f6-a386-95768352ecfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38c5ae28-bb7a-4c22-b1ff-9b70dd28bd13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "245edd45-d04c-48f6-a386-95768352ecfc",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "c30d0074-f7c1-4e05-8851-ce5e11b12d9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "80c316d9-1aa4-485d-b0e3-cd2cecc7f8e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c30d0074-f7c1-4e05-8851-ce5e11b12d9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b989bef-fcc8-4ef4-8195-6684a8064af1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c30d0074-f7c1-4e05-8851-ce5e11b12d9d",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "6a77e7b6-6b73-4d32-a9cc-02885ab85857",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "0e048043-4258-4fb2-8a18-f3bffc880999",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a77e7b6-6b73-4d32-a9cc-02885ab85857",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0255d05e-1fcf-4c79-a7d5-0d17ead5ec6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a77e7b6-6b73-4d32-a9cc-02885ab85857",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "0754207a-3457-4680-aee7-f8b20d4a67ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "8dc53893-c6a2-4569-9ffe-96d6956f4e55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0754207a-3457-4680-aee7-f8b20d4a67ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69e2e1ce-4c5a-4df8-962c-ceb83bd41e56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0754207a-3457-4680-aee7-f8b20d4a67ef",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "0f217294-8b7b-414b-8e51-6d751fb532ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "311ab74a-9cec-4fdd-bb18-47c2e729d87f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f217294-8b7b-414b-8e51-6d751fb532ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20e8ee8-a4a7-4978-8a60-59f80c057257",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f217294-8b7b-414b-8e51-6d751fb532ba",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "b5e86ee7-d224-47aa-ad4a-e6cdecdd2b55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "76812cdf-8bf6-41c7-935a-9a3c3a72253f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e86ee7-d224-47aa-ad4a-e6cdecdd2b55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b330b9e-9a9b-4cd0-a322-af87db3e7f03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e86ee7-d224-47aa-ad4a-e6cdecdd2b55",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "f1321e75-bd3c-480f-aa56-4077eb87a59d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "82648b40-b392-49f5-b3f3-988c039cde11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1321e75-bd3c-480f-aa56-4077eb87a59d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0165644-b43a-4d54-9f27-15b2adc96c6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1321e75-bd3c-480f-aa56-4077eb87a59d",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "26a1b01f-97ce-48a5-bd2f-db0f769b9fce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "b7848a63-b371-4db8-ad73-0d35dd69b154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26a1b01f-97ce-48a5-bd2f-db0f769b9fce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61ddb803-9e63-4b72-a6d5-28777ff39fb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26a1b01f-97ce-48a5-bd2f-db0f769b9fce",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "ae82476e-d8c5-4641-ad5f-75ec201a90ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "5c2767b6-9e7b-459b-8bf1-39604715c009",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae82476e-d8c5-4641-ad5f-75ec201a90ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fd2cbb5-d641-4a01-b100-4cb63d868fe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae82476e-d8c5-4641-ad5f-75ec201a90ae",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "526a49f5-196d-46b8-b86a-c571b69e70ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "fd59e16e-dbbf-4a09-8c08-d2480e53f741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "526a49f5-196d-46b8-b86a-c571b69e70ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b63d759-c543-4d98-84c7-e69c926fcfab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "526a49f5-196d-46b8-b86a-c571b69e70ac",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "cf102955-500d-4b74-b819-1ce2896f3b9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "31efb599-595a-4e5a-80fe-5bdeecbe8bb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf102955-500d-4b74-b819-1ce2896f3b9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aef8fb31-1e88-45d2-8849-4f97d7d750f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf102955-500d-4b74-b819-1ce2896f3b9c",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "65931592-bb8c-4061-b725-2382ab27358a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "fb9a139e-620b-452c-afed-0cd1ef69b071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65931592-bb8c-4061-b725-2382ab27358a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "093d206c-34d6-48f7-b4db-61d324357170",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65931592-bb8c-4061-b725-2382ab27358a",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "2f0cd48d-f1e8-41e3-88ef-93ec3a70d5df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "53be38b0-ba95-4a94-a706-599760a179cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f0cd48d-f1e8-41e3-88ef-93ec3a70d5df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "336ced73-6ae7-4c11-8801-3c14e7cbf1d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f0cd48d-f1e8-41e3-88ef-93ec3a70d5df",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        },
        {
            "id": "fc058d11-e74b-4f34-b291-7cdd39e88e0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "compositeImage": {
                "id": "92e01f0e-30a5-4394-8de0-1641b52c66aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc058d11-e74b-4f34-b291-7cdd39e88e0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e895df83-831f-47b6-bdd9-1c118bbee78d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc058d11-e74b-4f34-b291-7cdd39e88e0f",
                    "LayerId": "bc367b34-d256-45d4-a57d-21af2830fbc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "bc367b34-d256-45d4-a57d-21af2830fbc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9bbd02d-2637-4e0c-b86e-0028ce976e1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 164,
    "yorig": 144
}