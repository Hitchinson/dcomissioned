{
    "id": "f8da3e90-3a21-4f30-952b-80a768580f8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GunStart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 3,
    "bbox_right": 76,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1cded632-8004-4f6f-8086-31cf7b84d06b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8da3e90-3a21-4f30-952b-80a768580f8e",
            "compositeImage": {
                "id": "6ad01373-713e-4cdd-8748-4dad091d8d95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cded632-8004-4f6f-8086-31cf7b84d06b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fd1e877-5b44-4e04-a9a3-04890f1fb8ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cded632-8004-4f6f-8086-31cf7b84d06b",
                    "LayerId": "b05cc60b-d110-42cd-88e5-b2b3551ea120"
                }
            ]
        },
        {
            "id": "2f104e93-d58f-44f8-a5b9-02874c3413dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8da3e90-3a21-4f30-952b-80a768580f8e",
            "compositeImage": {
                "id": "5b2ca60d-56b5-496d-906b-4001336d539a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f104e93-d58f-44f8-a5b9-02874c3413dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1545500e-f913-4d7f-a9c0-8affe9422f3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f104e93-d58f-44f8-a5b9-02874c3413dc",
                    "LayerId": "b05cc60b-d110-42cd-88e5-b2b3551ea120"
                }
            ]
        },
        {
            "id": "63e2118f-b8f6-43ab-af9d-7e0e05ae6335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8da3e90-3a21-4f30-952b-80a768580f8e",
            "compositeImage": {
                "id": "0925200b-0a51-4f74-9ec3-7a2364e3d2e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63e2118f-b8f6-43ab-af9d-7e0e05ae6335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "611adc36-0f3f-42e3-9a7e-6a02e2b94208",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63e2118f-b8f6-43ab-af9d-7e0e05ae6335",
                    "LayerId": "b05cc60b-d110-42cd-88e5-b2b3551ea120"
                }
            ]
        },
        {
            "id": "cac6b1de-4b4d-4d67-b4db-97c54591f678",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8da3e90-3a21-4f30-952b-80a768580f8e",
            "compositeImage": {
                "id": "be1c6df9-85c9-4cc2-a1ee-b8a836a994ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cac6b1de-4b4d-4d67-b4db-97c54591f678",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a27d672-2b2f-48c8-9842-ec960aef3b88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cac6b1de-4b4d-4d67-b4db-97c54591f678",
                    "LayerId": "b05cc60b-d110-42cd-88e5-b2b3551ea120"
                }
            ]
        },
        {
            "id": "f94cc6ee-69f0-4568-8319-f7d7885394eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8da3e90-3a21-4f30-952b-80a768580f8e",
            "compositeImage": {
                "id": "630f19b9-e4d9-4fa6-88e8-d73446481c1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f94cc6ee-69f0-4568-8319-f7d7885394eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d20437b-1351-4ab0-bac4-7541bc88423c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f94cc6ee-69f0-4568-8319-f7d7885394eb",
                    "LayerId": "b05cc60b-d110-42cd-88e5-b2b3551ea120"
                }
            ]
        },
        {
            "id": "18d94ac2-75ec-460d-9e69-a4b8f36dd81d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8da3e90-3a21-4f30-952b-80a768580f8e",
            "compositeImage": {
                "id": "6faa5346-61f1-40c9-b03d-b568f91d6a5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18d94ac2-75ec-460d-9e69-a4b8f36dd81d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d637ca0e-941a-409d-b954-181e62bb0241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18d94ac2-75ec-460d-9e69-a4b8f36dd81d",
                    "LayerId": "b05cc60b-d110-42cd-88e5-b2b3551ea120"
                }
            ]
        },
        {
            "id": "95606885-b94e-4a82-92ed-3b55b31e8312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8da3e90-3a21-4f30-952b-80a768580f8e",
            "compositeImage": {
                "id": "c28ebcf7-6523-4c57-a57d-50d5dfa8e8e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95606885-b94e-4a82-92ed-3b55b31e8312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e89c2e5a-c3e0-47a6-9d1e-c174a05d7353",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95606885-b94e-4a82-92ed-3b55b31e8312",
                    "LayerId": "b05cc60b-d110-42cd-88e5-b2b3551ea120"
                }
            ]
        },
        {
            "id": "5a184cbd-7797-480c-a766-81c9e5f89476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8da3e90-3a21-4f30-952b-80a768580f8e",
            "compositeImage": {
                "id": "dcb59d65-7278-4bb3-a2b5-94d4e647a3b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a184cbd-7797-480c-a766-81c9e5f89476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ff4e671-b930-492d-8861-57677ff51aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a184cbd-7797-480c-a766-81c9e5f89476",
                    "LayerId": "b05cc60b-d110-42cd-88e5-b2b3551ea120"
                }
            ]
        },
        {
            "id": "f6869323-ffe9-4004-9843-47bc578294af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8da3e90-3a21-4f30-952b-80a768580f8e",
            "compositeImage": {
                "id": "18de80cd-6b6c-4c31-9881-b2f24f0025b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6869323-ffe9-4004-9843-47bc578294af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0c5e599-cf1e-412b-9290-bd454cfe5bb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6869323-ffe9-4004-9843-47bc578294af",
                    "LayerId": "b05cc60b-d110-42cd-88e5-b2b3551ea120"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 74,
    "layers": [
        {
            "id": "b05cc60b-d110-42cd-88e5-b2b3551ea120",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8da3e90-3a21-4f30-952b-80a768580f8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 37
}