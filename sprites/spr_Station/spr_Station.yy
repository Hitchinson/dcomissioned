{
    "id": "4cadc284-9c74-443d-8048-a6b3b81bc6e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Station",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 437,
    "bbox_left": 0,
    "bbox_right": 243,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2b398752-05fc-4373-a290-b11ae53fda5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cadc284-9c74-443d-8048-a6b3b81bc6e6",
            "compositeImage": {
                "id": "43a4aaee-6f94-4477-affd-d4a584652402",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b398752-05fc-4373-a290-b11ae53fda5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e8eead2-c97e-4d7c-a3fc-2de94cb6e8b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b398752-05fc-4373-a290-b11ae53fda5d",
                    "LayerId": "7ddc6853-e919-4899-b3fb-ca0667a3d82e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 438,
    "layers": [
        {
            "id": "7ddc6853-e919-4899-b3fb-ca0667a3d82e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cadc284-9c74-443d-8048-a6b3b81bc6e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 244,
    "xorig": 82,
    "yorig": 64
}