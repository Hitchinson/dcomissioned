{
    "id": "86fdde4b-8940-4ab0-9c64-dcc0bbbceee8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerDead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 290,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0e4dea7f-8960-4027-a8e1-81370fdb755f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86fdde4b-8940-4ab0-9c64-dcc0bbbceee8",
            "compositeImage": {
                "id": "8a4d82e5-ef58-4eae-9600-ffc4f188ad58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e4dea7f-8960-4027-a8e1-81370fdb755f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13d129e4-0c33-4eb8-b6b2-2b26a0ac9fcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e4dea7f-8960-4027-a8e1-81370fdb755f",
                    "LayerId": "c0d9bfc2-9ca0-456b-b527-67c8c7461016"
                }
            ]
        },
        {
            "id": "bca553ae-daef-4aae-8a2b-6096cff09cc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86fdde4b-8940-4ab0-9c64-dcc0bbbceee8",
            "compositeImage": {
                "id": "1981b8fe-51b6-497d-9724-b33d1b1d5cc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bca553ae-daef-4aae-8a2b-6096cff09cc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "968e1262-248c-4f3c-9ac3-55a96bd5038e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca553ae-daef-4aae-8a2b-6096cff09cc1",
                    "LayerId": "c0d9bfc2-9ca0-456b-b527-67c8c7461016"
                }
            ]
        },
        {
            "id": "50509894-5cca-42ba-8159-09f6a938f7c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86fdde4b-8940-4ab0-9c64-dcc0bbbceee8",
            "compositeImage": {
                "id": "889ea15d-19e6-44ff-9cf6-0f3805a3f68a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50509894-5cca-42ba-8159-09f6a938f7c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43c2833b-ee06-4b23-b413-f56f99c460ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50509894-5cca-42ba-8159-09f6a938f7c4",
                    "LayerId": "c0d9bfc2-9ca0-456b-b527-67c8c7461016"
                }
            ]
        },
        {
            "id": "112a7beb-b801-457f-b220-2065cbba5886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86fdde4b-8940-4ab0-9c64-dcc0bbbceee8",
            "compositeImage": {
                "id": "3b52442c-43ee-436d-b868-02c4c8398eeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "112a7beb-b801-457f-b220-2065cbba5886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbb6e798-82b1-4c92-bad8-b59b996e6cfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "112a7beb-b801-457f-b220-2065cbba5886",
                    "LayerId": "c0d9bfc2-9ca0-456b-b527-67c8c7461016"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "c0d9bfc2-9ca0-456b-b527-67c8c7461016",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86fdde4b-8940-4ab0-9c64-dcc0bbbceee8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 150
}