{
    "id": "2797cdef-4bbd-4df3-a91f-07e255a8a018",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Platform4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 274,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e02dca1d-c946-46db-885f-5da2ccf455b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2797cdef-4bbd-4df3-a91f-07e255a8a018",
            "compositeImage": {
                "id": "260e9efb-f97c-4d94-b3da-e4af428c4771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e02dca1d-c946-46db-885f-5da2ccf455b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ba33e50-1461-4dde-8ac3-167f2c47c796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e02dca1d-c946-46db-885f-5da2ccf455b5",
                    "LayerId": "43fc0e0a-6e6b-49d4-bf11-c794f4a28b59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 275,
    "layers": [
        {
            "id": "43fc0e0a-6e6b-49d4-bf11-c794f4a28b59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2797cdef-4bbd-4df3-a91f-07e255a8a018",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 137
}