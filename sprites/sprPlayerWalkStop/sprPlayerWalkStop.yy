{
    "id": "2ceb61e7-7669-4647-9568-e689f9a19beb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerWalkStop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 71,
    "bbox_right": 265,
    "bbox_top": 75,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d6196495-30d6-4cf7-96b5-8320d54a3024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ceb61e7-7669-4647-9568-e689f9a19beb",
            "compositeImage": {
                "id": "b59ceddc-c67d-4c3b-80ba-0264d942fdc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6196495-30d6-4cf7-96b5-8320d54a3024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceabd2a5-64f2-422f-a85d-644e6b11426a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6196495-30d6-4cf7-96b5-8320d54a3024",
                    "LayerId": "d54a3d4b-7059-46d8-af36-c81f9714cbe0"
                }
            ]
        },
        {
            "id": "75140115-2a54-402e-a850-656db90e84d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ceb61e7-7669-4647-9568-e689f9a19beb",
            "compositeImage": {
                "id": "50395570-1cd7-4afd-8a70-f595c562d110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75140115-2a54-402e-a850-656db90e84d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbae0f9b-b325-44df-9bc7-cac87c6e18a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75140115-2a54-402e-a850-656db90e84d6",
                    "LayerId": "d54a3d4b-7059-46d8-af36-c81f9714cbe0"
                }
            ]
        },
        {
            "id": "5d8d44b3-ed66-4559-ab96-3d57b3ed66c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ceb61e7-7669-4647-9568-e689f9a19beb",
            "compositeImage": {
                "id": "9935bc89-440c-48ac-9903-97642de3f4b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d8d44b3-ed66-4559-ab96-3d57b3ed66c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b8c7d72-7494-4758-8458-e859c9da1772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d8d44b3-ed66-4559-ab96-3d57b3ed66c3",
                    "LayerId": "d54a3d4b-7059-46d8-af36-c81f9714cbe0"
                }
            ]
        },
        {
            "id": "d0ea4f60-c96a-4ac5-a398-35219b3796e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ceb61e7-7669-4647-9568-e689f9a19beb",
            "compositeImage": {
                "id": "55fd1b07-93ac-4fa1-9dc8-0f908e84c910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0ea4f60-c96a-4ac5-a398-35219b3796e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e4330b4-d50d-4055-8f4b-afad37e49140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0ea4f60-c96a-4ac5-a398-35219b3796e7",
                    "LayerId": "d54a3d4b-7059-46d8-af36-c81f9714cbe0"
                }
            ]
        },
        {
            "id": "8c48c527-f2e4-4a1f-9295-6727387f43ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ceb61e7-7669-4647-9568-e689f9a19beb",
            "compositeImage": {
                "id": "9cdcc0ac-4b02-46db-8517-8a9ff4411ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c48c527-f2e4-4a1f-9295-6727387f43ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc36da6-7133-4409-aae7-a4f380030a96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c48c527-f2e4-4a1f-9295-6727387f43ea",
                    "LayerId": "d54a3d4b-7059-46d8-af36-c81f9714cbe0"
                }
            ]
        },
        {
            "id": "515758d9-7d69-4377-9365-cd369f429883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ceb61e7-7669-4647-9568-e689f9a19beb",
            "compositeImage": {
                "id": "f1490f98-6694-4849-8dc4-1a73f90cef65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "515758d9-7d69-4377-9365-cd369f429883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eeee7eb-ce34-4c18-b47d-465968857084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "515758d9-7d69-4377-9365-cd369f429883",
                    "LayerId": "d54a3d4b-7059-46d8-af36-c81f9714cbe0"
                }
            ]
        },
        {
            "id": "42d2e4fd-5b4f-454a-a22f-e3e0cddf12d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ceb61e7-7669-4647-9568-e689f9a19beb",
            "compositeImage": {
                "id": "a2486427-d999-47ba-b838-3ac32d493449",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42d2e4fd-5b4f-454a-a22f-e3e0cddf12d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e82339a7-8734-4a72-9e9d-071431c83d99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42d2e4fd-5b4f-454a-a22f-e3e0cddf12d1",
                    "LayerId": "d54a3d4b-7059-46d8-af36-c81f9714cbe0"
                }
            ]
        },
        {
            "id": "b7f075b9-cff3-4f8b-abf9-38dcaeaeaf8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ceb61e7-7669-4647-9568-e689f9a19beb",
            "compositeImage": {
                "id": "46721d63-4024-4d06-93ba-42df2b3eb814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7f075b9-cff3-4f8b-abf9-38dcaeaeaf8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea1d4666-c8a0-466e-b069-6f1d4cefb197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7f075b9-cff3-4f8b-abf9-38dcaeaeaf8b",
                    "LayerId": "d54a3d4b-7059-46d8-af36-c81f9714cbe0"
                }
            ]
        },
        {
            "id": "5025a793-a10d-434a-bad0-992d7d266fd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ceb61e7-7669-4647-9568-e689f9a19beb",
            "compositeImage": {
                "id": "1ee66a84-97a5-45bd-9841-1849efcff038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5025a793-a10d-434a-bad0-992d7d266fd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad04090b-369b-4288-83d7-6e1c1194bcd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5025a793-a10d-434a-bad0-992d7d266fd9",
                    "LayerId": "d54a3d4b-7059-46d8-af36-c81f9714cbe0"
                }
            ]
        },
        {
            "id": "263f2a74-7deb-4426-a352-c576f555c627",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ceb61e7-7669-4647-9568-e689f9a19beb",
            "compositeImage": {
                "id": "fe787b2a-b592-434d-b115-5fa260d99be9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "263f2a74-7deb-4426-a352-c576f555c627",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e72687fe-3329-4618-ab61-4162c441a73f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "263f2a74-7deb-4426-a352-c576f555c627",
                    "LayerId": "d54a3d4b-7059-46d8-af36-c81f9714cbe0"
                }
            ]
        },
        {
            "id": "8a8795ad-165b-4403-b489-b64020934e6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ceb61e7-7669-4647-9568-e689f9a19beb",
            "compositeImage": {
                "id": "21f6b281-7c7a-439a-b6c4-d20a68b30f56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a8795ad-165b-4403-b489-b64020934e6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0fa78c5-bbf2-46f3-ae6e-6d49d354727a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a8795ad-165b-4403-b489-b64020934e6f",
                    "LayerId": "d54a3d4b-7059-46d8-af36-c81f9714cbe0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "d54a3d4b-7059-46d8-af36-c81f9714cbe0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ceb61e7-7669-4647-9568-e689f9a19beb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 164,
    "yorig": 144
}