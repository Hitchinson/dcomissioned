{
    "id": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerFlyLand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 71,
    "bbox_right": 265,
    "bbox_top": 75,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9a9468e9-cf82-423c-951a-83764c0efca4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "0d35f917-bdaf-403f-9f27-d5b0b04254ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a9468e9-cf82-423c-951a-83764c0efca4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f176075-00d0-4c5b-9b19-71d3872cf6c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9468e9-cf82-423c-951a-83764c0efca4",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "8113a29e-506b-403e-ab2a-4cbeebbb2205",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "070050c6-8034-442b-aeb7-3e8b33a3935a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8113a29e-506b-403e-ab2a-4cbeebbb2205",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f51206ce-50c3-4dc4-93ae-cc14948502ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8113a29e-506b-403e-ab2a-4cbeebbb2205",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "10d46c51-128c-4c28-95c1-7ba19a38756a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "e72132ed-52f8-4efa-a8b0-958021b7b685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10d46c51-128c-4c28-95c1-7ba19a38756a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cdfee4a-01e0-4735-b146-41718dfcd73f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d46c51-128c-4c28-95c1-7ba19a38756a",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "c4ec4438-dd24-450f-8d72-c9ee5e0b9e87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "e2612fcd-c9b3-42c4-ba25-6d5f3fd915b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4ec4438-dd24-450f-8d72-c9ee5e0b9e87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99812f4d-d1b0-47f6-aee0-179ea792a69d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4ec4438-dd24-450f-8d72-c9ee5e0b9e87",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "771ccf73-5a73-48d3-99a1-1e759d706864",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "30eaf7ac-79b8-47ea-87c3-a66c2f24e610",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "771ccf73-5a73-48d3-99a1-1e759d706864",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b025dec-9961-40d2-a0fa-0bf2fa72d4a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "771ccf73-5a73-48d3-99a1-1e759d706864",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "fdf1308c-8f6b-4fcc-a9c0-48ee1533eee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "2dea2e87-0e52-4d58-944a-be205aeeec52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdf1308c-8f6b-4fcc-a9c0-48ee1533eee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9f0ebe4-1612-41b1-902f-d9ee07298d4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdf1308c-8f6b-4fcc-a9c0-48ee1533eee6",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "027a42d5-4e37-4d0b-8932-6f99ebe39f82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "930598c4-b325-4b09-bcc5-f03141367297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "027a42d5-4e37-4d0b-8932-6f99ebe39f82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40777a68-d163-4658-86c7-a132810ac32d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "027a42d5-4e37-4d0b-8932-6f99ebe39f82",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "1dd9fc91-cd1a-4602-b178-dc96476249da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "d5407626-2338-485a-8801-934f90a5024d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dd9fc91-cd1a-4602-b178-dc96476249da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f194942-ab62-402c-b04f-fca5a96815de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dd9fc91-cd1a-4602-b178-dc96476249da",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "9b911179-2cea-4172-baba-d4324b845c7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "c6fbb4b9-b684-4a22-b2dc-71f1ace90cb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b911179-2cea-4172-baba-d4324b845c7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e1bee33-c3b3-44fd-bb81-9344fddb47f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b911179-2cea-4172-baba-d4324b845c7e",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "e86964b9-d8e1-42c5-86e7-aad8eff7d53d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "5b80cd8a-7ac8-4802-8a27-f6ab32cea290",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e86964b9-d8e1-42c5-86e7-aad8eff7d53d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cce1d16-566c-40ff-a51f-0a3f4cafd272",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e86964b9-d8e1-42c5-86e7-aad8eff7d53d",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "2fd1c513-e525-4589-b351-8d26d2050eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "5da0462a-ebe8-4d60-a153-d8e8ed3a7bd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fd1c513-e525-4589-b351-8d26d2050eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dcea564-595d-42ab-8102-66a3d25418c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fd1c513-e525-4589-b351-8d26d2050eb6",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "3da1c207-eca9-4eb8-91e5-a80bd91f2b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "eeec9ef5-c5cd-4c1d-b48c-09e77611b273",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3da1c207-eca9-4eb8-91e5-a80bd91f2b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ebe5b28-bc83-4fbe-a8d9-9105403f6314",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3da1c207-eca9-4eb8-91e5-a80bd91f2b5c",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "ebbd3fc7-ddc6-47ad-aa88-75854aaf8b5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "d6259e35-40b6-4f04-ac01-c64e21261bcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebbd3fc7-ddc6-47ad-aa88-75854aaf8b5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aca6a9d2-56cb-49ad-85a4-9cfc8dcb9dc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebbd3fc7-ddc6-47ad-aa88-75854aaf8b5b",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "ff8fffa4-9f21-4d5c-90d4-85eb9edacc3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "4cced6d5-45bf-4b93-aa06-cdb2737ef520",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff8fffa4-9f21-4d5c-90d4-85eb9edacc3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6bcb660-8733-43df-a4b5-1006b1e16167",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff8fffa4-9f21-4d5c-90d4-85eb9edacc3e",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "ca02b2a0-4760-43cf-9ced-996bc8273810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "9f972c4e-11cf-4065-94ba-f6349bf357f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca02b2a0-4760-43cf-9ced-996bc8273810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16ac5732-41b6-4a65-aa71-637f75c146fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca02b2a0-4760-43cf-9ced-996bc8273810",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "3471fbfc-3c51-42c7-bbaa-3392870c9f45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "6b12ac68-2d8b-412b-9881-37d01a960f4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3471fbfc-3c51-42c7-bbaa-3392870c9f45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a94c2cc-c086-40ec-94fc-d89462539aad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3471fbfc-3c51-42c7-bbaa-3392870c9f45",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "2fd0f5bb-24df-4840-8d1c-fe36fad19251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "ab15e32b-3a51-4c88-8161-7e6baad36a6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fd0f5bb-24df-4840-8d1c-fe36fad19251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65620ed6-c47a-4602-ae79-4a35e3e95ad3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fd0f5bb-24df-4840-8d1c-fe36fad19251",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "0954de0e-d9fe-48e4-aacc-289feac60803",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "aeb99c6e-d192-4c9d-aa12-ba201bae49af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0954de0e-d9fe-48e4-aacc-289feac60803",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cf08100-f74d-43d9-8ed6-b7b10959ab5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0954de0e-d9fe-48e4-aacc-289feac60803",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "3fefd853-c242-4c7a-beca-30b44cd07931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "00270817-a1d1-4b30-8883-fcfb07cac3d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fefd853-c242-4c7a-beca-30b44cd07931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67c6745f-a01a-4967-acb9-a25a324a4265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fefd853-c242-4c7a-beca-30b44cd07931",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "f13c98ad-13b1-4a9a-9942-6e8892b77bcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "3d5b778a-694d-4b59-89ab-a2c6f515094a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f13c98ad-13b1-4a9a-9942-6e8892b77bcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0100b79d-298e-4ee9-9726-64816b866df8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13c98ad-13b1-4a9a-9942-6e8892b77bcc",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "ece04122-e2a4-4310-b99f-ac53109df0af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "7cf94299-584a-415b-a7d5-4e08a60a6c3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ece04122-e2a4-4310-b99f-ac53109df0af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6423e0e0-b02c-4fae-ba7b-b4dc6bb4e2d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ece04122-e2a4-4310-b99f-ac53109df0af",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        },
        {
            "id": "4adce7b2-6991-4421-ae53-8adf0f3c21d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "compositeImage": {
                "id": "2b697770-04b0-4a2e-a20b-2caaa95cd6d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4adce7b2-6991-4421-ae53-8adf0f3c21d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38bb478c-d1a5-48d3-be5b-1216bfaae2de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4adce7b2-6991-4421-ae53-8adf0f3c21d7",
                    "LayerId": "529971d5-7106-41b4-8707-f8101d144d5b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "529971d5-7106-41b4-8707-f8101d144d5b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4bb56b9-6766-4414-bd04-bb17ae29c310",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 164,
    "yorig": 144
}