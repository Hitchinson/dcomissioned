{
    "id": "549de4d2-17ca-4025-bac7-33a7861cbc67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Textbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d99b5373-c68e-41bb-b8c8-12a8b534af9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "549de4d2-17ca-4025-bac7-33a7861cbc67",
            "compositeImage": {
                "id": "c28177b2-637e-48ad-bbde-dbeb91582fe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d99b5373-c68e-41bb-b8c8-12a8b534af9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f34251ab-f368-40b3-92f4-985aa963dc1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d99b5373-c68e-41bb-b8c8-12a8b534af9e",
                    "LayerId": "9085285c-51f8-4548-8a18-6fb1e73078bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "9085285c-51f8-4548-8a18-6fb1e73078bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "549de4d2-17ca-4025-bac7-33a7861cbc67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}