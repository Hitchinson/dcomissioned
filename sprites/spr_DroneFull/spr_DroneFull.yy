{
    "id": "1d64066a-7650-41aa-89db-02cc4a021fb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_DroneFull",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 2,
    "bbox_right": 64,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5eac0965-7f4d-485c-92fc-52d8dfb9da10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d64066a-7650-41aa-89db-02cc4a021fb6",
            "compositeImage": {
                "id": "b181a222-de3a-478f-b773-5d9ceeee7f70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eac0965-7f4d-485c-92fc-52d8dfb9da10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0aa5b347-59a0-4248-9c9b-f7da75c06939",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eac0965-7f4d-485c-92fc-52d8dfb9da10",
                    "LayerId": "53a70a8d-b2d5-4cc1-b01c-b370ccb2f6cd"
                }
            ]
        },
        {
            "id": "9d060057-1698-4e43-aa06-63fae3a65848",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d64066a-7650-41aa-89db-02cc4a021fb6",
            "compositeImage": {
                "id": "9a982b74-6b60-4995-b019-e1d439dabd33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d060057-1698-4e43-aa06-63fae3a65848",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbae35ff-ca51-42df-a6c0-c0f84f75b98a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d060057-1698-4e43-aa06-63fae3a65848",
                    "LayerId": "53a70a8d-b2d5-4cc1-b01c-b370ccb2f6cd"
                }
            ]
        },
        {
            "id": "c23609ab-1a3b-4a5d-a65b-e1ca838c6408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d64066a-7650-41aa-89db-02cc4a021fb6",
            "compositeImage": {
                "id": "ce5fc02a-4cdd-4677-8f61-1dc0c550f84c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c23609ab-1a3b-4a5d-a65b-e1ca838c6408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "920f3aea-4403-4664-af83-781d03d81c32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c23609ab-1a3b-4a5d-a65b-e1ca838c6408",
                    "LayerId": "53a70a8d-b2d5-4cc1-b01c-b370ccb2f6cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "53a70a8d-b2d5-4cc1-b01c-b370ccb2f6cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d64066a-7650-41aa-89db-02cc4a021fb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 34,
    "yorig": 23
}