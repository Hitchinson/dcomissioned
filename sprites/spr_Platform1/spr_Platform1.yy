{
    "id": "8900dbae-5820-4167-80c0-01e8a4212567",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Platform1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 206,
    "bbox_left": 0,
    "bbox_right": 137,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a3858abf-ceec-42c6-905f-304be2a8bf9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8900dbae-5820-4167-80c0-01e8a4212567",
            "compositeImage": {
                "id": "bc1be160-0a1a-4abb-9e3a-27a252c2a6e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3858abf-ceec-42c6-905f-304be2a8bf9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cb8260e-c958-4be4-bffb-dd8b665ae109",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3858abf-ceec-42c6-905f-304be2a8bf9f",
                    "LayerId": "acf26802-8dd9-4af9-9000-d24406acc81b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 207,
    "layers": [
        {
            "id": "acf26802-8dd9-4af9-9000-d24406acc81b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8900dbae-5820-4167-80c0-01e8a4212567",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 138,
    "xorig": 69,
    "yorig": 103
}