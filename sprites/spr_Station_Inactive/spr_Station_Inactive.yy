{
    "id": "33171869-845a-466d-b081-63496b1c41c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Station_Inactive",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 437,
    "bbox_left": 0,
    "bbox_right": 243,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e67058f4-7cb4-4b5e-99c5-635d4f537858",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33171869-845a-466d-b081-63496b1c41c6",
            "compositeImage": {
                "id": "c7edfa27-069a-43e2-a4f4-7fa58870aaaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e67058f4-7cb4-4b5e-99c5-635d4f537858",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40a52dcc-4f1a-48fd-b012-375b29ffe3bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e67058f4-7cb4-4b5e-99c5-635d4f537858",
                    "LayerId": "a8f2774e-7057-495d-8fe6-b43739ca7d1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 438,
    "layers": [
        {
            "id": "a8f2774e-7057-495d-8fe6-b43739ca7d1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33171869-845a-466d-b081-63496b1c41c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 244,
    "xorig": 122,
    "yorig": 219
}