{
    "id": "378c1f2d-818d-453b-9302-6cf11be1da31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EnemyType1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 369,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "62d47b25-6c7a-417f-a570-488a6529ca0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "979388c4-fbf0-4c7b-a602-e6780ce637f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62d47b25-6c7a-417f-a570-488a6529ca0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5045dc7f-e502-4258-b8ed-a5aeec4a4954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62d47b25-6c7a-417f-a570-488a6529ca0d",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "39c9178b-d5a3-478e-9efc-d319df75dc4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "bcee0c87-d9ef-4581-95f1-627c65361418",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39c9178b-d5a3-478e-9efc-d319df75dc4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c277013-693b-4d44-84e4-0e1b2fffea18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39c9178b-d5a3-478e-9efc-d319df75dc4a",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "ae0c4970-2067-4654-a64d-a83fb74fa11b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "9a09c5c4-4563-48ea-9081-70e2c7680328",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae0c4970-2067-4654-a64d-a83fb74fa11b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1aa84c0-f725-4aeb-8062-5df3d2565988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae0c4970-2067-4654-a64d-a83fb74fa11b",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "3ff8245f-d65b-40c8-b8b9-a4d4edfbb85f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "bedabdae-a9b9-4278-8456-b7fbd3d5e824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ff8245f-d65b-40c8-b8b9-a4d4edfbb85f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3af64b7-b4ab-4439-971a-fa69828784db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ff8245f-d65b-40c8-b8b9-a4d4edfbb85f",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "91d04e14-67a9-4527-a34c-75d334d4facc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "a3d59321-0d10-4a4c-b5de-d9d632a58b89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91d04e14-67a9-4527-a34c-75d334d4facc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bfd9eb0-eb17-405e-a1f1-86e913e846f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91d04e14-67a9-4527-a34c-75d334d4facc",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "3d668f7b-9443-454d-8979-d6819187ca00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "172a0181-cda3-4843-a942-ec156dbdabc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d668f7b-9443-454d-8979-d6819187ca00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebae0003-42f7-4ea4-a59b-f35a23c45d5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d668f7b-9443-454d-8979-d6819187ca00",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "1c1cbdf1-39aa-4891-a2f2-7a83e4935507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "7746e7ee-61bf-4608-abc1-4b77627447e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c1cbdf1-39aa-4891-a2f2-7a83e4935507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2872d0d-cc53-4a9f-82d1-0217ef32816f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c1cbdf1-39aa-4891-a2f2-7a83e4935507",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "e806b7ee-6304-4075-93f9-9711f6f0072e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "69a6e39e-5b8b-4b49-b15d-93ec51befc4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e806b7ee-6304-4075-93f9-9711f6f0072e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a938776-6a18-46d3-bc35-2beffbd97c3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e806b7ee-6304-4075-93f9-9711f6f0072e",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "22dec0e8-9072-416e-b89b-57ddbf46e890",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "2b806359-7d6a-446c-a73e-af7979706a0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22dec0e8-9072-416e-b89b-57ddbf46e890",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "def4357e-0e01-44ef-84a3-2b6e4b335466",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22dec0e8-9072-416e-b89b-57ddbf46e890",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "6ba3260f-0401-4d4e-bbe7-a51a77de76bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "795fe9a2-16b2-4dde-a009-127f34e56312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ba3260f-0401-4d4e-bbe7-a51a77de76bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f8b4fdc-bdfa-4e3d-9a88-b5a1173a91d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ba3260f-0401-4d4e-bbe7-a51a77de76bd",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "68341c83-3d09-4346-bd01-32707bb2385b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "83ec6aa2-4e04-4256-ab3c-e6490958c86e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68341c83-3d09-4346-bd01-32707bb2385b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02fbc172-9709-4c10-a823-0c85e58592f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68341c83-3d09-4346-bd01-32707bb2385b",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "0f99f75b-49f4-41cd-9a82-34556ffd4605",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "94556e23-c92a-408c-8545-28ee6203c1de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f99f75b-49f4-41cd-9a82-34556ffd4605",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1054076f-f76a-4e4b-815e-a0a4526d1864",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f99f75b-49f4-41cd-9a82-34556ffd4605",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "992c95a9-e36e-426b-ad12-974b869b54e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "079d2f73-942b-4c01-b39d-120afd35f25f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "992c95a9-e36e-426b-ad12-974b869b54e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d3c0471-9c65-4922-a6c0-03f778737d42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "992c95a9-e36e-426b-ad12-974b869b54e5",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "06775e3c-de20-4220-a5ec-566016c4f34a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "2c792f12-abdb-41f9-93cf-956d5f0494a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06775e3c-de20-4220-a5ec-566016c4f34a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d00fdddc-cb07-4545-a4f0-15a327f9dfe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06775e3c-de20-4220-a5ec-566016c4f34a",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "ec802116-ea41-4ca6-a6de-37fd16c33260",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "85b0206d-d41b-4d3f-869c-d9aa101b971e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec802116-ea41-4ca6-a6de-37fd16c33260",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71cc258c-f6a7-44cc-8435-f5472db574cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec802116-ea41-4ca6-a6de-37fd16c33260",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "b1c3319a-dc22-4b87-8d4a-d2426541e523",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "bf561c94-3c40-4b82-b8dd-64983948141d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1c3319a-dc22-4b87-8d4a-d2426541e523",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f58afd0a-6edf-4997-bd7c-cb4c828bb97c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1c3319a-dc22-4b87-8d4a-d2426541e523",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "eddae819-9bfd-4b7d-9ec8-784ea6b82093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "51575e76-b748-44ec-b3da-35d7d406823b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eddae819-9bfd-4b7d-9ec8-784ea6b82093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "788f9e91-0b96-41fd-9631-1e9cf01297f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eddae819-9bfd-4b7d-9ec8-784ea6b82093",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "1258c54b-e93a-446a-aa9e-169a640b1ba0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "6e594ea8-d087-492c-afa8-9ce19d122873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1258c54b-e93a-446a-aa9e-169a640b1ba0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3229a73a-4d3e-46be-823a-343381b8ffb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1258c54b-e93a-446a-aa9e-169a640b1ba0",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "a1fbf145-4dd8-4d0f-97bd-7d019d0ba206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "2026dbfe-4705-4a87-8eb7-91444f382569",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1fbf145-4dd8-4d0f-97bd-7d019d0ba206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32797aa8-ef88-4a13-a2d7-465d6ec15130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1fbf145-4dd8-4d0f-97bd-7d019d0ba206",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "4fb08473-54c6-41ff-bdce-cd0edf5a4988",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "c2b78880-85f8-48e5-953c-d15801706c4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fb08473-54c6-41ff-bdce-cd0edf5a4988",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77853e5b-9116-4f4c-909c-47c6db85d5bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb08473-54c6-41ff-bdce-cd0edf5a4988",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "7b66d7a6-a8cb-4f2d-8316-88a4e1fee79b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "59ada1d9-fabd-457e-a71b-163c161ea7df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b66d7a6-a8cb-4f2d-8316-88a4e1fee79b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56d25875-9c13-4d1d-9c7f-b37f8fc0b33a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b66d7a6-a8cb-4f2d-8316-88a4e1fee79b",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "3beb15bf-c5e6-43fc-9d31-df7dfcd66ccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "db66dd4e-e020-4f2e-8835-d5e6cd5bd477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3beb15bf-c5e6-43fc-9d31-df7dfcd66ccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "419b36d3-fcf6-4a39-b77a-413be71ddccd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3beb15bf-c5e6-43fc-9d31-df7dfcd66ccb",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "02ff9aea-fdde-4fc7-8cd4-ff8a5e10a5bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "d75af70b-79ec-4101-8cb3-2d41546419be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02ff9aea-fdde-4fc7-8cd4-ff8a5e10a5bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a093d98d-1c0f-40db-9c44-1e5bcffe3bf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02ff9aea-fdde-4fc7-8cd4-ff8a5e10a5bd",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        },
        {
            "id": "d3499c36-b994-40a2-ba66-ddf744cbec03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "compositeImage": {
                "id": "52aea3f5-d219-4b3d-903a-e1fb86788c55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3499c36-b994-40a2-ba66-ddf744cbec03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a108c70-bb9f-44b6-8d2f-6f0b7094cda9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3499c36-b994-40a2-ba66-ddf744cbec03",
                    "LayerId": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "616e7dfe-5fbc-4d77-b834-07f10f6c3a9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "378c1f2d-818d-453b-9302-6cf11be1da31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 370,
    "xorig": 185,
    "yorig": 100
}