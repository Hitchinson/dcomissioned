{
    "id": "74a9f8a7-65bb-4822-8003-68344d539d1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Station2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 437,
    "bbox_left": 0,
    "bbox_right": 243,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0bef355b-08f5-4a59-9a5c-444b7784c6bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74a9f8a7-65bb-4822-8003-68344d539d1b",
            "compositeImage": {
                "id": "3844ec65-7fde-4ef1-bf3a-5c52dfdbfa0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bef355b-08f5-4a59-9a5c-444b7784c6bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4737408-b40d-46f0-b023-03a916fa13a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bef355b-08f5-4a59-9a5c-444b7784c6bf",
                    "LayerId": "61d3bda0-0852-4915-af58-2ae7bf2d353c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 438,
    "layers": [
        {
            "id": "61d3bda0-0852-4915-af58-2ae7bf2d353c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74a9f8a7-65bb-4822-8003-68344d539d1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 244,
    "xorig": 122,
    "yorig": 219
}