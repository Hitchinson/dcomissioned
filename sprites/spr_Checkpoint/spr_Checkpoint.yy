{
    "id": "5e4a682f-6a4b-4070-92e8-07d57abd5390",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Checkpoint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 442,
    "bbox_left": 0,
    "bbox_right": 84,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "62252b78-9797-4154-856c-29b3c2592fc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e4a682f-6a4b-4070-92e8-07d57abd5390",
            "compositeImage": {
                "id": "d5771aa7-26cd-4128-bf72-7fb975d93889",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62252b78-9797-4154-856c-29b3c2592fc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "106148b1-ed71-4ffc-a40e-8274d377f6a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62252b78-9797-4154-856c-29b3c2592fc6",
                    "LayerId": "6f392f0e-0356-4d36-80ec-4f67501e7e5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 443,
    "layers": [
        {
            "id": "6f392f0e-0356-4d36-80ec-4f67501e7e5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e4a682f-6a4b-4070-92e8-07d57abd5390",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 85,
    "xorig": 42,
    "yorig": 221
}