{
    "id": "39e32874-d8bd-4ecc-948e-08db1ce0f058",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Platform5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 160,
    "bbox_left": 0,
    "bbox_right": 154,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "25c01af9-8535-474c-a454-8709b5e11dec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39e32874-d8bd-4ecc-948e-08db1ce0f058",
            "compositeImage": {
                "id": "1a55a226-0181-435e-9a40-bfb46cee5da5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25c01af9-8535-474c-a454-8709b5e11dec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "106a1330-ca8c-47d9-a9d7-d310367e7721",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25c01af9-8535-474c-a454-8709b5e11dec",
                    "LayerId": "6740dbc7-23c6-4597-9459-f4c4574b1278"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "6740dbc7-23c6-4597-9459-f4c4574b1278",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39e32874-d8bd-4ecc-948e-08db1ce0f058",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 155,
    "xorig": 77,
    "yorig": 80
}