{
    "id": "a622bcdb-a596-4142-8927-5c3a8a54d9d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FuelBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "045737a3-305c-4554-806c-53cea31081dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a622bcdb-a596-4142-8927-5c3a8a54d9d0",
            "compositeImage": {
                "id": "6fc65b44-de21-420e-8c56-781c5aa7971a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "045737a3-305c-4554-806c-53cea31081dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6229df65-f427-4f30-8097-efb5dee7050a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "045737a3-305c-4554-806c-53cea31081dc",
                    "LayerId": "ae54808d-c493-4969-a9a6-e01126eff3f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "ae54808d-c493-4969-a9a6-e01126eff3f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a622bcdb-a596-4142-8927-5c3a8a54d9d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 0,
    "yorig": 0
}