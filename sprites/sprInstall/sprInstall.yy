{
    "id": "1770339c-2143-433d-b7d6-52d48d399587",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprInstall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 251,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d08ca1e1-9b51-4fda-b200-370b350ebc51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1770339c-2143-433d-b7d6-52d48d399587",
            "compositeImage": {
                "id": "2e21a2a1-026c-4241-952e-47ed764e8a71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d08ca1e1-9b51-4fda-b200-370b350ebc51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a58eb23b-5264-45f4-8f28-b5fc3af04628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d08ca1e1-9b51-4fda-b200-370b350ebc51",
                    "LayerId": "b717ead0-ba77-4801-8d0c-6b2c9da4f009"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 61,
    "layers": [
        {
            "id": "b717ead0-ba77-4801-8d0c-6b2c9da4f009",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1770339c-2143-433d-b7d6-52d48d399587",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 252,
    "xorig": 126,
    "yorig": 30
}