{
    "id": "60c0906e-6ac2-4822-a0a6-5553d1d2dfb3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_DroneLow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 2,
    "bbox_right": 64,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8d7e19d0-c36b-4f42-a17a-db3861d12fd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60c0906e-6ac2-4822-a0a6-5553d1d2dfb3",
            "compositeImage": {
                "id": "89142af2-1f6b-4ec8-b8ba-8632ce3cc084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d7e19d0-c36b-4f42-a17a-db3861d12fd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16f25a03-34ac-4464-b520-9cdeadc2a44f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d7e19d0-c36b-4f42-a17a-db3861d12fd7",
                    "LayerId": "320af8c1-8b48-4ac9-9870-cadacccb6bb8"
                }
            ]
        },
        {
            "id": "fdf98a23-4e34-497f-846d-6195066e8943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60c0906e-6ac2-4822-a0a6-5553d1d2dfb3",
            "compositeImage": {
                "id": "89692881-7c13-4a14-9152-c11a1fbee13e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdf98a23-4e34-497f-846d-6195066e8943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "407c4e9a-8c24-4e15-9026-5aad3bf863b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdf98a23-4e34-497f-846d-6195066e8943",
                    "LayerId": "320af8c1-8b48-4ac9-9870-cadacccb6bb8"
                }
            ]
        },
        {
            "id": "83607e6a-0182-4611-8306-712ace358bf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60c0906e-6ac2-4822-a0a6-5553d1d2dfb3",
            "compositeImage": {
                "id": "1e014518-52eb-4592-a87f-d5f78cad6e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83607e6a-0182-4611-8306-712ace358bf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f24f11f-1fa6-47bd-85ce-d6d92096b1ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83607e6a-0182-4611-8306-712ace358bf8",
                    "LayerId": "320af8c1-8b48-4ac9-9870-cadacccb6bb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "320af8c1-8b48-4ac9-9870-cadacccb6bb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60c0906e-6ac2-4822-a0a6-5553d1d2dfb3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 34,
    "yorig": 23
}