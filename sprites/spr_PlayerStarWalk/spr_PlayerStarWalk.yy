{
    "id": "b3432348-1c61-423d-be28-703918a0266a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerStarWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 412,
    "bbox_left": 85,
    "bbox_right": 664,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2c62784e-972d-42a8-843d-17c90178283b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3432348-1c61-423d-be28-703918a0266a",
            "compositeImage": {
                "id": "8095cc3a-8fdc-4de2-8952-c694effb55d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c62784e-972d-42a8-843d-17c90178283b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3be466a8-9be2-4acc-8295-ee11b8c37823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c62784e-972d-42a8-843d-17c90178283b",
                    "LayerId": "8d8b69ab-e7e4-44eb-bf46-b816f5257b26"
                }
            ]
        },
        {
            "id": "2bb54553-ab92-4b4d-bbb4-f4fea75eccc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3432348-1c61-423d-be28-703918a0266a",
            "compositeImage": {
                "id": "c40fb367-3d88-4093-b419-5c2bc96ebe8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bb54553-ab92-4b4d-bbb4-f4fea75eccc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84bc92e8-f128-4632-aef9-f25f941e950d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bb54553-ab92-4b4d-bbb4-f4fea75eccc8",
                    "LayerId": "8d8b69ab-e7e4-44eb-bf46-b816f5257b26"
                }
            ]
        },
        {
            "id": "2d910c7f-464f-4cae-bfa2-4a3c8aba125c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3432348-1c61-423d-be28-703918a0266a",
            "compositeImage": {
                "id": "30a7e6a6-d457-4262-9382-82afc8a07201",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d910c7f-464f-4cae-bfa2-4a3c8aba125c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f000da7b-61cd-4076-8f9e-22d2a8c54cf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d910c7f-464f-4cae-bfa2-4a3c8aba125c",
                    "LayerId": "8d8b69ab-e7e4-44eb-bf46-b816f5257b26"
                }
            ]
        },
        {
            "id": "336217f8-e543-41d5-9307-df6bba3bc7d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3432348-1c61-423d-be28-703918a0266a",
            "compositeImage": {
                "id": "16658bd4-3970-4db2-8705-6d77bb6e2d31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "336217f8-e543-41d5-9307-df6bba3bc7d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db83a396-0ddb-4b22-bdc9-02418727fd3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "336217f8-e543-41d5-9307-df6bba3bc7d8",
                    "LayerId": "8d8b69ab-e7e4-44eb-bf46-b816f5257b26"
                }
            ]
        },
        {
            "id": "868b0456-d75d-41f9-b76a-d694cc4968a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3432348-1c61-423d-be28-703918a0266a",
            "compositeImage": {
                "id": "9c672d20-3bf5-49b8-8357-10d73a84dc83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "868b0456-d75d-41f9-b76a-d694cc4968a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4041bfa0-81fb-4cfc-aa78-65f967929cb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "868b0456-d75d-41f9-b76a-d694cc4968a8",
                    "LayerId": "8d8b69ab-e7e4-44eb-bf46-b816f5257b26"
                }
            ]
        },
        {
            "id": "48e34ccc-0591-49fb-bcfa-1aa372163b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3432348-1c61-423d-be28-703918a0266a",
            "compositeImage": {
                "id": "7eeb1434-c5cb-4ef6-8ef7-5aa0df5eb443",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48e34ccc-0591-49fb-bcfa-1aa372163b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52b17952-59f3-481d-bffb-f5efa14a0a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48e34ccc-0591-49fb-bcfa-1aa372163b09",
                    "LayerId": "8d8b69ab-e7e4-44eb-bf46-b816f5257b26"
                }
            ]
        },
        {
            "id": "cd3b5ff7-899e-4e73-b48e-9236bd488bfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3432348-1c61-423d-be28-703918a0266a",
            "compositeImage": {
                "id": "f74c6aba-f1a3-4f8a-a73e-537530325a17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd3b5ff7-899e-4e73-b48e-9236bd488bfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82753ede-b963-4070-b32a-22f6b8132d1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd3b5ff7-899e-4e73-b48e-9236bd488bfe",
                    "LayerId": "8d8b69ab-e7e4-44eb-bf46-b816f5257b26"
                }
            ]
        },
        {
            "id": "f5471ee2-e7ca-4336-920d-9dc12ab49c74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3432348-1c61-423d-be28-703918a0266a",
            "compositeImage": {
                "id": "925b8340-9896-450e-8ec4-b0683569d265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5471ee2-e7ca-4336-920d-9dc12ab49c74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bf423c3-2b2b-4c59-a55f-4dd86ed24640",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5471ee2-e7ca-4336-920d-9dc12ab49c74",
                    "LayerId": "8d8b69ab-e7e4-44eb-bf46-b816f5257b26"
                }
            ]
        },
        {
            "id": "ca2013d6-d7dd-4a7c-ba2f-f920ebeeb9c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3432348-1c61-423d-be28-703918a0266a",
            "compositeImage": {
                "id": "71c60e8d-640d-4c23-a9e7-ca908094f9a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca2013d6-d7dd-4a7c-ba2f-f920ebeeb9c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8883f1c7-954f-40ee-be1c-16b204aa546d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca2013d6-d7dd-4a7c-ba2f-f920ebeeb9c1",
                    "LayerId": "8d8b69ab-e7e4-44eb-bf46-b816f5257b26"
                }
            ]
        },
        {
            "id": "eaeaa05b-766f-4d36-acbc-0be15c36e961",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3432348-1c61-423d-be28-703918a0266a",
            "compositeImage": {
                "id": "4ab4675b-c317-4e43-b8a0-08ffa071950b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaeaa05b-766f-4d36-acbc-0be15c36e961",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaa9e63a-32ac-417c-8491-217f58a7e519",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaeaa05b-766f-4d36-acbc-0be15c36e961",
                    "LayerId": "8d8b69ab-e7e4-44eb-bf46-b816f5257b26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 444,
    "layers": [
        {
            "id": "8d8b69ab-e7e4-44eb-bf46-b816f5257b26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3432348-1c61-423d-be28-703918a0266a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 694,
    "xorig": 0,
    "yorig": 0
}