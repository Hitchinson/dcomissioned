{
    "id": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerWalkStart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 71,
    "bbox_right": 265,
    "bbox_top": 75,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "41d2e465-73c6-42af-bdc4-9655b142c545",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "5d91a4c8-036c-419c-8386-4eebb7701d41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41d2e465-73c6-42af-bdc4-9655b142c545",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37bcc507-c8cf-4511-ac03-fcad803fe3be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41d2e465-73c6-42af-bdc4-9655b142c545",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "fb3a803f-ab14-49f6-8d4f-a2c0b3eec345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "3b2050fd-eb44-4106-963f-d17d45ec8072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb3a803f-ab14-49f6-8d4f-a2c0b3eec345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ace0ce27-594b-46e2-b18e-1f5d9ae997fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb3a803f-ab14-49f6-8d4f-a2c0b3eec345",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "d15d2d6b-219f-4657-96d4-bac30ae4d286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "d3514630-8b71-48e6-a081-0f458139d35e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d15d2d6b-219f-4657-96d4-bac30ae4d286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cc35c6b-219d-485f-96a8-adad3331ccf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d15d2d6b-219f-4657-96d4-bac30ae4d286",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "1a346c84-0b14-436f-8ba6-71ba2922b6b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "002a1278-a096-4b39-a270-e06d41fb8717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a346c84-0b14-436f-8ba6-71ba2922b6b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d745314-bc81-440d-8c98-3e88832f74cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a346c84-0b14-436f-8ba6-71ba2922b6b0",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "1b27d29b-fae5-40dc-a712-079929f4f6f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "67463855-044d-4908-9cf4-97b36690acab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b27d29b-fae5-40dc-a712-079929f4f6f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0c78300-49f0-41f2-a4a8-6fcdc36b8616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b27d29b-fae5-40dc-a712-079929f4f6f5",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "156aa63d-49f4-430e-8e46-b46d2ae12f99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "b956a942-88e7-4f77-a0ac-a4b772bc74bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "156aa63d-49f4-430e-8e46-b46d2ae12f99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d75dd2d0-8643-4417-bc76-e0efdc87e7e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "156aa63d-49f4-430e-8e46-b46d2ae12f99",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "5461801e-6a19-4d65-b085-6b12debcb8e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "88db58b0-1c41-4afa-90d1-180ce4556b02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5461801e-6a19-4d65-b085-6b12debcb8e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "580513d7-95de-4db4-8933-3388a55e3835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5461801e-6a19-4d65-b085-6b12debcb8e0",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "64675747-bc79-46c8-b21d-49216e81020c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "871c07c1-e339-4e9a-82aa-ae26b6cf1066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64675747-bc79-46c8-b21d-49216e81020c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1f2c354-ec93-4325-8641-da545068dd97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64675747-bc79-46c8-b21d-49216e81020c",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "05640f8e-1c7f-44ae-acb7-f8db08d1330e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "6b297176-bbe0-4e33-a4a2-a692d43c2e4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05640f8e-1c7f-44ae-acb7-f8db08d1330e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd0e2375-fe1f-4a24-b3a3-02097682fe28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05640f8e-1c7f-44ae-acb7-f8db08d1330e",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "7ce69ba9-7933-4d46-9ff3-9d755588072b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "266a7189-baab-4d3e-a9a5-bae8e7bdf799",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ce69ba9-7933-4d46-9ff3-9d755588072b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9d3ac61-9cc6-4227-84eb-072cf76f2c53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ce69ba9-7933-4d46-9ff3-9d755588072b",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "d2efb4a0-2f90-4781-ad68-cef3fb21f5eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "21989aff-6c97-4bce-a26b-9c7f97bc1989",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2efb4a0-2f90-4781-ad68-cef3fb21f5eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d68ce11-7d01-436f-81ab-feb1147593a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2efb4a0-2f90-4781-ad68-cef3fb21f5eb",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "1ad632b9-61da-4462-a9c1-74c6c7e33d67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "a675fe70-fcf1-46ca-b418-8f24e5b83f3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ad632b9-61da-4462-a9c1-74c6c7e33d67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aadeafa-019c-4a77-83a5-b2124f854c9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ad632b9-61da-4462-a9c1-74c6c7e33d67",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "e554d170-2427-4586-8f35-9078b9673da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "398049e5-cd91-406e-888b-70f3e15595e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e554d170-2427-4586-8f35-9078b9673da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0960c78a-fd89-4402-8e48-97d4879805af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e554d170-2427-4586-8f35-9078b9673da1",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "c93ff7ee-9bb7-46e5-82b4-16b1610a046c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "435e1c6d-2a07-4d9b-9bd1-002e4dbd1b30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c93ff7ee-9bb7-46e5-82b4-16b1610a046c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "599d7c86-62de-46ef-9d63-8c12c58a02d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c93ff7ee-9bb7-46e5-82b4-16b1610a046c",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        },
        {
            "id": "cfbc90af-ed30-4593-8c5f-e452018b0f16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "compositeImage": {
                "id": "219fa504-a574-476f-90a9-96a443fb0284",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfbc90af-ed30-4593-8c5f-e452018b0f16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abe0b7a7-c6cc-4446-b43b-811ba9cb271c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfbc90af-ed30-4593-8c5f-e452018b0f16",
                    "LayerId": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "cdfdbca6-1acc-410c-9cd2-34c0b3ea492c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48f8549c-b5ff-41cc-a6ca-b41c95779a30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 164,
    "yorig": 144
}