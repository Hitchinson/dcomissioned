{
    "id": "a455317b-66c7-4a46-ac6f-814e2df5cb4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SkyBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2879,
    "bbox_left": 0,
    "bbox_right": 11999,
    "bbox_top": 960,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4dcb353e-b822-4a53-8c7e-dc480cd3a5d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a455317b-66c7-4a46-ac6f-814e2df5cb4a",
            "compositeImage": {
                "id": "dd0c0c2e-6979-46bb-8694-c738903be3d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dcb353e-b822-4a53-8c7e-dc480cd3a5d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a0b66bd-acf9-4286-b33e-887a41e6db32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dcb353e-b822-4a53-8c7e-dc480cd3a5d8",
                    "LayerId": "e5d7d595-fd56-42d0-b0b5-4bd545bf8b95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2880,
    "layers": [
        {
            "id": "e5d7d595-fd56-42d0-b0b5-4bd545bf8b95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a455317b-66c7-4a46-ac6f-814e2df5cb4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12000,
    "xorig": 0,
    "yorig": 0
}