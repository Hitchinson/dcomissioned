{
    "id": "d0a269ec-41eb-4574-be3c-92a0d7337378",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerJumpUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 71,
    "bbox_right": 265,
    "bbox_top": 75,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c3ef6019-9663-4a9d-9e75-a4228b68d22e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0a269ec-41eb-4574-be3c-92a0d7337378",
            "compositeImage": {
                "id": "88fba86b-bd16-41f0-92db-1b9832fde3a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3ef6019-9663-4a9d-9e75-a4228b68d22e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a8424b6-db5e-424d-a7ea-22f2c888a2b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3ef6019-9663-4a9d-9e75-a4228b68d22e",
                    "LayerId": "7940cc8c-f147-4775-ada0-f3daff394b02"
                }
            ]
        },
        {
            "id": "0576aa93-0c9f-4888-9b00-513e94c03778",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0a269ec-41eb-4574-be3c-92a0d7337378",
            "compositeImage": {
                "id": "d522caba-3223-4851-938d-f7c7a161e2cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0576aa93-0c9f-4888-9b00-513e94c03778",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b5df85a-7e28-44e6-8175-f747c934320e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0576aa93-0c9f-4888-9b00-513e94c03778",
                    "LayerId": "7940cc8c-f147-4775-ada0-f3daff394b02"
                }
            ]
        },
        {
            "id": "0e3e11fc-0e59-4d7f-aec4-6da6678e9354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0a269ec-41eb-4574-be3c-92a0d7337378",
            "compositeImage": {
                "id": "c862cfee-e3c5-475c-8920-2792545efd3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e3e11fc-0e59-4d7f-aec4-6da6678e9354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d5f5ddd-96dc-4690-b5f2-b8d336ea40db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e3e11fc-0e59-4d7f-aec4-6da6678e9354",
                    "LayerId": "7940cc8c-f147-4775-ada0-f3daff394b02"
                }
            ]
        },
        {
            "id": "b8b6ad65-9cba-4361-8bed-aceefe5766a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0a269ec-41eb-4574-be3c-92a0d7337378",
            "compositeImage": {
                "id": "d0ce854f-d7c4-4ee7-a4f1-c110c33b0a5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8b6ad65-9cba-4361-8bed-aceefe5766a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60baee72-23ca-4a77-96b6-82dcf3998596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8b6ad65-9cba-4361-8bed-aceefe5766a3",
                    "LayerId": "7940cc8c-f147-4775-ada0-f3daff394b02"
                }
            ]
        },
        {
            "id": "6db44359-ffa3-4bbf-ae4b-a9400a5149da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0a269ec-41eb-4574-be3c-92a0d7337378",
            "compositeImage": {
                "id": "862be5c5-9150-412f-b3b8-3a6bb0b604bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6db44359-ffa3-4bbf-ae4b-a9400a5149da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3cd891f-053a-44b4-a8d9-e44ce3ef51a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6db44359-ffa3-4bbf-ae4b-a9400a5149da",
                    "LayerId": "7940cc8c-f147-4775-ada0-f3daff394b02"
                }
            ]
        },
        {
            "id": "8b266db8-0cb4-471d-b566-b7ca16cacddd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0a269ec-41eb-4574-be3c-92a0d7337378",
            "compositeImage": {
                "id": "42a6597b-3e0a-4b80-95b4-4c89f0ca2b34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b266db8-0cb4-471d-b566-b7ca16cacddd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2370590-bc4a-455f-86c3-141d0142ee78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b266db8-0cb4-471d-b566-b7ca16cacddd",
                    "LayerId": "7940cc8c-f147-4775-ada0-f3daff394b02"
                }
            ]
        },
        {
            "id": "14ee143f-26e5-47e8-9208-6866fcabee7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0a269ec-41eb-4574-be3c-92a0d7337378",
            "compositeImage": {
                "id": "845fc858-e100-46fc-8b44-4c1ac75c8793",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14ee143f-26e5-47e8-9208-6866fcabee7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b794a7be-4e83-484b-802c-8a390a3af7b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14ee143f-26e5-47e8-9208-6866fcabee7c",
                    "LayerId": "7940cc8c-f147-4775-ada0-f3daff394b02"
                }
            ]
        },
        {
            "id": "3cfd559d-362a-400b-b583-7f0e43efe108",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0a269ec-41eb-4574-be3c-92a0d7337378",
            "compositeImage": {
                "id": "14dc9287-6632-4773-b913-30d9f54e714f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cfd559d-362a-400b-b583-7f0e43efe108",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a0347bd-8513-469f-a85a-8c0fa710525a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cfd559d-362a-400b-b583-7f0e43efe108",
                    "LayerId": "7940cc8c-f147-4775-ada0-f3daff394b02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "7940cc8c-f147-4775-ada0-f3daff394b02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0a269ec-41eb-4574-be3c-92a0d7337378",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 164,
    "yorig": 144
}