{
    "id": "deca4d61-5c18-4555-bce2-2c2fec343390",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EnemyType2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2bc5ebc1-c3a5-4a3f-bce4-6b5cb1977cf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "deca4d61-5c18-4555-bce2-2c2fec343390",
            "compositeImage": {
                "id": "f006f601-d134-42b5-937e-93b4c51ce1a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc5ebc1-c3a5-4a3f-bce4-6b5cb1977cf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6abaa148-0783-4d48-9a59-af076d8adb41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc5ebc1-c3a5-4a3f-bce4-6b5cb1977cf5",
                    "LayerId": "06d755cd-1328-4cd7-8b0d-77899f068e90"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "06d755cd-1328-4cd7-8b0d-77899f068e90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "deca4d61-5c18-4555-bce2-2c2fec343390",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}