{
    "id": "e3726050-bd87-4f50-a413-846101e0edde",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 71,
    "bbox_right": 265,
    "bbox_top": 75,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "17845529-5513-4bfd-af67-bdd18151d92e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "973df92b-796e-49c1-8c39-949af80ce001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17845529-5513-4bfd-af67-bdd18151d92e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b624d55-7041-4cd0-839c-5106d75322a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17845529-5513-4bfd-af67-bdd18151d92e",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "69be347c-92b2-40f4-a199-fc03d1409a3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "c984048b-a3ae-46c8-989b-81c4703ac538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69be347c-92b2-40f4-a199-fc03d1409a3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1941a8ad-e8fb-49f5-b2d5-b0a2173b1f20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69be347c-92b2-40f4-a199-fc03d1409a3a",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "9f5ab8e0-19f7-449e-a578-c7ce5dc2bba0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "b9a4ec02-5444-40ff-a1c2-627793f0a50a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f5ab8e0-19f7-449e-a578-c7ce5dc2bba0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca76fe5b-f99c-4bf6-aee1-2cf28eaf5820",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f5ab8e0-19f7-449e-a578-c7ce5dc2bba0",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "535559cb-bf7d-4c3e-bf78-3b933319210b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "f1be48b1-562b-478b-8e1d-35eac7791410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "535559cb-bf7d-4c3e-bf78-3b933319210b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "076cdcf3-f1c3-40eb-bc14-0f01e986aa18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "535559cb-bf7d-4c3e-bf78-3b933319210b",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "2d00a17f-31db-4355-986e-8dee9f680741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "54ad14a2-1e6a-4670-bc7c-2c1fa8907074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d00a17f-31db-4355-986e-8dee9f680741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3750d0ed-972e-402b-9083-db82bbcae0ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d00a17f-31db-4355-986e-8dee9f680741",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "04217ae2-bf42-46b9-9265-54933eb2092d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "28f7c768-4669-4e21-a787-def5db88c7a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04217ae2-bf42-46b9-9265-54933eb2092d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4071da72-a255-4c4d-9e62-b699d40996ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04217ae2-bf42-46b9-9265-54933eb2092d",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "6d9d9cb0-d995-4c36-8120-b7395334ee2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "cb9e091f-da25-43b5-a796-e74bf7e2e4b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d9d9cb0-d995-4c36-8120-b7395334ee2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2d19236-a8eb-4aef-8392-99d8cd5b756c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d9d9cb0-d995-4c36-8120-b7395334ee2f",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "fcaf4924-9f9e-44c8-8942-c085e8b22e55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "7c33901b-d82c-4a86-8acf-08ecd9231488",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcaf4924-9f9e-44c8-8942-c085e8b22e55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3943f38-ae86-420c-85ed-58881afe7b59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcaf4924-9f9e-44c8-8942-c085e8b22e55",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "c75a5a5f-7ac4-4e81-b290-790ce5da1c3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "48145cd7-abaf-49cf-b9fd-73ee5d730dfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c75a5a5f-7ac4-4e81-b290-790ce5da1c3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e933772-625c-4bc7-a51b-1af71379ca85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c75a5a5f-7ac4-4e81-b290-790ce5da1c3c",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "6eb08a14-21ee-4005-a46d-16fba761a41a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "a8d83efa-8057-4bc1-a5b5-f6b605f12d71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eb08a14-21ee-4005-a46d-16fba761a41a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e7e1066-2d92-48d4-81e8-f55d021ff77c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eb08a14-21ee-4005-a46d-16fba761a41a",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "cc8ce268-d0d6-40e4-8daa-226e267f17cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "e3309ba6-a7b8-47b6-872d-8ceb6729c3a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc8ce268-d0d6-40e4-8daa-226e267f17cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c732d572-83c1-44a6-82d3-cf0572591813",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc8ce268-d0d6-40e4-8daa-226e267f17cd",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "a497e7f4-940f-4203-aab9-6d0809042591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "8416b850-fe24-404a-9d09-5ae64828353f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a497e7f4-940f-4203-aab9-6d0809042591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be622ef6-3b9d-4aea-ba81-a0849937784a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a497e7f4-940f-4203-aab9-6d0809042591",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "04fe8f8b-b6dd-4886-a4a5-6695bc85ce6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "bca46630-ce12-4072-8448-bdd121761181",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04fe8f8b-b6dd-4886-a4a5-6695bc85ce6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08271d84-8748-46fb-9e6e-dc9df9e98a1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04fe8f8b-b6dd-4886-a4a5-6695bc85ce6e",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "ce46403d-138c-4c57-bbfd-cd6d96664fa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "e6d5db31-78c0-405c-8a56-d02f48e313ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce46403d-138c-4c57-bbfd-cd6d96664fa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7788eb4f-2c68-4a4a-84e2-d2fe17b6ec43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce46403d-138c-4c57-bbfd-cd6d96664fa5",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "2ba7feb3-258f-438f-b989-98710856fb3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "47937fe0-2c7f-45b0-859f-047d40bb2d30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ba7feb3-258f-438f-b989-98710856fb3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c14eae3-e57c-4abe-930b-23873db9cf8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ba7feb3-258f-438f-b989-98710856fb3a",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "ef03e3bf-c177-4039-8d8e-4c6ce4edef9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "2f27c245-1c7f-44f0-9119-54c7d2cedd37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef03e3bf-c177-4039-8d8e-4c6ce4edef9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "483a0ab8-7348-46e9-a733-52347bf3b465",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef03e3bf-c177-4039-8d8e-4c6ce4edef9d",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "a13047fe-766b-464b-a9ff-9c9a00344b85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "ddd9dce2-3b04-49d8-82c4-e643ab1ad031",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a13047fe-766b-464b-a9ff-9c9a00344b85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8edf2d-2882-447b-b37f-518b9b06ea0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a13047fe-766b-464b-a9ff-9c9a00344b85",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "ed828382-355c-40af-9816-abf4ec54b3da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "a62dc7bf-2718-4a45-ad0f-6dff18c5db9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed828382-355c-40af-9816-abf4ec54b3da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8c0fbba-2d17-46be-8e53-a4181523a1ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed828382-355c-40af-9816-abf4ec54b3da",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "011fd303-4f89-43fd-854f-326b17c948b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "a6592e7a-db41-4421-85ef-257b3c562feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "011fd303-4f89-43fd-854f-326b17c948b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcf65a20-19d9-4300-bc66-028c52e26b82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "011fd303-4f89-43fd-854f-326b17c948b3",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "12aed801-2269-4914-83ea-01c890510b0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "d833146d-1589-491a-8eb4-ea7f60a4d350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12aed801-2269-4914-83ea-01c890510b0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c5ad056-ad58-4a28-9c30-b3463e07761b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12aed801-2269-4914-83ea-01c890510b0a",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "223cec28-7b19-4867-bddb-9f159e091b00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "70f8a936-3405-4df9-82bc-15358c2d2f21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "223cec28-7b19-4867-bddb-9f159e091b00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fa54531-f9d1-48a7-aff8-160879555e74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "223cec28-7b19-4867-bddb-9f159e091b00",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "4c41ad46-b544-462b-aaa1-81d2f3506626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "3769a993-3ff6-49c0-8ac3-eb6bac5a70f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c41ad46-b544-462b-aaa1-81d2f3506626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f783f38-405d-4336-a84a-c098a9f7db80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c41ad46-b544-462b-aaa1-81d2f3506626",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "bd4e7aa8-3821-483e-a715-0a9414a5507c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "50902f17-33d1-4a25-bda4-a5e7f8376387",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd4e7aa8-3821-483e-a715-0a9414a5507c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b4ac89c-0f67-4e46-b868-6b492130b91f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd4e7aa8-3821-483e-a715-0a9414a5507c",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "63d5d265-1b30-459d-b75e-230dfd1ae9bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "8e0196a3-2ec4-4ba2-9784-65b121709df4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63d5d265-1b30-459d-b75e-230dfd1ae9bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94a73265-f794-41a2-a893-3829e708153d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63d5d265-1b30-459d-b75e-230dfd1ae9bd",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "3466f3b1-81f5-41f7-a126-0ca870f33ba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "dd650a4a-d3a6-40fd-8ab9-1858cfd71c1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3466f3b1-81f5-41f7-a126-0ca870f33ba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25ef3fca-338d-4804-8e6b-d8dd9aac5dad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3466f3b1-81f5-41f7-a126-0ca870f33ba3",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "3d9c3118-2974-4cbf-b367-939af3b27d3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "71ae4932-7d5f-493a-b3e0-d73d36123d95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d9c3118-2974-4cbf-b367-939af3b27d3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71e877ea-6a47-4c57-b8c8-d1026b50c562",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d9c3118-2974-4cbf-b367-939af3b27d3d",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "2fe7e784-4d44-454a-909a-eeadb7cdc22f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "97f18460-f87d-49ff-892e-3549cc8c1c6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fe7e784-4d44-454a-909a-eeadb7cdc22f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91aac3e7-133d-488a-ad05-627be7eefb7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fe7e784-4d44-454a-909a-eeadb7cdc22f",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "4d388bde-fb01-4404-808b-d51826edbf95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "f07878c7-f23f-4070-aabe-ca0dad38a47e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d388bde-fb01-4404-808b-d51826edbf95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f789ed5-89d9-4cd5-bfce-7309cea64b4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d388bde-fb01-4404-808b-d51826edbf95",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "e0ef366d-f467-4e62-aa70-bd152561be75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "78f28e97-271e-4ad8-93c3-cb736f1e5080",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0ef366d-f467-4e62-aa70-bd152561be75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c88d4b8-5bf9-4917-8bb5-f7964496e071",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0ef366d-f467-4e62-aa70-bd152561be75",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "dbe73da3-294d-4327-8455-88ff38cfc86e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "4e07bc60-e25c-47b5-88b0-210548f98f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbe73da3-294d-4327-8455-88ff38cfc86e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "353d5bff-98de-47af-b7e6-bfc983fb4d0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbe73da3-294d-4327-8455-88ff38cfc86e",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "00ab4363-e88c-411a-85c1-af9195f6e380",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "e73359d8-e2cd-4e47-a12a-4664daa1be16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00ab4363-e88c-411a-85c1-af9195f6e380",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f85ad27e-2a44-494a-8b79-75e8e15493f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00ab4363-e88c-411a-85c1-af9195f6e380",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "24cdfcd1-c95d-4466-b4f5-99113c1af764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "481df5a0-9763-4a00-9915-25f1b8c685f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24cdfcd1-c95d-4466-b4f5-99113c1af764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd661048-29a5-489e-a9d0-961e5784fd7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24cdfcd1-c95d-4466-b4f5-99113c1af764",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "31d9f4d9-98b6-45b2-aa76-df83430125d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "df565c61-3aa8-4d8e-b7e7-61a4caee9cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31d9f4d9-98b6-45b2-aa76-df83430125d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f4f3062-4656-42ec-b1da-f6742c32aec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31d9f4d9-98b6-45b2-aa76-df83430125d5",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "f1b99f9f-a2c0-4d7a-9fea-51ad049647a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "098b5dfd-dfa6-41de-9dc8-b9078d19e76e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1b99f9f-a2c0-4d7a-9fea-51ad049647a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f033164-9462-459a-a191-9571a25c63e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1b99f9f-a2c0-4d7a-9fea-51ad049647a6",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "c8445a6e-fc10-480f-b293-a6857caa18b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "0022f6b8-b5c6-427e-9943-b32b0a686498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8445a6e-fc10-480f-b293-a6857caa18b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a5d66ec-2778-470c-9966-5cb2787d81e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8445a6e-fc10-480f-b293-a6857caa18b7",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "08eb34e6-f645-4be2-95a7-5f461448488c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "bcf3b31a-c2ff-4d61-957e-0761f06282fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08eb34e6-f645-4be2-95a7-5f461448488c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8350ee5a-41a9-4596-b4a3-3753b39b68f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08eb34e6-f645-4be2-95a7-5f461448488c",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "0b0825a2-46b6-4959-a4d2-bf8030a536ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "b81e76fe-ebf6-4e88-ba50-890f1a371deb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b0825a2-46b6-4959-a4d2-bf8030a536ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e89dc087-3ff0-4a8a-aeda-eb9fe7b454d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b0825a2-46b6-4959-a4d2-bf8030a536ce",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "529a4ecc-6df2-4b0b-9857-4c4fedfd8816",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "042ee3ad-1533-4139-b059-6e415926d4cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "529a4ecc-6df2-4b0b-9857-4c4fedfd8816",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e643458d-f4cb-46b6-8cce-c426dec633ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "529a4ecc-6df2-4b0b-9857-4c4fedfd8816",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "29979c82-55d4-480e-a927-a4e7e76fd026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "52b446ef-621b-4c6d-a2b4-2131111dd885",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29979c82-55d4-480e-a927-a4e7e76fd026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "416a8ef1-841b-40c7-b7e5-6331325e91c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29979c82-55d4-480e-a927-a4e7e76fd026",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "15339ce5-5ac7-40bb-8535-5158b998c207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "bffcb848-c9e0-4e09-b1cc-c731d7f85dcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15339ce5-5ac7-40bb-8535-5158b998c207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed879de2-630b-45f8-be62-18114d3ab6fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15339ce5-5ac7-40bb-8535-5158b998c207",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "760e24cb-6a62-4f49-8e6a-5f7a842b33a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "5b600b10-fb9f-49cf-ae8a-eca4d5588a15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "760e24cb-6a62-4f49-8e6a-5f7a842b33a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eb28bd1-c508-4162-9b72-1bee847e6ee1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "760e24cb-6a62-4f49-8e6a-5f7a842b33a4",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "cb805154-6a05-4c91-99b3-45307d9492ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "1cad0b86-a3e8-408c-9975-0f730ace31a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb805154-6a05-4c91-99b3-45307d9492ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70884dd5-2ab4-4572-a7b4-2361fb9c9cef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb805154-6a05-4c91-99b3-45307d9492ed",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "709b0c9a-c71e-457e-9fb5-43a98b87b016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "b541458b-0efb-47a1-b98a-4b4feeefa4f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "709b0c9a-c71e-457e-9fb5-43a98b87b016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff211519-b3a5-4b75-8840-6d5b789d6f3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "709b0c9a-c71e-457e-9fb5-43a98b87b016",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "cb6c2bab-5c59-4956-b9bd-a928bc0f4787",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "b185037a-aefd-406f-a96c-85b60160691b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb6c2bab-5c59-4956-b9bd-a928bc0f4787",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "061a6614-ca8d-40bd-87ea-fc53fef7ea05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb6c2bab-5c59-4956-b9bd-a928bc0f4787",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "893eaf46-a226-4590-8be1-a1b5f8c93984",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "1094809a-b861-4911-b83e-702726663464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "893eaf46-a226-4590-8be1-a1b5f8c93984",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c16a747-fd47-4eae-833e-51d3f79244e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "893eaf46-a226-4590-8be1-a1b5f8c93984",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "451aa296-c9d0-46ff-9169-8b3c5e62b684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "f0548277-e1be-42f0-9b42-66d4c0ac8694",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "451aa296-c9d0-46ff-9169-8b3c5e62b684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67373037-762d-4a46-9900-fe538011e1b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "451aa296-c9d0-46ff-9169-8b3c5e62b684",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "73ffe40a-2d56-4885-80fe-93a00e428fd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "99176fd0-c761-4630-add2-d2ec9f2edfab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73ffe40a-2d56-4885-80fe-93a00e428fd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "863f323b-02ac-4d2a-bb3b-d78ded114260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73ffe40a-2d56-4885-80fe-93a00e428fd7",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "abc05caf-e86c-4975-b5f3-3201c135b308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "c7ad41fb-e157-4adc-9b6d-06f8c0ff474d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abc05caf-e86c-4975-b5f3-3201c135b308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a346e772-30dd-436b-9028-bf51ef2f10ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abc05caf-e86c-4975-b5f3-3201c135b308",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        },
        {
            "id": "ce6d9021-0af4-4370-a26e-a5eebf65eb93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "compositeImage": {
                "id": "996dd2fd-58f0-4747-94ad-a1cce33e561b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce6d9021-0af4-4370-a26e-a5eebf65eb93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7c1fa62-6554-4478-bdb2-ec3f7a4e366f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce6d9021-0af4-4370-a26e-a5eebf65eb93",
                    "LayerId": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "bfa95d6a-84ad-4942-85fd-2736e2cf7cd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3726050-bd87-4f50-a413-846101e0edde",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 164,
    "yorig": 144
}