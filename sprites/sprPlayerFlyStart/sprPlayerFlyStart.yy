{
    "id": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerFlyStart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 71,
    "bbox_right": 265,
    "bbox_top": 75,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1aa30f8b-5410-4ebe-b4e0-2b4958731da7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "53f90019-785b-45f3-b6c5-4271f7df3b7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aa30f8b-5410-4ebe-b4e0-2b4958731da7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d6c7230-87de-4043-a035-dd569ab92db6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aa30f8b-5410-4ebe-b4e0-2b4958731da7",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "f1b4fc12-fd12-473b-ab90-3b67557078c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "d1e4be94-49b3-402d-8b5e-02069b2c1028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1b4fc12-fd12-473b-ab90-3b67557078c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82d26918-5ab8-4211-a223-0305bc7e8821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1b4fc12-fd12-473b-ab90-3b67557078c1",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "f7e81327-63ae-4114-b746-aa745c693a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "23cf1ca7-57e0-4f9e-8af7-4ff24e03c64a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7e81327-63ae-4114-b746-aa745c693a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec27075a-9d5e-444f-860d-a495fdde33dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7e81327-63ae-4114-b746-aa745c693a40",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "db233683-fe1c-4bd5-92c9-dd03526ff7a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "6ed5147a-1a51-433c-a472-11ee94bae3d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db233683-fe1c-4bd5-92c9-dd03526ff7a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "322c3d08-a02b-4c05-a85b-c4b79af863cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db233683-fe1c-4bd5-92c9-dd03526ff7a4",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "666f837a-aec7-4564-a4fd-4299852c8b1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "b118f73e-7a26-45e7-8157-c844c77f03b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "666f837a-aec7-4564-a4fd-4299852c8b1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a4085fb-6ffc-4347-b8c5-4fc96ff3f0ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "666f837a-aec7-4564-a4fd-4299852c8b1f",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "06437752-7a4f-4b5d-b4d6-f14dd9aac1e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "a5d455f8-da1c-4467-a424-fceecc0c7cf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06437752-7a4f-4b5d-b4d6-f14dd9aac1e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad17f656-c660-4952-adfb-38c8eaa90551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06437752-7a4f-4b5d-b4d6-f14dd9aac1e7",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "336f97e2-c6ea-4005-b6a5-6c1dd0c5d8a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "a60a78ec-7e17-41fc-8b4d-2e1be6a225b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "336f97e2-c6ea-4005-b6a5-6c1dd0c5d8a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b472783a-40a3-4640-9509-5167a25500a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "336f97e2-c6ea-4005-b6a5-6c1dd0c5d8a5",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "29dc1743-7da9-4571-abd5-eda005340cf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "2498c55d-4257-478b-99cf-1186c8b9e5e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29dc1743-7da9-4571-abd5-eda005340cf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04a4b8b7-e209-41a1-bdf4-9ad934aa3a3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29dc1743-7da9-4571-abd5-eda005340cf7",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "6690da18-9491-4c2b-9602-60e8cbc29771",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "ddcab2d5-6d77-4f94-bd16-d568b5d12450",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6690da18-9491-4c2b-9602-60e8cbc29771",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9957577c-fd60-4dc4-b019-c2499f54c3db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6690da18-9491-4c2b-9602-60e8cbc29771",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "b05cce9a-cfd4-4d89-83be-ba5f16ee15b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "7fbdc493-0a1b-4549-b3d4-0bbdcfbe3d86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b05cce9a-cfd4-4d89-83be-ba5f16ee15b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3785152a-5313-4754-8cf9-cc6fce7d300a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b05cce9a-cfd4-4d89-83be-ba5f16ee15b6",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "8984e8da-ae50-402f-b3fd-86c76ac9cb14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "78cb022f-bc7e-4830-8eb2-e07747d010a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8984e8da-ae50-402f-b3fd-86c76ac9cb14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd27d509-0ea0-4bc7-87a9-ea64109f8ba9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8984e8da-ae50-402f-b3fd-86c76ac9cb14",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "289641d4-6b37-4d1a-a763-a0363415d771",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "75a318da-ddf0-4f9a-8b7b-bb1b4e174327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "289641d4-6b37-4d1a-a763-a0363415d771",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2f6431f-9948-4c98-9cfb-a45b99207a54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "289641d4-6b37-4d1a-a763-a0363415d771",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "9a9aec37-06c6-4d1d-821d-f2b723a89441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "f46feb57-c0ab-49e9-abd6-1282a040399d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a9aec37-06c6-4d1d-821d-f2b723a89441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b72ba21-7d43-478c-b19c-35d0c2e0110f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9aec37-06c6-4d1d-821d-f2b723a89441",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "b6f80955-23cc-4dae-9687-9af16149b32c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "1030acf9-1397-4171-ac83-4dccb183b504",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6f80955-23cc-4dae-9687-9af16149b32c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "701fea3b-301b-402b-8822-3b7b3272e588",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6f80955-23cc-4dae-9687-9af16149b32c",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "10d8a4a0-0c78-418d-b59c-e5a0307e4472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "2688a194-0868-4c90-8ea3-7a407d43cce8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10d8a4a0-0c78-418d-b59c-e5a0307e4472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eabae4ba-8c4c-427d-b2da-98a81b421adc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d8a4a0-0c78-418d-b59c-e5a0307e4472",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "7d6be4da-69c1-43cd-8778-cdaa162e68aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "8c299801-c811-4807-b3c5-46b99e506773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d6be4da-69c1-43cd-8778-cdaa162e68aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77a407ea-c907-49e8-98ff-b96daf613f8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d6be4da-69c1-43cd-8778-cdaa162e68aa",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "d2e025e8-b1d8-4351-9395-4960b819b643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "4c12e862-b1d8-4888-9295-5229bb84f04c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2e025e8-b1d8-4351-9395-4960b819b643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f858681-962d-42ff-9cf9-8896237da337",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2e025e8-b1d8-4351-9395-4960b819b643",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "d9c1ba5e-75e3-4a6b-8f23-270c589c67a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "09bbf841-4b2a-4562-bf3b-b38868d65f77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9c1ba5e-75e3-4a6b-8f23-270c589c67a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f351283-f010-42f7-b14a-b886ee651a45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9c1ba5e-75e3-4a6b-8f23-270c589c67a3",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "1574cbfa-f799-4058-a037-46b45ed78280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "76da5b23-c4dc-40b5-9f3b-7898dd522bab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1574cbfa-f799-4058-a037-46b45ed78280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8db3688-27f2-4898-93fd-9e57e3847da6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1574cbfa-f799-4058-a037-46b45ed78280",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "e9d1fe84-3688-42f6-8cfd-884cae84741d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "e5d39690-efc3-49f8-844f-85a4b016d139",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9d1fe84-3688-42f6-8cfd-884cae84741d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c42bafe-3b34-4fcc-bb1b-bba9eb5368b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9d1fe84-3688-42f6-8cfd-884cae84741d",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "badcc09c-551e-4e14-88e9-3875b9a16122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "27fccda5-73bf-4942-a638-1898702ac2e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "badcc09c-551e-4e14-88e9-3875b9a16122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "407d8aa1-aa0d-4939-a053-1faeb66d49cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "badcc09c-551e-4e14-88e9-3875b9a16122",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "3db1a14f-d6f6-431d-8d8f-2771c439a175",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "7abd80fd-7521-46b4-9144-4e5817a26875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3db1a14f-d6f6-431d-8d8f-2771c439a175",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b231bcf-7670-4ada-87f5-015bfeacf572",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3db1a14f-d6f6-431d-8d8f-2771c439a175",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "c620aca1-a137-4644-8eb6-01b68c233de5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "d5add1a3-c15a-486d-b9c6-085839d5ecc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c620aca1-a137-4644-8eb6-01b68c233de5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b87d4c4-fd36-48a1-b797-23e3db120faa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c620aca1-a137-4644-8eb6-01b68c233de5",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "00fe1541-7792-4f0d-a666-7e5c7ca45bf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "11459476-026d-4372-bc83-0f119d5e91d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00fe1541-7792-4f0d-a666-7e5c7ca45bf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed4bbf91-3578-42cf-8110-7ff5056a19aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00fe1541-7792-4f0d-a666-7e5c7ca45bf6",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "785c6d98-0e68-4fa6-b2e4-1480226dee14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "1fb5d106-f8ef-4bbe-a10c-c58ea6d96651",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "785c6d98-0e68-4fa6-b2e4-1480226dee14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c5d48ca-157a-41f0-8541-c543b5108e04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "785c6d98-0e68-4fa6-b2e4-1480226dee14",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "1008de20-c8cc-40e7-a4ce-0b30ba840152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "c47e82be-d4ba-4eb8-8052-48ef8396a55b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1008de20-c8cc-40e7-a4ce-0b30ba840152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "992daf21-dd0a-49bb-ab9d-1f136ccfd793",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1008de20-c8cc-40e7-a4ce-0b30ba840152",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "87385861-a8df-41f1-9e7f-b01c07f32759",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "c7613762-faf6-436c-abb2-923d6023e768",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87385861-a8df-41f1-9e7f-b01c07f32759",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f761c19-d182-41b0-bf0d-fdf848d9448d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87385861-a8df-41f1-9e7f-b01c07f32759",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "b06e99a6-777a-435c-83a9-f8cebeb0ceb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "a5bce6b2-e660-4a5d-b640-8137d73830ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b06e99a6-777a-435c-83a9-f8cebeb0ceb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90cd34ed-5c93-4df0-ac67-5204d0b7f196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b06e99a6-777a-435c-83a9-f8cebeb0ceb1",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "3a6ad045-620a-47bf-a5fb-9486bae43f45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "13829ec0-cbb3-4a24-9709-9b936a92f9b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a6ad045-620a-47bf-a5fb-9486bae43f45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fde56c06-c96b-482f-97bc-0658b11d4bfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a6ad045-620a-47bf-a5fb-9486bae43f45",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "73a9f558-3dd9-49ea-9483-1db55798a70d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "67cd49ec-7364-4b9f-808d-88507ec830d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73a9f558-3dd9-49ea-9483-1db55798a70d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae701ecc-0aaa-42b9-a25f-51a4fcb09e3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73a9f558-3dd9-49ea-9483-1db55798a70d",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "ba5ee57f-2b73-4fa8-a348-562e985d6944",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "5ca1cac4-c725-4bab-89aa-c2ffb4d89524",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba5ee57f-2b73-4fa8-a348-562e985d6944",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a392282e-ea28-440b-9bf1-55a6899aaafa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba5ee57f-2b73-4fa8-a348-562e985d6944",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "87f50db6-6164-48ae-8741-a89990c72764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "e15b01ff-322d-4ab6-b222-a7762f4a84d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87f50db6-6164-48ae-8741-a89990c72764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ef8e48f-c7aa-4682-ac94-9bccdd7755c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87f50db6-6164-48ae-8741-a89990c72764",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "361fb007-5508-4ce6-b4ee-0bcd60c34efb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "1c68f9be-7f91-4a9b-acee-bcf7977cae0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "361fb007-5508-4ce6-b4ee-0bcd60c34efb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3640809-061b-4495-be26-b1d7c29287fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "361fb007-5508-4ce6-b4ee-0bcd60c34efb",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "7c6b4212-a5cd-4e1e-89df-ba83848171bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "c1b9137f-2de8-4b6a-acd1-67b84503b37a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c6b4212-a5cd-4e1e-89df-ba83848171bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32f7f3c5-2e56-4790-93eb-ab0d6563d77f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c6b4212-a5cd-4e1e-89df-ba83848171bc",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "fde6ca07-31ed-4003-a033-f1293699c960",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "fd4e847a-078a-436e-b907-a5a659ab01b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fde6ca07-31ed-4003-a033-f1293699c960",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74cc0cde-986f-4dd5-a38e-b81de41dda84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde6ca07-31ed-4003-a033-f1293699c960",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "c4c5eb32-d9b1-41a8-8aea-d52a3fea0811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "c5544399-f843-4c95-9580-fd93836a9aa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4c5eb32-d9b1-41a8-8aea-d52a3fea0811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "758c913c-6e2b-4a98-8b0e-b74c42c23c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4c5eb32-d9b1-41a8-8aea-d52a3fea0811",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "4b5961d4-cc19-43e3-9c08-df3313a84d36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "f197f726-c0b0-42c8-9b43-94bb9f6a4db6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b5961d4-cc19-43e3-9c08-df3313a84d36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eb64306-1527-4399-9b25-ccdea8c778b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b5961d4-cc19-43e3-9c08-df3313a84d36",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "7f439ec9-f396-498e-93bd-176fce99f57b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "d2297090-6f1f-4530-9dac-49ac7a5e91bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f439ec9-f396-498e-93bd-176fce99f57b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8403d0b7-be01-4658-8b89-b95c2206d545",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f439ec9-f396-498e-93bd-176fce99f57b",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "df8a39b8-bd16-4a3e-9bdf-b7dc09160856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "b4c3688c-7f00-4476-a137-2f88f3f1598a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df8a39b8-bd16-4a3e-9bdf-b7dc09160856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6711c8b-affe-4994-a7c1-9f9e3ae1e2a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df8a39b8-bd16-4a3e-9bdf-b7dc09160856",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "7225658f-9ba2-42f1-bc14-fc31cd40d53b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "6cbf0e18-c695-450b-bdec-a96fc4a298c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7225658f-9ba2-42f1-bc14-fc31cd40d53b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dc32256-47bd-4d02-ab88-4cd258966efa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7225658f-9ba2-42f1-bc14-fc31cd40d53b",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "422ba5f9-de69-43fa-a906-27969c1aec9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "3176ff61-e0b8-4991-a100-b5718ed2ec74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "422ba5f9-de69-43fa-a906-27969c1aec9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f2e253d-1e41-4f77-a2f6-4688a90d9f9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "422ba5f9-de69-43fa-a906-27969c1aec9c",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "b554bb0d-7d08-453d-9969-7c8a3bc5d583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "a27e2037-7b14-4c88-9396-6012202d1f0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b554bb0d-7d08-453d-9969-7c8a3bc5d583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2c6b425-1be9-4612-a67c-33862e565371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b554bb0d-7d08-453d-9969-7c8a3bc5d583",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "5260f1a3-d9c2-4de4-8c13-908fc806f5ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "db04a876-864c-488c-ad7b-b7bcecd6a4e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5260f1a3-d9c2-4de4-8c13-908fc806f5ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58d592bf-1674-4c1a-bd3f-926f1dee0e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5260f1a3-d9c2-4de4-8c13-908fc806f5ef",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "f62f5496-d254-4b6b-adb6-f7d7d0defb4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "538b2740-31ab-453e-a1c3-1ebe9b46acb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f62f5496-d254-4b6b-adb6-f7d7d0defb4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6a9d5bc-2acc-45c0-a711-12fa63f4085f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f62f5496-d254-4b6b-adb6-f7d7d0defb4c",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "8d258973-bdbf-4b67-a0ca-74b1e890b830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "075f5bf2-54d2-4f5a-a989-1304698d5ca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d258973-bdbf-4b67-a0ca-74b1e890b830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "271212cb-443c-4be5-87ab-b8ab0944d407",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d258973-bdbf-4b67-a0ca-74b1e890b830",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "7d1ce948-0f18-4991-ad3f-2c9089650fb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "0368825e-ff89-42fb-80af-244f4e722a47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d1ce948-0f18-4991-ad3f-2c9089650fb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd80838b-751b-449f-b568-87431ad70aaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d1ce948-0f18-4991-ad3f-2c9089650fb1",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "86f2df0d-cd18-4e73-962a-11d7b8f465e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "342661d0-8e97-4083-8d8e-5fb24decb04b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86f2df0d-cd18-4e73-962a-11d7b8f465e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e014f509-c04b-4383-9b97-2a13dcaa9f18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f2df0d-cd18-4e73-962a-11d7b8f465e9",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "eef407e1-b5a7-4b5a-ba4c-4389f0905067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "a78a9538-b8f1-4a38-99fd-368bace50749",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eef407e1-b5a7-4b5a-ba4c-4389f0905067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7b8ff01-e703-4654-be04-97542f7308e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eef407e1-b5a7-4b5a-ba4c-4389f0905067",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "2272fbe8-cf1b-4f8d-924c-29fcf55a4fca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "c0a88fd5-f298-4099-a909-05f84c311bf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2272fbe8-cf1b-4f8d-924c-29fcf55a4fca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8122c774-1e49-406a-b7a9-d0ee2d4d34b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2272fbe8-cf1b-4f8d-924c-29fcf55a4fca",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "24c98ed4-2ae7-4b30-883f-ed38fac42a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "11b2a698-d7d9-402e-b936-4acda883db41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24c98ed4-2ae7-4b30-883f-ed38fac42a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8691ddf9-23bf-4d92-a16e-de4ffb706cb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24c98ed4-2ae7-4b30-883f-ed38fac42a12",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "34a6d4f9-4c4d-4290-8dc6-d2abc2a2ab77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "c6caf049-31a1-4137-aba6-f2cdc23f57de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34a6d4f9-4c4d-4290-8dc6-d2abc2a2ab77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c428077-81a5-4aaf-aa1b-688d7fc7338c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34a6d4f9-4c4d-4290-8dc6-d2abc2a2ab77",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "b8ae0f4e-097b-4517-9c62-e21f9edf43f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "545c16f2-5593-4446-ae37-9aa073e8b918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8ae0f4e-097b-4517-9c62-e21f9edf43f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01c019dd-1982-4908-910a-68ac112961ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8ae0f4e-097b-4517-9c62-e21f9edf43f3",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "b762717f-1183-4761-8752-e2d5e1236df7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "6689aa58-a5a9-424c-9411-dda746dc7f3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b762717f-1183-4761-8752-e2d5e1236df7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cbb0803-0bd6-4551-acce-e45c33f5d9dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b762717f-1183-4761-8752-e2d5e1236df7",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "a87a82e6-d928-4473-9b80-815eaccff110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "4610e6a8-54b7-49a0-8b2b-53bca82cf692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a87a82e6-d928-4473-9b80-815eaccff110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5da5bee-b6dd-46bb-b818-0f9c32b7a911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a87a82e6-d928-4473-9b80-815eaccff110",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        },
        {
            "id": "4408b333-58b0-48ab-b6e7-53e95a97d932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "compositeImage": {
                "id": "44420865-6117-4b82-a9fc-7ef663c933a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4408b333-58b0-48ab-b6e7-53e95a97d932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aeb33cb-2048-4fc4-a7d1-aa3ab15c5817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4408b333-58b0-48ab-b6e7-53e95a97d932",
                    "LayerId": "420e555f-e979-4468-aad8-3ccba58ead33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "420e555f-e979-4468-aad8-3ccba58ead33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65d9f39f-ef50-4138-a497-8fe82c3a51a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 164,
    "yorig": 144
}