{
    "id": "0bffa598-1da9-4daf-9330-d9440a9ed7f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerJumpLand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 71,
    "bbox_right": 265,
    "bbox_top": 75,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a06952c1-0fcc-4a7e-935b-156906b9c994",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bffa598-1da9-4daf-9330-d9440a9ed7f3",
            "compositeImage": {
                "id": "7b8ad967-9244-4e7c-a5c3-eaeb8c0fcc55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a06952c1-0fcc-4a7e-935b-156906b9c994",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e214d406-b5df-46a3-ac45-52037ef05ac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a06952c1-0fcc-4a7e-935b-156906b9c994",
                    "LayerId": "c4040049-ceb0-4ca6-b1f4-f9c58a370544"
                }
            ]
        },
        {
            "id": "428ada78-0c45-4ba6-bb25-d3dd83f0239a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bffa598-1da9-4daf-9330-d9440a9ed7f3",
            "compositeImage": {
                "id": "da44b0bd-6702-4cf2-ad7d-5673390b00ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "428ada78-0c45-4ba6-bb25-d3dd83f0239a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c339922c-1796-49f3-86c7-ceee07cb58ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "428ada78-0c45-4ba6-bb25-d3dd83f0239a",
                    "LayerId": "c4040049-ceb0-4ca6-b1f4-f9c58a370544"
                }
            ]
        },
        {
            "id": "1d644bb6-a7ea-4c70-bc7f-9af4659b2576",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bffa598-1da9-4daf-9330-d9440a9ed7f3",
            "compositeImage": {
                "id": "4a091c3f-a35d-46b5-aeb3-a891490e7ecc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d644bb6-a7ea-4c70-bc7f-9af4659b2576",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d309387-371f-45e6-b57a-da81dc676a06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d644bb6-a7ea-4c70-bc7f-9af4659b2576",
                    "LayerId": "c4040049-ceb0-4ca6-b1f4-f9c58a370544"
                }
            ]
        },
        {
            "id": "870fad2a-3517-46a6-be10-9627a0bbf437",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bffa598-1da9-4daf-9330-d9440a9ed7f3",
            "compositeImage": {
                "id": "de3811e1-2db3-4ba9-939b-bbbe9e11b685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "870fad2a-3517-46a6-be10-9627a0bbf437",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e51c28e9-41ec-409d-8b4f-c393bdf82900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "870fad2a-3517-46a6-be10-9627a0bbf437",
                    "LayerId": "c4040049-ceb0-4ca6-b1f4-f9c58a370544"
                }
            ]
        },
        {
            "id": "180580bb-2c29-4d61-be58-7cd7cfce2176",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bffa598-1da9-4daf-9330-d9440a9ed7f3",
            "compositeImage": {
                "id": "47f56173-97e5-416d-8d13-96aec3983a16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "180580bb-2c29-4d61-be58-7cd7cfce2176",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34d2f333-09f9-4f5e-9da0-51b5026d8a73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "180580bb-2c29-4d61-be58-7cd7cfce2176",
                    "LayerId": "c4040049-ceb0-4ca6-b1f4-f9c58a370544"
                }
            ]
        },
        {
            "id": "91b93664-e3c0-4a82-b3d9-6281bf870a79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bffa598-1da9-4daf-9330-d9440a9ed7f3",
            "compositeImage": {
                "id": "6e3a2ace-eaca-46df-9c95-7f78f5323b2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91b93664-e3c0-4a82-b3d9-6281bf870a79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a06d547c-2524-4ece-b058-44d9072d43d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91b93664-e3c0-4a82-b3d9-6281bf870a79",
                    "LayerId": "c4040049-ceb0-4ca6-b1f4-f9c58a370544"
                }
            ]
        },
        {
            "id": "6bc3286d-1a2b-4bea-a0ac-c2a81d9133fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bffa598-1da9-4daf-9330-d9440a9ed7f3",
            "compositeImage": {
                "id": "26de05ce-d3fd-4699-b280-b08f638765b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bc3286d-1a2b-4bea-a0ac-c2a81d9133fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4169ee2-20f2-4a74-ada8-7a7ed2e8e719",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bc3286d-1a2b-4bea-a0ac-c2a81d9133fb",
                    "LayerId": "c4040049-ceb0-4ca6-b1f4-f9c58a370544"
                }
            ]
        },
        {
            "id": "4b2df356-134b-47f3-8dd5-fb357ecb31a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bffa598-1da9-4daf-9330-d9440a9ed7f3",
            "compositeImage": {
                "id": "5f23b2b4-97ff-4baa-a238-4a45617a5ff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b2df356-134b-47f3-8dd5-fb357ecb31a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd1f97f5-05e1-47f7-88c7-1404931416ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b2df356-134b-47f3-8dd5-fb357ecb31a8",
                    "LayerId": "c4040049-ceb0-4ca6-b1f4-f9c58a370544"
                }
            ]
        },
        {
            "id": "b70021b3-0d72-438d-8779-32aed94edc74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bffa598-1da9-4daf-9330-d9440a9ed7f3",
            "compositeImage": {
                "id": "b9824e1a-574a-4607-b61a-2d081f01c6f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b70021b3-0d72-438d-8779-32aed94edc74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13c1fbc8-57b7-422c-ab4d-e3fda7553de2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b70021b3-0d72-438d-8779-32aed94edc74",
                    "LayerId": "c4040049-ceb0-4ca6-b1f4-f9c58a370544"
                }
            ]
        },
        {
            "id": "bc93a07e-cf31-457a-9602-3afd28a07263",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bffa598-1da9-4daf-9330-d9440a9ed7f3",
            "compositeImage": {
                "id": "261b4928-a665-4d27-aa44-3ae7f042d0b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc93a07e-cf31-457a-9602-3afd28a07263",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eff9b5ab-5008-43cb-8522-d4edb5ed43ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc93a07e-cf31-457a-9602-3afd28a07263",
                    "LayerId": "c4040049-ceb0-4ca6-b1f4-f9c58a370544"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "c4040049-ceb0-4ca6-b1f4-f9c58a370544",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0bffa598-1da9-4daf-9330-d9440a9ed7f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 164,
    "yorig": 144
}