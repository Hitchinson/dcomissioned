{
    "id": "f4b12623-f868-4883-95d9-253ea382b482",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GunShoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 4,
    "bbox_right": 96,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c56e4677-9aff-40f8-bc03-01d996e8eace",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4b12623-f868-4883-95d9-253ea382b482",
            "compositeImage": {
                "id": "b8a72da7-f65a-4143-aeb0-6ecf109cc0a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c56e4677-9aff-40f8-bc03-01d996e8eace",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f0d9c8d-4003-4a2d-8270-cd18d1f568dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c56e4677-9aff-40f8-bc03-01d996e8eace",
                    "LayerId": "2d5571bb-9bc2-4185-a112-96252ab4deef"
                }
            ]
        },
        {
            "id": "b94380d6-d9cb-48fe-9ea9-f14b0453f5ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4b12623-f868-4883-95d9-253ea382b482",
            "compositeImage": {
                "id": "c7accc52-4b1c-434c-b938-a60ccd8326ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b94380d6-d9cb-48fe-9ea9-f14b0453f5ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b4626c6-ffaf-4969-9bc2-408aad3f21a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b94380d6-d9cb-48fe-9ea9-f14b0453f5ed",
                    "LayerId": "2d5571bb-9bc2-4185-a112-96252ab4deef"
                }
            ]
        },
        {
            "id": "ef1eb92d-6584-4d3f-8b26-3042dd4d82c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4b12623-f868-4883-95d9-253ea382b482",
            "compositeImage": {
                "id": "8870377b-36bb-4d49-b5bf-293198e27349",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef1eb92d-6584-4d3f-8b26-3042dd4d82c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b03cc97f-658c-45d7-8f57-5e0b114f070f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef1eb92d-6584-4d3f-8b26-3042dd4d82c2",
                    "LayerId": "2d5571bb-9bc2-4185-a112-96252ab4deef"
                }
            ]
        },
        {
            "id": "de817781-1dcb-47d3-8857-7d60c937c8bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4b12623-f868-4883-95d9-253ea382b482",
            "compositeImage": {
                "id": "bcbc5a71-311e-4d79-8460-c80365cc5d9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de817781-1dcb-47d3-8857-7d60c937c8bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da7b407d-c572-49b1-ae83-7bf719bd5e73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de817781-1dcb-47d3-8857-7d60c937c8bb",
                    "LayerId": "2d5571bb-9bc2-4185-a112-96252ab4deef"
                }
            ]
        },
        {
            "id": "7542b370-7e3c-47e2-911c-887f87d48669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4b12623-f868-4883-95d9-253ea382b482",
            "compositeImage": {
                "id": "19168f2d-85e1-45f7-9833-656001a99100",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7542b370-7e3c-47e2-911c-887f87d48669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c2d14f3-926d-4606-bd0b-052b77a59609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7542b370-7e3c-47e2-911c-887f87d48669",
                    "LayerId": "2d5571bb-9bc2-4185-a112-96252ab4deef"
                }
            ]
        },
        {
            "id": "e61b9d20-ceb1-4547-8c16-49b81c5b3d95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4b12623-f868-4883-95d9-253ea382b482",
            "compositeImage": {
                "id": "81eb28da-1d52-4b49-9a60-722abbbc1ce6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e61b9d20-ceb1-4547-8c16-49b81c5b3d95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff066d2-a0ee-4e1e-a134-f79336319c8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e61b9d20-ceb1-4547-8c16-49b81c5b3d95",
                    "LayerId": "2d5571bb-9bc2-4185-a112-96252ab4deef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 74,
    "layers": [
        {
            "id": "2d5571bb-9bc2-4185-a112-96252ab4deef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4b12623-f868-4883-95d9-253ea382b482",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 37
}