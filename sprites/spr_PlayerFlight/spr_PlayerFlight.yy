{
    "id": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerFlight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 153,
    "bbox_left": 13,
    "bbox_right": 205,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b59736e0-d522-4be4-82ee-266a3f55f733",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "a5c07774-f2f2-470a-bc43-5d845a1be195",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b59736e0-d522-4be4-82ee-266a3f55f733",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "875fb310-3c32-4a1d-afd3-5a087acc018e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b59736e0-d522-4be4-82ee-266a3f55f733",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "855bf1d8-aeed-41b1-a773-9a38768b52df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "0476c925-5b1b-4bf7-ac3c-90fca80e29b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "855bf1d8-aeed-41b1-a773-9a38768b52df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40ebc29-e114-4153-9d00-ed31f53d8b02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "855bf1d8-aeed-41b1-a773-9a38768b52df",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "8b175c5e-c7a4-4dd1-bf9b-505015460505",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "364ae795-4f13-4a34-a82d-66d4bcc1e8c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b175c5e-c7a4-4dd1-bf9b-505015460505",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99a9356e-2b32-43e7-b3a3-6ded42b83dd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b175c5e-c7a4-4dd1-bf9b-505015460505",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "3689b7fe-f5ea-42e5-bcf0-c8f0e5cdceaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "32c7f02f-b4a3-49c2-af16-0e7905a247ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3689b7fe-f5ea-42e5-bcf0-c8f0e5cdceaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a47052d-587e-4455-a8bf-ce85781bb7e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3689b7fe-f5ea-42e5-bcf0-c8f0e5cdceaa",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "ab493a5d-4f68-4dea-bb37-e8e369aa1f94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "55d34570-f82b-4865-915c-e6748d97de8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab493a5d-4f68-4dea-bb37-e8e369aa1f94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0d7dabf-eba9-4df1-9e6b-c3c00fa74d87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab493a5d-4f68-4dea-bb37-e8e369aa1f94",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "b81f9ac7-1c60-4bd4-b89c-d7073381d1b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "2253be15-af2f-4438-840d-e38ce00d97d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b81f9ac7-1c60-4bd4-b89c-d7073381d1b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c02f8b8-c00d-4e00-a91c-31a216a7f0d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b81f9ac7-1c60-4bd4-b89c-d7073381d1b2",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "46442d55-e11c-424d-b024-bff05aecf360",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "1804dd6f-b9eb-4d67-86d5-77140771ed20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46442d55-e11c-424d-b024-bff05aecf360",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a29eb40-3013-43b3-8099-7497193dac18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46442d55-e11c-424d-b024-bff05aecf360",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "057d86bf-0d93-4b26-b24d-d6f27c42d274",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "75c95f96-5502-4b9f-8df7-8b51647d0764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "057d86bf-0d93-4b26-b24d-d6f27c42d274",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac21d76d-963d-4167-b50a-d0116fef59ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "057d86bf-0d93-4b26-b24d-d6f27c42d274",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "516d1b75-c9ce-44e8-8283-dc19c988a109",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "58cc76ff-3c82-441f-b2d3-39c3ecd0aa19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "516d1b75-c9ce-44e8-8283-dc19c988a109",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c819d429-987d-42e4-9fb8-d33a194126a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "516d1b75-c9ce-44e8-8283-dc19c988a109",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "412ecc1e-b3e7-44be-9b2b-2fa852091645",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "173a8b57-e9e6-407d-bab2-bbc309693a20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "412ecc1e-b3e7-44be-9b2b-2fa852091645",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28771bd7-d7be-4702-817b-5f12a1ac8cbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "412ecc1e-b3e7-44be-9b2b-2fa852091645",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "827a3d1d-d31e-4ed6-9ef8-2e21b5f3ddd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "9dfa040b-4748-46d2-ba35-774dada42175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "827a3d1d-d31e-4ed6-9ef8-2e21b5f3ddd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a08d671-a56b-42cd-a50c-c0880a333b3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "827a3d1d-d31e-4ed6-9ef8-2e21b5f3ddd1",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "224720e0-b76d-43c0-a341-d5e3be926381",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "b75a69b9-f465-49a6-beba-25714dd5a968",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "224720e0-b76d-43c0-a341-d5e3be926381",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4efd464d-68ba-4b6c-98bf-3c712a7f0fe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "224720e0-b76d-43c0-a341-d5e3be926381",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "bb60501f-32ea-4429-8162-8292241aa9d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "db333b4f-acf1-4ea2-b9af-7c30b9ca5e47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb60501f-32ea-4429-8162-8292241aa9d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88a756aa-d779-4123-a4bf-a22dd1e37dd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb60501f-32ea-4429-8162-8292241aa9d2",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "2e6deb95-4138-489d-b966-625d37ec9efe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "a82f11ec-8852-40d6-b5fb-9d9a139d7afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e6deb95-4138-489d-b966-625d37ec9efe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b196a3e-225f-4273-872d-0dade072dd37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e6deb95-4138-489d-b966-625d37ec9efe",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "b25882fb-9dc6-458e-99f1-66ebae0ee486",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "94e212b2-a714-41cf-b58f-2e91fe7cd872",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b25882fb-9dc6-458e-99f1-66ebae0ee486",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daacfb04-0684-4683-93db-af1738fcb268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b25882fb-9dc6-458e-99f1-66ebae0ee486",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "c2f3ada0-f261-4f93-8e31-28ffd66961e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "412bf13c-856c-49f2-a4ce-f7fea2d26b1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2f3ada0-f261-4f93-8e31-28ffd66961e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb08ac2d-958f-4d3e-9fce-aa082b405c65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2f3ada0-f261-4f93-8e31-28ffd66961e0",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "3e8940bd-3279-4770-a7c9-799d864e6646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "515adee7-386d-4c59-b41e-f402c618e087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e8940bd-3279-4770-a7c9-799d864e6646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89121272-d2de-4374-bbe5-dfb5aaa4403f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e8940bd-3279-4770-a7c9-799d864e6646",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "3c32bbee-808e-4800-a73c-627c257f12ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "d30815df-9347-47de-9b6b-091ca47d6037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c32bbee-808e-4800-a73c-627c257f12ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "173a3014-6909-4992-8ccb-6daf5415ab58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c32bbee-808e-4800-a73c-627c257f12ac",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "4a073c51-5173-4bb4-aca6-c78482a81ccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "45f0b21c-c42c-4619-a640-72802157105b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a073c51-5173-4bb4-aca6-c78482a81ccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45d84646-1853-44b6-8586-8fbd19456b22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a073c51-5173-4bb4-aca6-c78482a81ccb",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "fe1f7750-d1e8-4cf1-ac8a-88a0d5e5acbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "0e13dc65-d526-44bd-9de5-da16976164f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe1f7750-d1e8-4cf1-ac8a-88a0d5e5acbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc072fce-2e12-4030-9902-c5e4765dcf55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe1f7750-d1e8-4cf1-ac8a-88a0d5e5acbf",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "89c59f25-58ea-43d3-97fe-91f27c8a0a62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "3720c013-2f40-44ef-99c6-514af7a73b51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89c59f25-58ea-43d3-97fe-91f27c8a0a62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "315e10fa-370c-4947-9fac-4d31105b823e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89c59f25-58ea-43d3-97fe-91f27c8a0a62",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "888b9a87-2ccd-471d-a2fd-13fa19eb58f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "d46dd4fe-9014-47d5-b816-99857a1b33b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "888b9a87-2ccd-471d-a2fd-13fa19eb58f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "769e171e-113e-4f2a-9f71-726f61c9e7ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "888b9a87-2ccd-471d-a2fd-13fa19eb58f0",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "c94a989a-b2d2-40d3-8c5e-d97740277678",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "b0d8e87e-d0d2-4e01-81aa-8ba9242d7651",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c94a989a-b2d2-40d3-8c5e-d97740277678",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dec15e4a-2c2f-4874-b9c6-fcebec6b5d21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c94a989a-b2d2-40d3-8c5e-d97740277678",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "1a1435f2-b686-4414-b20e-78e6a4fe75fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "09568460-220d-4d82-8197-a035e6392d1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a1435f2-b686-4414-b20e-78e6a4fe75fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0988aa4-19ec-4538-b78e-bccc421b23ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a1435f2-b686-4414-b20e-78e6a4fe75fa",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "b66faa5e-d9db-4499-adc1-e85627631e77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "301af32c-ce2f-456b-ae75-7eb5e8980c20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b66faa5e-d9db-4499-adc1-e85627631e77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90ca1827-cd08-4f3c-be83-a73f4e9fc93d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b66faa5e-d9db-4499-adc1-e85627631e77",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "d7d7e739-3e9e-4598-81c1-73edb5477f4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "61869f91-44a8-437f-9724-a3e735bd18e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7d7e739-3e9e-4598-81c1-73edb5477f4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11ac90f0-bd71-430e-824e-df19f6951804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7d7e739-3e9e-4598-81c1-73edb5477f4e",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        },
        {
            "id": "07a5b692-3515-4180-9789-6f63296587ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "compositeImage": {
                "id": "dfb81ccb-e3ba-490f-b1f1-2e66a79553e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07a5b692-3515-4180-9789-6f63296587ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea8b4568-aa6f-4438-a624-5b0788ae54e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07a5b692-3515-4180-9789-6f63296587ed",
                    "LayerId": "918c6ef4-4e7e-4202-bca3-74d06e6251d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 212,
    "layers": [
        {
            "id": "918c6ef4-4e7e-4202-bca3-74d06e6251d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "853bd290-6a71-4eaa-9935-6cdc4f0f91e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 208,
    "xorig": 104,
    "yorig": 106
}