{
    "id": "41c9469e-8d8a-4e2f-b409-5b0b66bf3d9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e680640a-caa7-465b-bd70-7b4231decc58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c9469e-8d8a-4e2f-b409-5b0b66bf3d9f",
            "compositeImage": {
                "id": "024e4bd7-da63-452d-81e1-4580cc0a4b5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e680640a-caa7-465b-bd70-7b4231decc58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92702e22-afed-4ae1-a869-7e83f701d415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e680640a-caa7-465b-bd70-7b4231decc58",
                    "LayerId": "5326c1fd-92d8-4575-8ce2-2916a6398ca6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5326c1fd-92d8-4575-8ce2-2916a6398ca6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41c9469e-8d8a-4e2f-b409-5b0b66bf3d9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 24,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}