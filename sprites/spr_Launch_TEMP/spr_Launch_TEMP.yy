{
    "id": "41c1f7e8-57e3-48b0-8d4a-94d7cd144b3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Launch_TEMP",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9a9f93d7-a1ee-49e9-8fd4-a9ef8ea34913",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c1f7e8-57e3-48b0-8d4a-94d7cd144b3e",
            "compositeImage": {
                "id": "c103bd68-4fd5-4664-b903-4bed9a2ae35c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a9f93d7-a1ee-49e9-8fd4-a9ef8ea34913",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "263c48cd-f868-45c5-b0b4-184a9dbd2d23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9f93d7-a1ee-49e9-8fd4-a9ef8ea34913",
                    "LayerId": "972ccfc4-9114-4b09-bb73-ba93a8b139d8"
                }
            ]
        },
        {
            "id": "f0a871d9-5388-4da8-b88c-9051e1da89dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c1f7e8-57e3-48b0-8d4a-94d7cd144b3e",
            "compositeImage": {
                "id": "dae08dbf-3d0d-42a2-9d23-6e66501f2e63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0a871d9-5388-4da8-b88c-9051e1da89dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a489f57c-053e-477e-b4b5-e467bf75c1be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0a871d9-5388-4da8-b88c-9051e1da89dc",
                    "LayerId": "972ccfc4-9114-4b09-bb73-ba93a8b139d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "972ccfc4-9114-4b09-bb73-ba93a8b139d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41c1f7e8-57e3-48b0-8d4a-94d7cd144b3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 960,
    "yorig": 540
}