{
    "id": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerRun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 6,
    "bbox_right": 186,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "36daac71-0702-4215-ac56-c17c11c5fa8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "3cbabdd1-ef94-4b56-afb6-d0e35ef03634",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36daac71-0702-4215-ac56-c17c11c5fa8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd3c6419-43cb-4d5b-9ce7-6382d97e3339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36daac71-0702-4215-ac56-c17c11c5fa8e",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "1f62c742-3f38-4411-8b87-4b15d5d5cb3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "a7aaad39-beb5-4da8-9685-9b70c34459ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f62c742-3f38-4411-8b87-4b15d5d5cb3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dea16e20-b37b-460b-a2fc-390fc6083ff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f62c742-3f38-4411-8b87-4b15d5d5cb3d",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "7cfd2432-0108-4b73-94f6-e97da0d8769a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "9526a622-cdc5-4e00-91b1-b99813f0ba9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cfd2432-0108-4b73-94f6-e97da0d8769a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a954cc9-6327-4115-b012-483a121daa64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cfd2432-0108-4b73-94f6-e97da0d8769a",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "7f9912f1-b945-4767-924e-aa7b2a61d7c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "3b852f07-a7c9-4dd2-97a9-7a54dd704ecb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f9912f1-b945-4767-924e-aa7b2a61d7c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3af38a65-ac03-4f30-9be1-0ddb656ca34c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f9912f1-b945-4767-924e-aa7b2a61d7c4",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "5de8d40d-c45b-4959-bd6d-b7801524576a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "11b4df57-f345-4db9-b4b8-7b24ad89cd6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5de8d40d-c45b-4959-bd6d-b7801524576a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2883acc0-7c13-4475-b16b-ab67b4104171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5de8d40d-c45b-4959-bd6d-b7801524576a",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "bcc2e0c9-99e4-4f2d-9d10-672c33185b34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "688dd61d-f22a-47d5-a363-8fe442c57ff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcc2e0c9-99e4-4f2d-9d10-672c33185b34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc78b203-0e31-4079-bd62-f8b5d4d35e90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcc2e0c9-99e4-4f2d-9d10-672c33185b34",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "4833fbf7-9fcd-43f2-ba48-84e0db3f1f63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "215a3b18-a728-47d9-8d3e-f0d1b0342035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4833fbf7-9fcd-43f2-ba48-84e0db3f1f63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03009ad7-d2d5-43e8-b12d-d7ba793d4247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4833fbf7-9fcd-43f2-ba48-84e0db3f1f63",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "a72fccbc-6f84-4db3-bd9e-884c432ea793",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "fc7372e7-243c-4341-a798-ec44528b16e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a72fccbc-6f84-4db3-bd9e-884c432ea793",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcd5013c-22f0-4eb6-a364-bd085534ec32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a72fccbc-6f84-4db3-bd9e-884c432ea793",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "61db9142-d981-4f88-aa9b-102c36d0ae3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "9280260a-3d8d-429b-84b4-79724290dbb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61db9142-d981-4f88-aa9b-102c36d0ae3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a519d36e-14ac-4c3f-8502-7e1ba6fc0d21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61db9142-d981-4f88-aa9b-102c36d0ae3b",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "2cf28edf-c236-4b05-bcf9-2925cacf280c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "c72c1b9c-4a7f-4e85-8620-7ed696fd550b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cf28edf-c236-4b05-bcf9-2925cacf280c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9338c0ee-b1aa-4af6-83b8-5e2a5d3076cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cf28edf-c236-4b05-bcf9-2925cacf280c",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "cd2ec487-5da7-437a-98b3-72f479d9662f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "fcc40f90-5990-49b6-9203-7ed04bc83f13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd2ec487-5da7-437a-98b3-72f479d9662f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a99b311-83b5-4992-804f-1a97265c377f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd2ec487-5da7-437a-98b3-72f479d9662f",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "21fa9d3b-dff5-4f64-aba9-669275ff3292",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "98afc89f-644c-4555-b10e-f3684b39d36d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21fa9d3b-dff5-4f64-aba9-669275ff3292",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7173d24-913e-4e56-ab32-fc7cf1214bfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21fa9d3b-dff5-4f64-aba9-669275ff3292",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "97e2bf8b-4da7-4301-9ef5-55b7b4630fad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "7f035888-2373-4ce6-a4c0-c1f05c2198d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97e2bf8b-4da7-4301-9ef5-55b7b4630fad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec52b064-875a-409f-947a-b974b45bbd4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97e2bf8b-4da7-4301-9ef5-55b7b4630fad",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "83bb9fbe-cd86-4c56-9bd1-d58a9eb62d71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "f7c91946-d8e9-4761-b876-d260d69bce4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83bb9fbe-cd86-4c56-9bd1-d58a9eb62d71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30ea62e5-49af-4ec4-b5e3-f3f0bf7f2ed5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83bb9fbe-cd86-4c56-9bd1-d58a9eb62d71",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "da605f1a-7a2d-4953-8ffa-619e76f4ed2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "9ab24bb7-f905-42b6-a759-341fe75f2ee9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da605f1a-7a2d-4953-8ffa-619e76f4ed2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f15ba105-3ee0-4084-9770-aab981ad16fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da605f1a-7a2d-4953-8ffa-619e76f4ed2a",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "10a1e583-7040-4261-a993-b0a0f448f642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "9735836a-ee6d-40ee-bce9-45174903de0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10a1e583-7040-4261-a993-b0a0f448f642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1e6e4e9-e178-4fb5-af23-3d6544c4054f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10a1e583-7040-4261-a993-b0a0f448f642",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "218a04ef-29c9-41fa-9aaf-f28807ef48e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "629bd740-8b93-4c12-9cf5-bd2901a14ae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "218a04ef-29c9-41fa-9aaf-f28807ef48e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ec01c49-23c4-4795-a63e-6405d4c31b99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "218a04ef-29c9-41fa-9aaf-f28807ef48e3",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "7e716897-2639-43cc-a493-d5e72d0b2921",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "b3b2ebc2-1f90-40ba-b802-ca9fe58904f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e716897-2639-43cc-a493-d5e72d0b2921",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9dfddea-d072-4ebf-8b99-1a26afe1ce89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e716897-2639-43cc-a493-d5e72d0b2921",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "7b824952-d3a8-4d12-b70d-d6a38e6e6d4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "87871075-c50f-433c-9888-9e929a5d65f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b824952-d3a8-4d12-b70d-d6a38e6e6d4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff6af982-bfc4-472f-bfd0-dcde2c12b011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b824952-d3a8-4d12-b70d-d6a38e6e6d4e",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "15418714-06d9-4135-a0ff-4a4f7612489b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "cc5520ae-0e1b-489d-8fb2-681de20cb117",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15418714-06d9-4135-a0ff-4a4f7612489b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48d14a8a-f992-4b8b-a03c-e60a09590016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15418714-06d9-4135-a0ff-4a4f7612489b",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "be38fe66-5062-4d95-b3c9-57f271d82582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "1fcc7e2f-bbfd-4539-876a-1cef61ba8fe1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be38fe66-5062-4d95-b3c9-57f271d82582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3440956c-30d8-4927-a23b-57bfd86a9abd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be38fe66-5062-4d95-b3c9-57f271d82582",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "eeb6a1d3-aa3b-4547-89ed-123f3a920de9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "ccc1cdb4-548c-4e02-a985-39a4c23b2f86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeb6a1d3-aa3b-4547-89ed-123f3a920de9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b133130b-0a74-4a99-bddb-faa8b9bcbebe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeb6a1d3-aa3b-4547-89ed-123f3a920de9",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "177992a4-b119-41dc-a44f-8dd0711d021d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "aea2962a-9164-442f-8aae-1f8b10f58604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "177992a4-b119-41dc-a44f-8dd0711d021d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2754cd5-3c0b-486e-9e89-42977a161f88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "177992a4-b119-41dc-a44f-8dd0711d021d",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "cc591637-843f-4191-b71d-4755963b30dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "449e7ce3-2a30-4553-9dc7-bd2bf50111d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc591637-843f-4191-b71d-4755963b30dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e2b78b3-7553-4ce5-8c14-6ff51c1f23cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc591637-843f-4191-b71d-4755963b30dd",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "72c935c9-150b-476a-b123-1636b491ef5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "9335de41-4153-4b15-91bd-41b174eec5b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72c935c9-150b-476a-b123-1636b491ef5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a34a3db-6422-4200-b3bf-11f15fb84b59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72c935c9-150b-476a-b123-1636b491ef5f",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "f3ee5138-caac-43d5-a2d6-9a18eb5b94cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "3354087e-354f-416f-9735-99e4c7ca6f65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3ee5138-caac-43d5-a2d6-9a18eb5b94cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a36797c7-c22c-4191-a04d-0472097b124a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3ee5138-caac-43d5-a2d6-9a18eb5b94cc",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "e7b4b161-aa6d-4dd9-9f70-17f3ea9fa559",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "12ea9f66-bdb1-494d-9f43-e344f0a6e1f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7b4b161-aa6d-4dd9-9f70-17f3ea9fa559",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f1e63e4-aa93-4fc8-8e18-f7bafffe49b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7b4b161-aa6d-4dd9-9f70-17f3ea9fa559",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "2cf2377d-f8b8-4491-ae7b-f5a22e64a930",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "d3f806d1-316a-4c9c-8686-a36b1d5033e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cf2377d-f8b8-4491-ae7b-f5a22e64a930",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f188e35e-6bbf-4113-80ce-19ba4c96799a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cf2377d-f8b8-4491-ae7b-f5a22e64a930",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "b4a1d94a-fbca-4edd-b9f0-942f70d7930e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "6ffe2d83-3c63-4a8b-9295-460b2c45d08c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4a1d94a-fbca-4edd-b9f0-942f70d7930e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cc6b1e1-cde9-4f2d-948f-a3b646edd2ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4a1d94a-fbca-4edd-b9f0-942f70d7930e",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        },
        {
            "id": "45cc4af3-cd1c-4b89-9b75-17c68d136817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "compositeImage": {
                "id": "d44f0cb7-7809-46fe-9a3f-d3f647229183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45cc4af3-cd1c-4b89-9b75-17c68d136817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c44552d4-33a1-4d8e-952c-ef87ed5b16ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45cc4af3-cd1c-4b89-9b75-17c68d136817",
                    "LayerId": "3766c6fa-1a74-4187-ba12-15653af011be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 121,
    "layers": [
        {
            "id": "3766c6fa-1a74-4187-ba12-15653af011be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fb85c73-4ffd-496c-b010-cb8b5d37ce28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 188,
    "xorig": 94,
    "yorig": 60
}