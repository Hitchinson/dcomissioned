{
    "id": "0e414b03-1358-4f59-943f-87cf886522f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EnemyType1Dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 293,
    "bbox_left": 45,
    "bbox_right": 251,
    "bbox_top": 184,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6058bc79-f8a3-4e46-929f-ba9ab0a4ad3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e414b03-1358-4f59-943f-87cf886522f2",
            "compositeImage": {
                "id": "b0ddfc1f-20ba-477a-9985-82accf7c4896",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6058bc79-f8a3-4e46-929f-ba9ab0a4ad3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58d30392-de68-4c59-9035-edbe0c43a374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6058bc79-f8a3-4e46-929f-ba9ab0a4ad3e",
                    "LayerId": "e0939afd-9eb5-4a58-a4d5-d832e7ece0fd"
                }
            ]
        },
        {
            "id": "8fe46ae2-4809-4dc6-b2e9-21fbc61fd4a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e414b03-1358-4f59-943f-87cf886522f2",
            "compositeImage": {
                "id": "2a6ad7d0-2d1c-44a9-9e53-996f0991a296",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fe46ae2-4809-4dc6-b2e9-21fbc61fd4a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe62174f-a5ad-4838-a4f4-e54a2a80ab60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fe46ae2-4809-4dc6-b2e9-21fbc61fd4a9",
                    "LayerId": "e0939afd-9eb5-4a58-a4d5-d832e7ece0fd"
                }
            ]
        },
        {
            "id": "d29725ca-423e-417a-bd5e-8a45005e7d8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e414b03-1358-4f59-943f-87cf886522f2",
            "compositeImage": {
                "id": "b06cdb04-13ff-40ab-a6af-8fa07c6af59e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d29725ca-423e-417a-bd5e-8a45005e7d8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3749c801-aafe-4931-9f87-f4133580c1a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d29725ca-423e-417a-bd5e-8a45005e7d8e",
                    "LayerId": "e0939afd-9eb5-4a58-a4d5-d832e7ece0fd"
                }
            ]
        },
        {
            "id": "a53d14bd-679d-461b-b4be-6c1425f3bd18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e414b03-1358-4f59-943f-87cf886522f2",
            "compositeImage": {
                "id": "3a5df6cf-18ab-4094-8927-692ef85ba54d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a53d14bd-679d-461b-b4be-6c1425f3bd18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab55157a-2c86-480c-81b4-e6a5b017cf6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a53d14bd-679d-461b-b4be-6c1425f3bd18",
                    "LayerId": "e0939afd-9eb5-4a58-a4d5-d832e7ece0fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "e0939afd-9eb5-4a58-a4d5-d832e7ece0fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e414b03-1358-4f59-943f-87cf886522f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 150
}