{
    "id": "a45d9228-29a4-44bb-9128-e88011fe17c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerFlyLoop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 71,
    "bbox_right": 265,
    "bbox_top": 75,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b3ff4b96-d9a5-4d94-afb4-dd799d804959",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "f2455da7-32f7-4ca1-abb3-354ded24b3a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3ff4b96-d9a5-4d94-afb4-dd799d804959",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72de7657-0a94-4d35-94ab-a90f72f32400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3ff4b96-d9a5-4d94-afb4-dd799d804959",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "79fba360-b720-4c41-9bfd-a0c38b3bb85e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "b4cf2f38-2ea3-4b3a-bde9-3be850ccecc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79fba360-b720-4c41-9bfd-a0c38b3bb85e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f443038-2d32-4aaf-9331-47d472aad345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79fba360-b720-4c41-9bfd-a0c38b3bb85e",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "7746653e-69f5-4f21-a296-140079fa26ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "736da142-2867-4873-8da0-12ce0fb1528f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7746653e-69f5-4f21-a296-140079fa26ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "063da60d-cb96-4994-8650-ff79d5e6b343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7746653e-69f5-4f21-a296-140079fa26ef",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "97ef78a4-d194-45c9-9227-b1ee13e18fb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "1cbdd80e-babb-48d9-b9dc-2045a7a3bf39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97ef78a4-d194-45c9-9227-b1ee13e18fb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25889780-78d6-4f7f-9bf5-a586a94831eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97ef78a4-d194-45c9-9227-b1ee13e18fb9",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "daa27dd8-4ec0-4150-8d1a-21ff95b3568a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "6b4a1b9c-5cb5-4c32-b83f-58cfb8d8d711",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daa27dd8-4ec0-4150-8d1a-21ff95b3568a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b40249b-553b-47d4-82f6-38c658f7defc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daa27dd8-4ec0-4150-8d1a-21ff95b3568a",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "b320d143-df02-49b7-aab1-d217e0f08348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "9eadb51e-1596-4a16-93d1-3f1286261037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b320d143-df02-49b7-aab1-d217e0f08348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e837310-b1fc-4df6-b507-49bac4db841e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b320d143-df02-49b7-aab1-d217e0f08348",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "8acae5a3-9ddb-4265-a4df-d8894389a098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "8abaa41d-41fd-451e-953d-50904df737ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8acae5a3-9ddb-4265-a4df-d8894389a098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed829989-dafe-4caf-beab-dde59c7504d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8acae5a3-9ddb-4265-a4df-d8894389a098",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "2648dfab-faf8-459a-a3f4-07c43a132f25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "ebf87ffc-628f-4974-9017-7deb10ff4ce7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2648dfab-faf8-459a-a3f4-07c43a132f25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45f7dead-8a4b-42ca-b46f-f79fb14eafab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2648dfab-faf8-459a-a3f4-07c43a132f25",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "2743214e-3f7e-4e1a-8771-f8ccba370785",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "51401c1c-fc24-428d-bba0-5cbccf8fcbf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2743214e-3f7e-4e1a-8771-f8ccba370785",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5bbc22b-8b32-4e86-87d6-b6f1b2958013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2743214e-3f7e-4e1a-8771-f8ccba370785",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "ed2dd78e-9beb-46db-a6a2-5a81fddb61c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "4e6ada8c-fa1b-4e32-ba34-9fc3a60f9726",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed2dd78e-9beb-46db-a6a2-5a81fddb61c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47c5e642-7433-42e6-9228-ea3e0cabfe6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed2dd78e-9beb-46db-a6a2-5a81fddb61c2",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "acad3aa2-daee-4f8c-b203-75a9ed1330f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "417de5a4-b74c-4a45-af78-93c5ddbf0964",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acad3aa2-daee-4f8c-b203-75a9ed1330f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dfb6b23-273e-4e02-92df-cb5b27b9033e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acad3aa2-daee-4f8c-b203-75a9ed1330f8",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "687db2f0-b02c-4c5f-8df5-cd88911d8772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "441feff4-27be-44fa-8b8f-605aaefa3b40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "687db2f0-b02c-4c5f-8df5-cd88911d8772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a264b55-b321-48e2-af17-ba81199cf1b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "687db2f0-b02c-4c5f-8df5-cd88911d8772",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "78282a82-fbf1-479f-b631-1c3cebafee02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "af789437-4a41-4bd1-a423-eb4e3cbfeb3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78282a82-fbf1-479f-b631-1c3cebafee02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad91225-7b97-409d-a405-472e9372df64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78282a82-fbf1-479f-b631-1c3cebafee02",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "6d47a4e9-2322-4b99-9e1f-925a0263d256",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "9d61a730-a747-4dfd-972e-90560a6a693d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d47a4e9-2322-4b99-9e1f-925a0263d256",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4add5b9-d965-4135-9db3-4a33ebbd3215",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d47a4e9-2322-4b99-9e1f-925a0263d256",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "4558e40d-7609-44b6-9add-93fbc7889c63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "70c1ba94-98f6-4ba1-b170-050d84d9e808",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4558e40d-7609-44b6-9add-93fbc7889c63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce46189f-e7fb-407e-9fc2-77897a4af87f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4558e40d-7609-44b6-9add-93fbc7889c63",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "31c7d38c-a243-483a-95bb-6f1ea0de2160",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "439f80b3-b3bb-4c7c-a61c-69abf3e9a37e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31c7d38c-a243-483a-95bb-6f1ea0de2160",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d9f3429-bf7b-4ed5-9f98-2325e7190389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31c7d38c-a243-483a-95bb-6f1ea0de2160",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "0e028597-9030-40b9-bce2-3ae0116f974d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "bd709e02-228a-4bd0-a5b6-2b468662f895",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e028597-9030-40b9-bce2-3ae0116f974d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05b2392c-b0fa-41a9-8a84-50e75db3b67b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e028597-9030-40b9-bce2-3ae0116f974d",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "e61ddca7-7a6c-49ad-a769-ca4ee0102d00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "c46d9a43-fd5f-4084-aef1-1f1f50ba8e4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e61ddca7-7a6c-49ad-a769-ca4ee0102d00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43cc6cc4-84ea-4893-89f2-383dfa1a7bfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e61ddca7-7a6c-49ad-a769-ca4ee0102d00",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "5ddb0644-cbfa-4750-9692-3188701a6956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "8703ac3f-a5a3-462c-8669-3ee24106501f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ddb0644-cbfa-4750-9692-3188701a6956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "253b6a92-c956-45de-a8c1-9b27dc0751e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ddb0644-cbfa-4750-9692-3188701a6956",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "586b4af4-cae5-4a85-812a-5804cbc537ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "2f13e161-9036-4da9-a160-63c9a01b12be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "586b4af4-cae5-4a85-812a-5804cbc537ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd2d2e0d-0686-40ac-add0-6ba2ad999703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "586b4af4-cae5-4a85-812a-5804cbc537ec",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "025fffce-fd9e-46b9-91dd-0dc2839b6826",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "c96c19ba-1966-45c6-977d-d74eaae76bae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "025fffce-fd9e-46b9-91dd-0dc2839b6826",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fac35111-ab32-429e-a7e7-eceda647d403",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "025fffce-fd9e-46b9-91dd-0dc2839b6826",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "8ae01d69-6adb-49bf-8248-e8d81bbb704e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "9fb3f9b9-327e-4889-8a92-a493fe5cceb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ae01d69-6adb-49bf-8248-e8d81bbb704e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aef737e-4224-4b66-8231-2e76335780a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ae01d69-6adb-49bf-8248-e8d81bbb704e",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "b8ce57bb-b75c-47cb-97f0-6bb3b3f0dd29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "5ce23306-3a9d-4ba5-8312-654f3746ef06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8ce57bb-b75c-47cb-97f0-6bb3b3f0dd29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf814250-eed8-4c83-8710-7facacbbebfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8ce57bb-b75c-47cb-97f0-6bb3b3f0dd29",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "9241d618-582c-4dc5-aed0-e55642287395",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "c301861f-15c7-49fd-affc-3c4c4a79b376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9241d618-582c-4dc5-aed0-e55642287395",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65b99b40-070a-4c5c-997a-d2ae95990a27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9241d618-582c-4dc5-aed0-e55642287395",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "8eefc31a-63e8-433b-8764-7461cf4604ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "e23a1b8e-99b9-449c-a344-9f143762ad6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eefc31a-63e8-433b-8764-7461cf4604ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a871e4ed-32d0-4c19-a64a-d11b473a6a2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eefc31a-63e8-433b-8764-7461cf4604ec",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "56a06a6a-6a61-45e1-83f8-478e9d7b8d49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "981d4440-c72f-4002-9c32-5efa1f879c7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56a06a6a-6a61-45e1-83f8-478e9d7b8d49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "650350ab-bbfc-4e17-9927-4b12d7748b45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56a06a6a-6a61-45e1-83f8-478e9d7b8d49",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "5749d830-8bb9-4744-8b98-b399d233965f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "1f94ef2e-f450-4f29-960e-d44dc144d560",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5749d830-8bb9-4744-8b98-b399d233965f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "608b73b0-8ef9-4ac4-908d-454f4f171833",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5749d830-8bb9-4744-8b98-b399d233965f",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        },
        {
            "id": "87af15e0-a30d-414e-b69b-64c6c1470eb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "compositeImage": {
                "id": "5adc7e9d-9960-4740-9387-144d0e80bd44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87af15e0-a30d-414e-b69b-64c6c1470eb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33dbfe38-c449-418d-8fd4-8e7624a3ae43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87af15e0-a30d-414e-b69b-64c6c1470eb3",
                    "LayerId": "2145e2bc-c313-41a9-8e22-06890fbf7460"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "2145e2bc-c313-41a9-8e22-06890fbf7460",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a45d9228-29a4-44bb-9128-e88011fe17c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 328,
    "xorig": 164,
    "yorig": 144
}