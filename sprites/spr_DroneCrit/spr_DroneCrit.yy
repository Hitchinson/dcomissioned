{
    "id": "95b6d386-a7de-444d-8ee7-e5e3a004781b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_DroneCrit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 2,
    "bbox_right": 64,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7ab3135f-dcb2-4c5b-8f53-6f08ccfb4db1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95b6d386-a7de-444d-8ee7-e5e3a004781b",
            "compositeImage": {
                "id": "708df405-ee2a-4e2c-8582-fd9c018492e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ab3135f-dcb2-4c5b-8f53-6f08ccfb4db1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee88c371-4d16-4019-beca-7fcc477f170b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ab3135f-dcb2-4c5b-8f53-6f08ccfb4db1",
                    "LayerId": "2b657c3c-b2ab-463b-8fa0-e06b333b3084"
                }
            ]
        },
        {
            "id": "13f593e0-8ce8-4630-b86d-525465c51b60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95b6d386-a7de-444d-8ee7-e5e3a004781b",
            "compositeImage": {
                "id": "c04a616f-76ce-41b6-bf81-a317ae971173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13f593e0-8ce8-4630-b86d-525465c51b60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2ded299-734c-490c-be5c-16e9377e699c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13f593e0-8ce8-4630-b86d-525465c51b60",
                    "LayerId": "2b657c3c-b2ab-463b-8fa0-e06b333b3084"
                }
            ]
        },
        {
            "id": "342a5898-a8f0-4868-a999-98b3959deb3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95b6d386-a7de-444d-8ee7-e5e3a004781b",
            "compositeImage": {
                "id": "058cc6ab-d71f-45b1-83e2-616041474999",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "342a5898-a8f0-4868-a999-98b3959deb3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "676e1d49-7b80-4dd6-acd1-409a3efac1ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "342a5898-a8f0-4868-a999-98b3959deb3a",
                    "LayerId": "2b657c3c-b2ab-463b-8fa0-e06b333b3084"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "2b657c3c-b2ab-463b-8fa0-e06b333b3084",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95b6d386-a7de-444d-8ee7-e5e3a004781b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 34,
    "yorig": 23
}