{
    "id": "8cdd3fe0-568b-4b0b-b8f2-76be22ec70fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FuelBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 409,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 127,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e7c54d4e-c3bd-43e4-87ea-3dbe172ec89e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cdd3fe0-568b-4b0b-b8f2-76be22ec70fd",
            "compositeImage": {
                "id": "21fc07c4-ca8d-4717-a452-20a2ad1d7042",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7c54d4e-c3bd-43e4-87ea-3dbe172ec89e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4d65571-339b-4c7d-ba05-f588ce5d4f74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c54d4e-c3bd-43e4-87ea-3dbe172ec89e",
                    "LayerId": "98244401-67b6-4b42-b460-79abab1b34dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 410,
    "layers": [
        {
            "id": "98244401-67b6-4b42-b460-79abab1b34dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8cdd3fe0-568b-4b0b-b8f2-76be22ec70fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 162,
    "xorig": 0,
    "yorig": 409
}