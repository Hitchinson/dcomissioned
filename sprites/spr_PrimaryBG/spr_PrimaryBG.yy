{
    "id": "48046c10-8c7c-4d31-ae3f-49a7fe20e61a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PrimaryBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2879,
    "bbox_left": 0,
    "bbox_right": 11999,
    "bbox_top": 1210,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "176a88dc-8911-4499-9657-b63bbb826083",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48046c10-8c7c-4d31-ae3f-49a7fe20e61a",
            "compositeImage": {
                "id": "10612741-bbda-44a1-9f8f-b008b0dd8fb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "176a88dc-8911-4499-9657-b63bbb826083",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1e98cc5-d6bc-4602-b3f7-d11c6fe8de47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "176a88dc-8911-4499-9657-b63bbb826083",
                    "LayerId": "d5cb02ed-c0f0-4f4a-8f31-c25bdc68c563"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2880,
    "layers": [
        {
            "id": "d5cb02ed-c0f0-4f4a-8f31-c25bdc68c563",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48046c10-8c7c-4d31-ae3f-49a7fe20e61a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12000,
    "xorig": 0,
    "yorig": 0
}