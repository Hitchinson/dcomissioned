{
    "id": "dbf4d5a7-767d-4c09-be75-9fa8d2760c4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Splash_TEMP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c6187c0b-46a4-4a47-9ed2-489cb0781bb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbf4d5a7-767d-4c09-be75-9fa8d2760c4b",
            "compositeImage": {
                "id": "6c863ad5-2eaf-4691-841b-9af4eb3f6c3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6187c0b-46a4-4a47-9ed2-489cb0781bb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc4f8063-642c-4503-9a2a-be0908d524e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6187c0b-46a4-4a47-9ed2-489cb0781bb8",
                    "LayerId": "69b65a4a-cf78-4160-ba91-094faa10b799"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "69b65a4a-cf78-4160-ba91-094faa10b799",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbf4d5a7-767d-4c09-be75-9fa8d2760c4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 24,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 960,
    "yorig": 540
}