{
    "id": "84ad9230-6e9d-48dc-bd02-d1e3a7e3a57f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprScrap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6c5d6d6e-1ee7-4485-86d8-e5d2719b3d34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84ad9230-6e9d-48dc-bd02-d1e3a7e3a57f",
            "compositeImage": {
                "id": "5df4cdef-580e-42a4-8ede-daea7f10fdc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c5d6d6e-1ee7-4485-86d8-e5d2719b3d34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31ddf064-4b14-42c8-88db-d4c66d54dfd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c5d6d6e-1ee7-4485-86d8-e5d2719b3d34",
                    "LayerId": "787b2b57-3804-4a7c-a469-a71c7ec81e20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "787b2b57-3804-4a7c-a469-a71c7ec81e20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84ad9230-6e9d-48dc-bd02-d1e3a7e3a57f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 24
}