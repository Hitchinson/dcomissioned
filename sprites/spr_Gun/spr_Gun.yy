{
    "id": "d3d0ff00-64ed-4f5e-854f-7188cb08cdad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 3,
    "bbox_right": 75,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7ecd0a01-d4c8-415b-b6ad-2b901cf04f1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3d0ff00-64ed-4f5e-854f-7188cb08cdad",
            "compositeImage": {
                "id": "79bddaa5-b63d-4131-9baa-fd0f88721d91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ecd0a01-d4c8-415b-b6ad-2b901cf04f1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b61ac0-0da1-4ca0-af7a-36eeb7dd89c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ecd0a01-d4c8-415b-b6ad-2b901cf04f1c",
                    "LayerId": "bfbdd51c-2e53-47f6-b375-46e2a76d85c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 74,
    "layers": [
        {
            "id": "bfbdd51c-2e53-47f6-b375-46e2a76d85c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3d0ff00-64ed-4f5e-854f-7188cb08cdad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 37
}