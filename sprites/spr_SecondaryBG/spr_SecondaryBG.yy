{
    "id": "46b45464-b63c-4311-bff8-1583adaa324b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SecondaryBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2680,
    "bbox_left": 0,
    "bbox_right": 11999,
    "bbox_top": 1396,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e9478490-cfc5-4d53-9bd2-a0b55e772001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b45464-b63c-4311-bff8-1583adaa324b",
            "compositeImage": {
                "id": "7c15f787-9b74-4a2a-8f05-fe8fad3d68a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9478490-cfc5-4d53-9bd2-a0b55e772001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b6aba54-0f4b-47cf-90d7-abe191b6cbfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9478490-cfc5-4d53-9bd2-a0b55e772001",
                    "LayerId": "aca41261-ff88-4985-a27e-9c48e3cfffd7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2880,
    "layers": [
        {
            "id": "aca41261-ff88-4985-a27e-9c48e3cfffd7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46b45464-b63c-4311-bff8-1583adaa324b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12000,
    "xorig": 0,
    "yorig": 0
}