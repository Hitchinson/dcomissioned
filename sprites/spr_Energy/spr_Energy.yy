{
    "id": "e6995e6a-759d-4f10-8d52-f3ce18b3fb9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Energy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 0,
    "bbox_right": 147,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "10e65561-a1b8-4ae8-a698-561673b198cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6995e6a-759d-4f10-8d52-f3ce18b3fb9a",
            "compositeImage": {
                "id": "d580a99d-2a00-43da-8165-e621802088a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10e65561-a1b8-4ae8-a698-561673b198cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "053c18f7-c15e-4eca-985e-78c147b87d3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10e65561-a1b8-4ae8-a698-561673b198cf",
                    "LayerId": "9acffdf3-9454-464d-81c9-911ed270ebe2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "9acffdf3-9454-464d-81c9-911ed270ebe2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6995e6a-759d-4f10-8d52-f3ce18b3fb9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 148,
    "xorig": 84,
    "yorig": 73
}