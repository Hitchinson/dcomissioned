{
    "id": "89df6766-2e4c-477e-8f4d-e8e9a177db21",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tl_SecondaryBG1",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 2,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "46b45464-b63c-4311-bff8-1583adaa324b",
    "sprite_no_export": false,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 12,
    "tileheight": 960,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 3000,
    "tilexoff": 0,
    "tileyoff": 0
}