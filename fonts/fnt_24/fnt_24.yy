{
    "id": "f1e02407-52bb-4d9e-9f52-315888ca4a85",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_24",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Open 24 Display St",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c95188ef-d8f8-4651-9554-4cbb8e6ff783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 99,
                "offset": 0,
                "shift": 38,
                "w": 38,
                "x": 396,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d02c4f51-e248-4ccd-8d4c-f64f323dbce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 89,
                "offset": 0,
                "shift": 13,
                "w": 9,
                "x": 35,
                "y": 305
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d315475f-aa56-461f-8732-2af837b9349e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 87,
                "y": 305
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0c5ea730-931b-4585-a53e-b298945795d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 89,
                "offset": -1,
                "shift": 87,
                "w": 83,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d6403f7a-793e-4cd8-8be0-c48db89dd273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 101,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 436,
                "y": 2
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2a509a2f-24c7-4cbb-9d1e-ad07267c2eca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 38,
                "y": 204
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0a41efa8-09f8-4da1-971f-0b29a60101ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 89,
                "offset": 0,
                "shift": 53,
                "w": 49,
                "x": 345,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b4b5b7f1-03a5-4afe-92ce-3bd219987e33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 3,
                "x": 101,
                "y": 305
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "02326e7c-c3b3-4b4a-a768-81fe5e958a85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 89,
                "offset": 0,
                "shift": 25,
                "w": 20,
                "x": 798,
                "y": 204
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1d6062d9-c57d-4a55-bad8-f1a307aba199",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 89,
                "offset": -1,
                "shift": 25,
                "w": 20,
                "x": 820,
                "y": 204
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0619cc39-0849-4be6-a052-489c793f4b83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 65,
                "offset": -1,
                "shift": 31,
                "w": 30,
                "x": 641,
                "y": 204
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d4865d2d-b856-4740-9cb1-fe110419b2c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 81,
                "offset": 0,
                "shift": 34,
                "w": 30,
                "x": 580,
                "y": 204
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ae43abbb-7da8-4881-a5b4-4ca40f851a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 98,
                "offset": 0,
                "shift": 13,
                "w": 9,
                "x": 2,
                "y": 305
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3fb7c13f-8fb7-48a6-bb7f-b76044b3ce3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 55,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 983,
                "y": 204
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "212a5342-20b0-4d98-a861-d6a30ca246e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 90,
                "offset": 0,
                "shift": 12,
                "w": 9,
                "x": 13,
                "y": 305
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "727327ff-00fc-4d76-8871-ce834835e723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 89,
                "offset": -1,
                "shift": 37,
                "w": 34,
                "x": 2,
                "y": 204
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6911e475-98fd-4dd9-b8d8-b3c6bdf61950",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 974,
                "y": 103
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2e580ee4-d748-4e5b-98c0-6b2e86712630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 89,
                "offset": 1,
                "shift": 26,
                "w": 21,
                "x": 728,
                "y": 204
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b9ac74ef-68e0-42fa-8cc6-95ee21c4c6b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 902,
                "y": 103
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7fff2f11-aa41-485e-9549-eda241a8066b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 31,
                "x": 250,
                "y": 204
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "47d280ab-0980-4adb-b083-e86deee7218b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 90,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 688,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "bcb8e1d0-5a86-435e-a11b-709f6a87ab5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 794,
                "y": 103
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "4d2121cf-defa-4e45-891b-8d9b1dd4d26c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 758,
                "y": 103
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5642a6c2-f46a-4e96-a1c5-3fd8f33c8e80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 89,
                "offset": -1,
                "shift": 37,
                "w": 31,
                "x": 382,
                "y": 204
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "46b0865c-c821-4a24-a40a-40c13d98bd98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 722,
                "y": 103
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "401ef8db-44ce-47e2-a8f9-a27eed45f2e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 146,
                "y": 204
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "70688654-fd2a-4d9a-93a1-26125e76786b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 90,
                "offset": 0,
                "shift": 12,
                "w": 9,
                "x": 24,
                "y": 305
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "cb4606d4-2e7d-4a66-95d1-89880d8a3b7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 98,
                "offset": 0,
                "shift": 23,
                "w": 9,
                "x": 1012,
                "y": 204
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "646eaa57-b374-48d5-aaf7-733b9b2bbcfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 78,
                "offset": 0,
                "shift": 29,
                "w": 24,
                "x": 702,
                "y": 204
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0fe4c2c9-1b61-4d0e-9c68-7a0fbf2ce325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 70,
                "offset": 0,
                "shift": 29,
                "w": 27,
                "x": 673,
                "y": 204
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e86b032b-c194-4487-ae07-ce746073c29e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 78,
                "offset": 1,
                "shift": 29,
                "w": 23,
                "x": 751,
                "y": 204
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9e6e204b-d623-44ae-94f2-60443bf7bb66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 31,
                "x": 547,
                "y": 204
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c7588482-fc96-4ec0-b2a9-7b76fc827c16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 830,
                "y": 103
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "28a0a8f6-789d-438d-8d6c-9f58386850b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 938,
                "y": 103
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "118b7be9-ecb9-4c68-a336-575fa2f5dfb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 866,
                "y": 103
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a16384c1-64ad-4170-bde4-51c843ab5021",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 31,
                "x": 514,
                "y": 204
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "54080cc6-8e6e-45c3-9e40-f751a13ae87b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 110,
                "y": 204
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "893f59e2-b398-415c-9ea8-c61441113513",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 31,
                "x": 415,
                "y": 204
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "0debfb5f-72a3-466f-aa08-30cfb8114f1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 31,
                "x": 481,
                "y": 204
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "73634356-5795-488c-a130-9efbaca39be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 74,
                "y": 204
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "73eb0f00-2f67-440e-9552-1ae408b916b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 686,
                "y": 103
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2aa36110-d1b3-46bf-b80d-c3d682d07abe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 89,
                "offset": 0,
                "shift": 12,
                "w": 7,
                "x": 55,
                "y": 305
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a5f8af16-0b65-4c9a-bc97-269e42ba0909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 614,
                "y": 103
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "720832ca-94b3-4be9-935d-238666655d92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 434,
                "y": 103
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "160c98c0-ee72-4db1-bf94-5e91ac9cd238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 31,
                "x": 448,
                "y": 204
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ad906180-dbb7-433c-85cb-72591bfe36c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 89,
                "offset": 0,
                "shift": 68,
                "w": 64,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e592afab-3d97-46b4-8d1b-27a20f8203ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 940,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ebd2e9ad-6b08-4955-8150-47e88491b0ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 650,
                "y": 103
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "13708135-540d-41b6-8f16-a73b5da23519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 976,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ff92ab79-219a-4e53-abe6-ff45344daa41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 98,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 472,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "f2c1aaed-b43d-495a-8d03-117dbd260efb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 90,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 544,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "773e6a87-3c4b-43fd-a77a-576f655194d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 2,
                "y": 103
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "368864d8-7637-4fd3-9a35-11f71da49dc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 89,
                "offset": -1,
                "shift": 37,
                "w": 34,
                "x": 38,
                "y": 103
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "999546d5-970a-4ee4-a655-ef809cd58d23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 74,
                "y": 103
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0139f4c7-b765-4029-b176-256f25460fd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 90,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 580,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a2f8375a-2ea9-4622-9ec2-b2a504edcd95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 89,
                "offset": 0,
                "shift": 65,
                "w": 61,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0f31c909-435b-4e28-8ed9-072c80d9ccd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 90,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 616,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5d1a78f6-1d81-4d49-8b01-c4095cb2cc76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 90,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 652,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6690ac47-1a15-4ced-9ffb-b5d13bd42b8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 218,
                "y": 103
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3c280301-a2f1-4912-b641-35f39dfafa12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 89,
                "offset": 0,
                "shift": 25,
                "w": 20,
                "x": 864,
                "y": 204
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b7c9f3e6-1522-44a5-905e-429f9ef588c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 89,
                "offset": 0,
                "shift": 37,
                "w": 33,
                "x": 182,
                "y": 204
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e6e1c4b3-aeab-488e-9c1b-22ee53000156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 89,
                "offset": -1,
                "shift": 25,
                "w": 20,
                "x": 886,
                "y": 204
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ccfa706e-2dd6-41a6-8890-1d03b8fbb624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 46,
                "offset": 2,
                "shift": 44,
                "w": 36,
                "x": 908,
                "y": 204
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1ecb88d4-8e6b-4419-a1b0-051d686caa42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 89,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 612,
                "y": 204
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6b04972c-bb9c-4c65-a390-fff1b3957513",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 39,
                "offset": 17,
                "shift": 43,
                "w": 12,
                "x": 73,
                "y": 305
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "66d41b2c-27c2-473e-a196-ac853e756318",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 254,
                "y": 103
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8b5d61f8-bba6-4bc9-9747-df89cd831df0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 290,
                "y": 103
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2b3a8fa3-36aa-452d-9056-e27e4489b769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 31,
                "x": 349,
                "y": 204
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "6146b312-3e25-41dd-87c2-47c0370e6329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 326,
                "y": 103
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "6868403d-756f-4a07-91ae-62be123cd9ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 31,
                "x": 316,
                "y": 204
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "453fb21b-3d90-4a5a-adcd-7761d46b82d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 31,
                "x": 283,
                "y": 204
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7485d5e0-1c43-4ac8-9e64-ca0ee0e69b98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 362,
                "y": 103
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a877ef73-34df-4e1d-8c26-a4c7e9f82ba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 398,
                "y": 103
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0f2e1762-fee3-4409-81d9-a389a6e100f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 89,
                "offset": 0,
                "shift": 12,
                "w": 7,
                "x": 64,
                "y": 305
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3ea02926-4ff8-41c5-8f50-f3114557129f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 470,
                "y": 103
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "17f897ff-4abe-42d6-847e-fb0831023492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 506,
                "y": 103
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2a64d611-70b7-413a-96af-f1e85bd887c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 31,
                "x": 217,
                "y": 204
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9db68cac-189f-4308-bf1f-c7bdb61a15b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 89,
                "offset": 0,
                "shift": 68,
                "w": 64,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "42d5a557-6e2a-4269-994f-5f8ebdf76662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 542,
                "y": 103
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "313941e8-6988-49fb-b147-7073b0339baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 868,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "821ba86a-c969-416d-a326-977f0a9924db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 578,
                "y": 103
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f802e7b6-1073-41fd-a706-b95fd3f5b679",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 98,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 508,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e51350d4-2b41-4d67-8486-c9ad04cfc364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 90,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 724,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "71ec8f5a-4b91-4ffa-9c7c-cde8ba1714c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 182,
                "y": 103
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "df7a1ccf-d3a0-42b6-931a-5b3c79ccccfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 89,
                "offset": -1,
                "shift": 37,
                "w": 34,
                "x": 146,
                "y": 103
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0b9f966d-d07c-4b11-bd41-430b253e57ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 110,
                "y": 103
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "945305e7-a353-439d-b2a4-c5a60d0e4391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 90,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 832,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "77056e6b-5c8f-4167-9bab-fa797c04dcfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 89,
                "offset": 0,
                "shift": 65,
                "w": 61,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d25c0df1-35ad-4209-a3a1-10b3e7fd6deb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 90,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 796,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "adc547dc-9b4f-450c-bb01-32c74f54218b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 90,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 760,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "94a8492b-6f7d-49ee-ae96-ebdbe0f69b51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 89,
                "offset": 0,
                "shift": 38,
                "w": 34,
                "x": 904,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d5cac307-b38f-4559-8255-4294f11c41c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 89,
                "offset": 0,
                "shift": 25,
                "w": 20,
                "x": 776,
                "y": 204
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "34cb62c0-82c3-47e2-8553-5aa8d5edf72f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 100,
                "offset": 0,
                "shift": 12,
                "w": 7,
                "x": 46,
                "y": 305
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ebb9a810-0dac-4b32-8015-a0082b1456f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 89,
                "offset": 0,
                "shift": 25,
                "w": 20,
                "x": 842,
                "y": 204
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "25507a73-cf91-48bc-9eed-ecd3d8b282ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 46,
                "offset": 0,
                "shift": 42,
                "w": 35,
                "x": 946,
                "y": 204
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 64,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}