{
    "id": "76914578-2bed-4859-b774-235c6e852342",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_ComicSans",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e829bfbf-b716-4418-9a2b-2a5019bbe83b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 122,
                "y": 74
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e859e329-539e-4aa8-b849-5e9227f04c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 217,
                "y": 74
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c15d0d1c-2cf7-4c08-a1f8-149ce312cce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 190,
                "y": 74
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d1417c8f-64b9-41a0-bd6b-6a3fbd0ad59e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "09c90c5a-d6b9-434c-a1c0-1f9aae0927ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "06238435-8985-4eb2-9506-031d00c9d370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "651715e0-7de5-4485-935b-7e5e25e80c3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 146,
                "y": 26
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e09158bb-9840-4264-97b8-25b1b49c806f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 11,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 248,
                "y": 74
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "79de6b58-ac32-45c7-91b3-4fd7e27b24a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 89,
                "y": 74
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "cc0e6064-d45e-4474-b7d0-3e76407bd50d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 105,
                "y": 74
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "69d96c7d-f98b-4ef0-9352-3b30b9ed4fcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 151,
                "y": 74
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d6ccdd23-e7e1-4242-affa-4b9f28eff28a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 42,
                "y": 74
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "7a73197f-10f5-4ac3-8a8e-42bee1b3d547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 212,
                "y": 74
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "cf1b56ab-34ac-4bcd-a2c0-9bbfe9bb8b1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 169,
                "y": 74
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3b176ec3-cf09-46ab-a4ac-ce306793b2d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 222,
                "y": 74
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "df69b649-e0bc-4693-ad6d-c6d30ee1152e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 141,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "8e3cbda4-6320-4b45-9baa-f683170ad0be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 194,
                "y": 26
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "447cb8d0-9e22-4927-a0d1-01061bed9b2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 143,
                "y": 74
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "03e95b15-1530-4596-9c46-cd566d458fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "59066638-2b8d-4975-9057-60bc5b0e65fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 74
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "cc0e68f9-359b-44b9-9ab2-45ed5e1a19bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 182,
                "y": 26
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b62e5e96-79a4-4526-9403-e6e04f408fe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 170,
                "y": 26
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d5ad40dd-87cf-4a98-b4ac-26d4af1b3a28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8a46e507-1684-496c-a037-0d7eb1e8dfa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 134,
                "y": 26
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4f9f7a15-6b96-4327-a32b-3e0b382a6a4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f8cf42d6-8add-417c-b370-a23a8206d35a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 40,
                "y": 26
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "643180c8-d9af-4502-b621-b1cfac0052b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 237,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e9f44b80-1237-4002-b779-91e6fbeb66b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 184,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4b22785c-89da-4da4-82bb-e5c82eb610b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 177,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b73fccc5-575d-4ab3-a7c2-a7df03160b65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 113,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "03e87a3c-a3ee-43a4-a638-d3e662d738d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 161,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8d415804-512e-4738-aaf4-9aa12d003d77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 151,
                "y": 50
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b31c6298-23f8-4697-a228-e7ab55e83cfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e5e3c12d-8a8e-46d2-96c8-16039f876af0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 26
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e0bca1de-477f-4440-be33-c62afbb1dd28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 50
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "276de755-dd60-4b5f-b8b0-60fa355a7e50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 86,
                "y": 26
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d270e91d-c130-4370-a0bc-838dc690092d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 26
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c6ebe0b2-0d0b-44a9-a147-66bb87b3708b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 50
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "52abdb80-1113-49d3-8760-94eb0c192244",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 50
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2b5e83a5-c14b-4f85-9239-47ff080af9f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "843e5f12-1885-40cc-a4b1-562bf5003f95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f84ed9f9-15f4-48d3-a9dc-a42faee9b18d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 237,
                "y": 26
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "83c4a58b-f80f-4e65-91a2-dcbaa400f914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1c1ffb8e-d6bc-4d2e-a13b-050467b2b0bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 226,
                "y": 26
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "00fe8924-b4e0-47a0-bdfb-cc877b2350ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5c387414-e58a-49e2-9851-744c4fd02939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5d753a93-3580-4e6f-98b6-1621f8e49560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "39f2f916-4861-4685-9f1a-8629bc712097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b3624071-41ad-4d5f-87ad-0a00d74fe2d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 74
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "583c0dee-43ca-4dd7-bfc2-8c57156e4b6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "68b31d4f-7472-48d7-87ad-04e467efbbbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 26
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6ca15c77-abec-400b-9ea8-bb88464d74d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 122,
                "y": 26
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "99ba656b-0a06-4b2a-b46a-7b03cbdd4a83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "92fae3bc-93ca-4c49-a154-0a2d234d67dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 26
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "70c0bec3-9760-4ae3-807d-3ae95ee016f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 28,
                "y": 26
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a5c82d8c-019a-4f11-a730-6dc1e8b6d259",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5cb49442-3048-4ad9-b73c-64499882d4ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 26
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4c9d33da-2d99-42da-ba49-fe907c3b058e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 110,
                "y": 26
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6d78add4-d281-4fb6-a5d1-b3b48d9b1506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "572094f2-b184-44e9-9852-fa066e24dafe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 136,
                "y": 74
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4259de75-9106-43a7-9c57-def4137545a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 52,
                "y": 74
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7d075649-f147-484a-8110-a3d5351291da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 129,
                "y": 74
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6c211b19-70ff-4f73-9c2d-78220483ce0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 203,
                "y": 74
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a36c2d43-f461-4e6d-808b-d84a1cfc233e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d7142e41-0e9c-48a5-b21a-d406bb20e2d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 9,
                "offset": 1,
                "shift": 9,
                "w": 4,
                "x": 242,
                "y": 74
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ae712258-0834-4a2b-be48-05ad752a15ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 181,
                "y": 50
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5bb19382-0374-47da-a5ed-a490d9ed6311",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 171,
                "y": 50
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1e579554-6149-4e41-9622-6a809297004f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 161,
                "y": 50
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "70e33600-395f-4915-82ef-cace2bb6e015",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 50
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "11988526-d0cd-43b3-801c-247b2b501d97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 50
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "16b88b0d-fae7-4d73-9005-882f49221c77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 191,
                "y": 50
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "070315d5-01c7-4837-b1fb-624dd84e4b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 206,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b816050f-05c2-4abd-aaf4-ac53fe9158eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 201,
                "y": 50
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "dbbe4502-f130-4bf6-b4ef-2a67292e121f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 232,
                "y": 74
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "da7ddede-8b0d-40f3-bfea-c3398aba5d8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 23,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 112,
                "y": 50
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b3efa6e1-6926-45fe-b162-363658745c65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 131,
                "y": 50
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "32867042-dd85-4d5f-9d3b-12ec0989e6b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 227,
                "y": 74
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5aa7d9ff-0fa7-4bb2-988e-4901f17a8f0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b878eb08-5e94-4f72-ab95-654b59cf854d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 231,
                "y": 50
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "0a7a11e3-f757-4697-b0c1-0b7dbc030ee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 211,
                "y": 50
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5758a210-c9e6-4148-b787-3d0af83b4efb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3c210bee-ceff-4180-900f-b17a1b01c663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 216,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b7f48572-999f-4f8c-a267-7a53295045ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 69,
                "y": 74
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "804c7f65-cc39-4148-b4be-eafa6fe81965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 241,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "314b8f4b-e495-4532-b147-691c6d4c7e36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f8f0282b-c9f2-4a8b-8676-5e3a4b30aa1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 221,
                "y": 50
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d3346e7b-9821-43fa-92dd-052b95084e7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 121,
                "y": 50
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f19b65dd-886a-4297-a507-0ae991e221cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a559e317-b7ef-4c9a-8588-11537bcfaed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 50
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f373262b-074d-4cc1-b2e6-abec95cc27d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 23,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7d5653de-b7b6-45be-8e53-18a8a22b1ce2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 50
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "9ae2b5ea-f94f-4be6-8021-c4099886dee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 61,
                "y": 74
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a38a26b6-c5b5-4897-bb0b-6c5d79738112",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 198,
                "y": 74
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "55c2cdf5-27f7-4775-8988-5abd7b4ce1b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 97,
                "y": 74
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b9d335f2-7125-4209-820f-c221f0d60a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 14,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 78,
                "y": 74
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}